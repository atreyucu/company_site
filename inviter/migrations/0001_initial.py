# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserInviter'
        db.create_table(u'inviter_userinviter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('referal_code', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'inviter', ['UserInviter'])


    def backwards(self, orm):
        # Deleting model 'UserInviter'
        db.delete_table(u'inviter_userinviter')


    models = {
        u'inviter.userinviter': {
            'Meta': {'object_name': 'UserInviter'},
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referal_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['inviter']