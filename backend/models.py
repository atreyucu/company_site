from __future__ import unicode_literals

from django.db import models

class ResDepartment(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    ##write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    description = models.TextField(blank=True)
    name = models.CharField(max_length=60)
    class Meta:
        db_table = 'res_department'

class ResUsers(models.Model):
    name = models.CharField(max_length=64)
    active = models.BooleanField( default=False)
    login = models.CharField(max_length=64, unique=True)
    password = models.CharField(max_length=64, blank=True)
    email = models.CharField(max_length=64, blank=True)
    context_tz = models.CharField(max_length=64, blank=True)
    signature = models.TextField(blank=True)
    context_lang = models.CharField(max_length=64)
    company_id = models.IntegerField()
    #create_uid = models.ForeignKey("self", null=True, blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey("self", null=True,  blank=True)
    menu_id = models.IntegerField(null=True, blank=True)
    menu_tips = models.BooleanField( default=False)
    date = models.DateTimeField(null=True, blank=True)
    department_ids = models.ForeignKey(ResDepartment,  db_column='department_ids', null=True,  blank=True)
    action_id = models.IntegerField(null=True, blank=True)
    user_email = models.CharField(max_length=64, blank=True)
    class Meta:
        db_table = 'res_users'

class ProductCategory(models.Model):
    parent_left = models.IntegerField(null=True, blank=True)
    parent_right = models.IntegerField(null=True, blank=True)
    create_uid = models.ForeignKey(ResUsers, db_column='create_uid', related_name='create_user', null=True, blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    write_uid = models.ForeignKey(ResUsers, db_column='write_uid', related_name='update_user',null=True, blank=True)
    name = models.CharField(max_length=64)
    sequence = models.IntegerField(null=True, blank=True)
    parent = models.ForeignKey("self", db_column='parent', null=True, blank=True)
    type = models.CharField(max_length=50, blank=True)
    class Meta:
        db_table = 'product_category'

class ProductInstaller(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    customers = models.CharField(max_length=32)
    class Meta:
        db_table = 'product_installer'

class ProductPackaging(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    ul = models.ForeignKey('ProductUl', db_column='ul')
    code = models.CharField(max_length=14, blank=True)
    product = models.ForeignKey('ProductProduct', db_column='product')
    weight = models.FloatField(null=True, blank=True)
    sequence = models.IntegerField(null=True, blank=True)
    ul_qty = models.IntegerField(null=True, blank=True)
    ean = models.CharField(max_length=14, blank=True)
    qty = models.FloatField(null=True, blank=True)
    width = models.FloatField(null=True, blank=True)
    length = models.FloatField(null=True, blank=True)
    rows = models.IntegerField()
    height = models.FloatField(null=True, blank=True)
    weight_ul = models.FloatField(null=True, blank=True)
    name = models.TextField(blank=True)
    class Meta:
        db_table = 'product_packaging'

class ProductPriceList(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    qty1 = models.IntegerField(null=True, blank=True)
    qty2 = models.IntegerField(null=True, blank=True)
    qty3 = models.IntegerField(null=True, blank=True)
    qty4 = models.IntegerField(null=True, blank=True)
    qty5 = models.IntegerField(null=True, blank=True)
    price_list = models.ForeignKey("self", db_column='price_list', related_name='product_price_list')
    class Meta:
        db_table = 'product_price_list'

class ProductPriceType(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    active = models.BooleanField( default=False)
    field = models.CharField(max_length=32)
    currency = models.ForeignKey('ResCurrency', db_column='currency')
    name = models.CharField(max_length=32)
    class Meta:
        db_table = 'product_price_type'

class ProductPricelist(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    currency = models.ForeignKey('ResCurrency', db_column='currency')
    name = models.CharField(max_length=64)
    active = models.BooleanField( default=False)
    type = models.CharField(max_length=50)
    company = models.ForeignKey('ResCompany', db_column='company',null=True, blank=True)
    class Meta:
        db_table = 'product_pricelist'

class ProductPricelistItem(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    price_round = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    price_discount = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    base_pricelist = models.ForeignKey(ProductPricelist, db_column='base_pricelist', null=True, blank=True)
    sequence = models.IntegerField()
    price_max_margin = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    company_id = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=64, blank=True)
    product_tmpl = models.ForeignKey('ProductTemplate',db_column='product_tmpl', null=True, blank=True)
    product = models.ForeignKey('ProductProduct', db_column='product', null=True, blank=True)
    base = models.IntegerField()
    price_version = models.ForeignKey('ProductPricelistVersion', db_column='price_version')
    min_quantity = models.IntegerField()
    price_min_margin = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    categ = models.ForeignKey(ProductCategory, db_column='categ',null=True, blank=True)
    price_surcharge = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    class Meta:
        db_table = 'product_pricelist_item'

class ProductPricelistType(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64)
    key = models.CharField(max_length=64)
    class Meta:
        db_table = 'product_pricelist_type'

class ProductPricelistVersion(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64)
    active = models.BooleanField( default=False)
    pricelist = models.ForeignKey(ProductPricelist, db_column='pricelist')
    date_end = models.DateField(null=True, blank=True)
    date_start = models.DateField(null=True, blank=True)
    company_id = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'product_pricelist_version'


class ProductFlavour(models.Model):
    #create_uid = models.ForeignKey('ResUsers', null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey('ResUsers', null=True, db_column='write_uid', blank=True)
    display_name = models.CharField(max_length=20)
    description = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    class Meta:
        db_table = 'product_flavour'

class ProductProduct(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    ean13 = models.CharField(max_length=13, blank=True)
    color = models.IntegerField(null=True, blank=True)
    price_extra = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    default_code = models.CharField(max_length=64, blank=True)
    name_template = models.CharField(max_length=128, blank=True)
    active = models.BooleanField( default=False)
    variants = models.CharField(max_length=64, blank=True)
    product_strength = models.ForeignKey('ProductStrength', db_column='product_strength', null=True, blank=True)
    product_tmpl = models.ForeignKey('ProductTemplate', db_column='product_tmpl_id')
    product_image = models.TextField(blank=True) # This field type is a guess.
    price_margin = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    is_flavour = models.BooleanField(default=True)
    is_flavour2 = models.BooleanField(default=True)
    product_title = models.TextField(blank=True)
    is_device = models.BooleanField(default=True)
    is_diy = models.BooleanField(default=True)
    is_other_suplements = models.BooleanField(default=True)
    stock_qty = models.BooleanField(default=True)
    smk_order = models.IntegerField(default=1)
    stock_qty = models.FloatField(default=0)
    cloud_category = models.CharField(max_length=2, blank=True)
    product_size = models.ForeignKey('ProductSize', null=True, db_column='product_size', blank=True)
    smk_weight = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    class Meta:
        db_table = 'product_product'

class ProductSize(models.Model):
    #create_uid = models.ForeignKey('ResUsers', null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey('ResUsers', null=True, db_column='write_uid', blank=True)
    display_name = models.CharField(max_length=20)
    description = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    class Meta:
        db_table = 'product_size'

class ProductStrength(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    display_name = models.CharField(max_length=20)
    marketing_text = models.CharField(max_length=255)
    marketing_text2 = models.CharField(max_length=255)
    class Meta:
        db_table = 'product_strength'

class ProductSupplierinfo(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.ForeignKey('ResPartner', db_column='name')
    sequence = models.IntegerField(null=True, blank=True)
    company = models.ForeignKey('ResCompany', null=True, blank=True, db_column='company')
    qty = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    delay = models.IntegerField()
    min_qty = models.FloatField()
    product_code = models.CharField(max_length=64, blank=True)
    product_name = models.CharField(max_length=128, blank=True)
    product = models.ForeignKey('ProductTemplate', db_column='product')
    class Meta:
        db_table = 'product_supplierinfo'

class ProductTemplate(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    warranty = models.FloatField(null=True, blank=True)
    supply_method = models.CharField(max_length=50)
    uos = models.ForeignKey('ProductUom', db_column='uos_id', related_name='product_template_uos', null=True, blank=True)
    list_price = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    weight = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    standard_price = models.DecimalField(max_digits=65535, decimal_places=65535)
    mes_type = models.CharField(max_length=50)
    uom = models.ForeignKey('ProductUom', db_column='uom_id', related_name='product_template_uom')
    description_purchase = models.TextField(blank=True)
    uos_coeff = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    purchase_ok = models.BooleanField( default=False)
    product_manager = models.ForeignKey(ResUsers, db_column='product_manager', null=True, blank=True)
    company = models.ForeignKey('ResCompany', db_column='company_id',null=True, blank=True)
    name = models.CharField(max_length=128)
    state = models.CharField(max_length=50, blank=True)
    loc_rack = models.CharField(max_length=16, blank=True)
    uom_po = models.ForeignKey('ProductUom',db_column='uom_po_id', related_name='product_tamplete_uom_po')
    type = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    weight_net = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    volume = models.FloatField(null=True, blank=True)
    loc_row = models.CharField(max_length=16, blank=True)
    description_sale = models.TextField(blank=True)
    procure_method = models.CharField(max_length=50)
    cost_method = models.CharField(max_length=50)
    rental = models.BooleanField( default=False)
    sale_ok = models.BooleanField( default=False)
    sale_delay = models.FloatField(null=True, blank=True)
    loc_case = models.CharField(max_length=16, blank=True)
    produce_delay = models.FloatField(null=True, blank=True)
    categ = models.ForeignKey(ProductCategory,db_column='categ_id',)
    class Meta:
        db_table = 'product_template'

class ProductUl(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    type = models.CharField(max_length=50)
    name = models.CharField(max_length=64)
    class Meta:
        db_table = 'product_ul'

class ProductUom(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    uom_type = models.CharField(max_length=50)
    category = models.ForeignKey('ProductUomCateg', db_column='category')
    name = models.CharField(max_length=64)
    rounding = models.DecimalField(max_digits=65535, decimal_places=65535)
    factor = models.DecimalField(max_digits=65535, decimal_places=65535)
    active = models.BooleanField( default=False)
    class Meta:
        db_table = 'product_uom'

class ProductUomCateg(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64)
    class Meta:
        db_table = 'product_uom_categ'

class ResBank(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    city = models.CharField(max_length=128, blank=True)
    fax = models.CharField(max_length=64, blank=True)
    name = models.CharField(max_length=128)
    zip = models.CharField(max_length=24, blank=True)
    country = models.ForeignKey('ResCountry', db_column='country', null=True,  blank=True)
    street2 = models.CharField(max_length=128, blank=True)
    bic = models.CharField(max_length=64, blank=True)
    phone = models.CharField(max_length=64, blank=True)
    state = models.ForeignKey('ResCountryState', db_column='state',null=True,  blank=True)
    street = models.CharField(max_length=128, blank=True)
    active = models.BooleanField( default=False)
    email = models.CharField(max_length=64, blank=True)
    class Meta:
        db_table = 'res_bank'



class ResCompany(models.Model):
    parent_id = models.IntegerField(null=True, blank=True)
    #create_uid = models.ForeignKey("self", null=True,  blank=True)
    #create_date = models.ForeignKey(ResUsers, null=True, db_column='create_date', blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    write_uid = models.IntegerField(null=True, blank=True)
    rml_header = models.ForeignKey(ResUsers, related_name='company_rml_header', db_column='rml_header')
    rml_footer1 = models.CharField(max_length=200, blank=True)
    paper_format = models.CharField(max_length=50)
    currency_id = models.IntegerField()
    rml_header2 = models.ForeignKey('ResCurrency', db_column='rml_header2')
    rml_header3 = models.TextField()
    rml_header1 = models.CharField(max_length=200, blank=True)
    logo = models.TextField(blank=True) # This field type is a guess.
    partner_id = models.IntegerField()
    account_no = models.ForeignKey('ResPartner', null=True, db_column='account_no', blank=True)
    company_registry = models.CharField(max_length=64, blank=True)
    name = models.CharField(max_length=128, unique=True, blank=True)
    class Meta:
        db_table = 'res_company'

class ResCompanyUsersRel(models.Model):
    cid = models.ForeignKey(ResCompany, db_column='cid')
    user = models.ForeignKey(ResUsers, db_column='user')
    class Meta:
        db_table = 'res_company_users_rel'

class ResConfig(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    class Meta:
        db_table = 'res_config'

class ResConfigInstaller(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    class Meta:
        db_table = 'res_config_installer'

class ResCountry(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    address_format = models.TextField(blank=True)
    code = models.CharField(max_length=2, unique=True)
    name = models.CharField(max_length=64, unique=True)
    class Meta:
        db_table = 'res_country'

class ResCountryState(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    code = models.CharField(max_length=3)
    country = models.ForeignKey(ResCountry, db_column='country_id')
    name = models.CharField(max_length=64)
    class Meta:
        db_table = 'res_country_state'

class ResCurrency(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=32)
    rounding = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    symbol = models.CharField(max_length=3, blank=True)
    company = models.ForeignKey(ResCompany, db_column='company', null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    base = models.BooleanField( default=False)
    active = models.BooleanField( default=False)
    position = models.CharField(max_length=50, blank=True)
    accuracy = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'res_currency'

class ResCurrencyRate(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    currency = models.ForeignKey(ResCurrency, null=True, blank=True)
    rate = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    name = models.DateField()
    currency_rate_type = models.ForeignKey('ResCurrencyRateType', db_column='currency_rate_type', null=True, blank=True)
    class Meta:
        db_table = 'res_currency_rate'

class ResCurrencyRateType(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64)
    class Meta:
        db_table = 'res_currency_rate_type'



class ResOwner(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    comment = models.TextField(blank=True)
    name = models.CharField(max_length=40, blank=True)
    class Meta:
        db_table = 'res_owner'

class ResPartner(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    website = models.CharField(max_length=64, blank=True)
    ean13 = models.CharField(max_length=13, blank=True)
    color = models.IntegerField(null=True, blank=True)
    active = models.BooleanField( default=False)
    owner = models.ForeignKey(ResOwner, db_column='owner',null=True,  blank=True)
    supplier = models.BooleanField( default=False)
    user = models.ForeignKey(ResUsers, db_column='user_id', null=True, blank=True)
    title = models.ForeignKey('ResPartnerTitle', db_column='title', null=True,  blank=True)
    company = models.ForeignKey(ResCompany, db_column='company_id', null=True, blank=True)
    parent = models.ForeignKey("self", db_column='parent_id', null=True, blank=True)
    password_blank = models.CharField(max_length=64, blank=True)
    employee = models.BooleanField( default=False)
    ref = models.CharField(max_length=64, blank=True)
    vat = models.CharField(max_length=32, blank=True)
    username = models.CharField(max_length=40, blank=True)
    lang = models.CharField(max_length=50, blank=True)
    date = models.DateField(null=True, blank=True)
    customer = models.BooleanField( default=False)
    credit_limit = models.FloatField(null=True, blank=True)
    name = models.CharField(max_length=128)
    comment = models.TextField(blank=True)
    class Meta:
        db_table = 'res_partner'

class ResPartnerAddress(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    function = models.CharField(max_length=128, blank=True)
    fax = models.CharField(max_length=64, blank=True)
    color = models.IntegerField(null=True, blank=True)
    street2 = models.CharField(max_length=128, blank=True)
    phone = models.CharField(max_length=64, blank=True)
    street = models.CharField(max_length=128, blank=True)
    active = models.BooleanField( default=False)
    partner = models.ForeignKey(ResPartner, db_column='partner_id', null=True, blank=True)
    city = models.CharField(max_length=128, blank=True)
    hbn = models.CharField(max_length=30, blank=True)
    name = models.CharField(max_length=64, blank=True)
    zip = models.CharField(max_length=24, blank=True)
    title = models.ForeignKey('ResPartnerTitle', db_column='title', null=True,  blank=True)
    mobile = models.CharField(max_length=64, blank=True)
    country = models.ForeignKey(ResCountry, db_column='country_id', null=True, blank=True)
    company = models.ForeignKey(ResCompany, db_column='company_id', null=True, blank=True)
    birthdate = models.CharField(max_length=64, blank=True)
    second_name = models.CharField(max_length=100, blank=True)
    state = models.ForeignKey(ResCountryState, db_column='state_id', null=True, blank=True)
    type = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=240, blank=True)
    class Meta:
        db_table = 'res_partner_address'

class ResPartnerBank(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    bank_name = models.CharField(max_length=32, blank=True)
    owner_name = models.CharField(max_length=128, blank=True)
    sequence = models.IntegerField(null=True, blank=True)
    street = models.CharField(max_length=128, blank=True)
    partner = models.ForeignKey(ResPartner, db_column='partner')
    bank = models.ForeignKey(ResBank, db_column='bank', null=True,  blank=True)
    bank_bic = models.CharField(max_length=16, blank=True)
    city = models.CharField(max_length=128, blank=True)
    name = models.CharField(max_length=64, blank=True)
    zip = models.CharField(max_length=24, blank=True)
    footer = models.BooleanField( default=False)
    country = models.ForeignKey(ResCountry, db_column='country', null=True, blank=True)
    company = models.ForeignKey(ResCompany, db_column='company', null=True, blank=True)
    state = models.CharField(max_length=50)
    state_id = models.ForeignKey(ResCountryState, null=True, db_column='state_id', blank=True) # Field renamed because of name conflict.
    acc_number = models.CharField(max_length=64)
    class Meta:
        db_table = 'res_partner_bank'

class ResPartnerBankType(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    code = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    format_layout = models.TextField(blank=True)
    class Meta:
        db_table = 'res_partner_bank_type'

class ResPartnerBankTypeField(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    bank_type = models.ForeignKey(ResPartnerBankType, db_column='bank_type')
    readonly = models.BooleanField( default=False)
    required = models.BooleanField( default=False)
    name = models.CharField(max_length=64)
    size = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'res_partner_bank_type_field'

class ResPartnerCategory(models.Model):
    parent_left = models.IntegerField(null=True, blank=True)
    parent_right = models.IntegerField(null=True, blank=True)
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64)
    parent = models.ForeignKey("self", db_column='parent', null=True, blank=True)
    active = models.BooleanField( default=False)
    class Meta:
        db_table = 'res_partner_category'

class ResPartnerCategoryRel(models.Model):
    category = models.ForeignKey(ResPartnerCategory, db_column='category')
    partner = models.ForeignKey(ResPartner, db_column='partner')
    class Meta:
        db_table = 'res_partner_category_rel'

class ResPartnerEvent(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    date = models.DateTimeField(null=True, blank=True)
    partner = models.ForeignKey(ResPartner, db_column='partner', null=True, blank=True)
    user = models.ForeignKey(ResUsers, db_column='user', null=True, blank=True)
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True)
    class Meta:
        db_table = 'res_partner_event'

class ResPartnerTitle(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    domain = models.CharField(max_length=24)
    name = models.CharField(max_length=46)
    shortcut = models.CharField(max_length=16)
    class Meta:
        db_table = 'res_partner_title'

class ResPayterm(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    name = models.CharField(max_length=64, blank=True)
    class Meta:
        db_table = 'res_payterm'

class ResRequest(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    body = models.TextField(blank=True)
    name = models.CharField(max_length=128)
    date_sent = models.DateTimeField(null=True, blank=True)
    ref_doc2 = models.CharField(max_length=128, blank=True)
    priority = models.CharField(max_length=50)
    ref_doc1 = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=50)
    act_from = models.ForeignKey(ResUsers,related_name='request_act_from', db_column='act_from')
    ref_partner = models.ForeignKey(ResPartner, db_column='ref_partner', null=True, blank=True)
    active = models.BooleanField( default=False)
    trigger_date = models.DateTimeField(null=True, blank=True)
    act_to = models.ForeignKey(ResUsers, related_name='request_act_to', db_column='act_to')
    class Meta:
        db_table = 'res_request'

class ResRequestHistory(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    body = models.TextField(blank=True)
    act_from = models.ForeignKey(ResUsers, related_name= 'request_history_act_from', db_column='act_from')
    name = models.CharField(max_length=128)
    req = models.ForeignKey(ResRequest, db_column='req')
    date_sent = models.DateTimeField()
    #act_to = models.ForeignKey(ResUsers, db_column='act_to')
    class Meta:
        db_table = 'res_request_history'

class ResRequestLink(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    priority = models.IntegerField(null=True, blank=True)
    object = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    class Meta:
        db_table = 'res_request_link'



class ResWidget(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    content = models.TextField()
    title = models.CharField(max_length=64)
    class Meta:
        db_table = 'res_widget'

class ResWidgetUser(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    user = models.ForeignKey(ResUsers, db_column='user',null=True, blank=True)
    widget = models.ForeignKey(ResWidget, db_column='widget')
    sequence = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'res_widget_user'

class ResWidgetWizard(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    widgets_list = models.CharField(max_length=50)
    class Meta:
        db_table = 'res_widget_wizard'

class SmkLqdHtmlTemplates(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    template_content = models.TextField()
    created_on = models.DateField(null=True, blank=True)
    name = models.CharField(max_length=50, blank=True)
    class Meta:
        db_table = 'smk_lqd_html_templates'

class SmkLqdInvites(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    convert_signup = models.BooleanField(default=False)
    sender = models.ForeignKey(ResPartner, db_column='sender')
    was_followed = models.BooleanField(default=False)
    sent_on = models.DateField(null=True, blank=True)
    channel = models.CharField(max_length=50)
    followed_on = models.DateField(null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_invites'

class SmkLqdMessage(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    body = models.TextField()
    sender = models.ForeignKey(ResPartner, db_column='sender',null=True,  blank=True)
    sender_name = models.CharField(max_length=200, default='', null=True)
    sender_email = models.CharField(max_length=200, default='')
    replier_user = models.ForeignKey(ResUsers, null=True, db_column='replier_user', blank=True)
    reply_body = models.TextField(blank=True)
    was_replied = models.BooleanField( default=False)
    is_public = models.BooleanField( default=False)
    from_site = models.CharField(max_length=50)
    sent_on = models.DateField(null=True, blank=True)
    subject = models.CharField(max_length=150)
    status = models.CharField(max_length=5,default='n')
    type = models.CharField(max_length=5, default='c')
    class Meta:
        db_table = 'smk_lqd_message'

class SmkLqdOrderBatch(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    envelope_printed = models.CharField(default='n',max_length=1)
    manual = models.CharField(default='n',max_length=1)
    created_on = models.DateField()
    printed = models.CharField(default='n',max_length=1)
    batch_id = models.CharField(max_length=50)
    require_envelope = models.CharField(default='n',max_length=1)
    order_count = models.IntegerField(default=0)
    potential_fraud = models.CharField(default='n',max_length=1)
    free_order = models.CharField(default='n',max_length=1)
    class Meta:
        db_table = 'smk_lqd_order_batch'

class SmkLqdOrderItem(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    product = models.ForeignKey(ProductProduct, db_column='product_id')
    line_total = models.IntegerField(null=True, blank=True)
    sale_price = models.DecimalField(null=True, max_digits=4, decimal_places=4, blank=True)
    order_ids = models.ForeignKey('SmkLqdOrderOrder', null=True, db_column='order_ids', blank=True)
    product_description = models.TextField(blank=True)
    product_name = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = 'smk_lqd_order_item'

class SmkLqdOrderOrder(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    customer_delivery_street_line2 = models.CharField(max_length=200, blank=True)
    customer_billing_street = models.CharField(max_length=200, blank=True)
    customer_email = models.CharField(max_length=20, blank=True)
    require_envelope = models.CharField(default='n',max_length=1)
    potential_fraud = models.CharField(default='n',max_length=1)
    envelope_printed = models.CharField(default='n',max_length=1)
    customer_phone = models.CharField(max_length=20, blank=True)
    customer_delivery_county = models.CharField(max_length=100, blank=True)
    customer_billing_county = models.CharField(max_length=100, blank=True)
    customer_billing_postcode = models.CharField(max_length=100, blank=True)
    created_on = models.DateField()
    printed = models.CharField(default='n',max_length=1)
    customer_billing_country = models.CharField(max_length=100, blank=True)
    customer_billing_hbn = models.CharField(max_length=50, blank=True)
    customer_billing_city = models.CharField(max_length=100, blank=True)
    customer = models.ForeignKey(ResPartner, db_column='customer')
    order_total = models.FloatField(null=True, blank=True, default=0)
    order_total_percent = models.FloatField(null=True, blank=True, default=0)
    customer_delivery_postcode = models.CharField(max_length=100, blank=True)
    customer_billing_street_line2 = models.CharField(max_length=200, blank=True)
    customer_delivery_street = models.CharField(max_length=200, blank=True)
    manual = models.CharField(default='n',max_length=1)
    customer_mobile = models.CharField(max_length=20, blank=True)
    customer_delivery_hbn = models.CharField(max_length=50, blank=True)
    batch_ids = models.ForeignKey(SmkLqdOrderBatch, null=True, db_column='batch_ids', blank=True)
    customer_delivery_city = models.CharField(max_length=100, blank=True)
    customer_name = models.CharField(max_length=150, blank=True)
    customer_fax = models.CharField(max_length=20, blank=True)
    customer_delivery_country = models.CharField(max_length=100, blank=True)
    trans_id = models.CharField(max_length=100)
    printed_on = models.DateField(null=True, blank=True)
    free_order = models.CharField(default='n',max_length=1)
    regional_division = models.ForeignKey('SmkLqdRegionalDivision', db_column='regional_division')
    print_postage = models.CharField(max_length=1, blank=True)
    class Meta:
        db_table = 'smk_lqd_order_order'

class SmkLqdRegionalDivision(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    fb_link = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=50)
    class Meta:
        db_table = 'smk_lqd_regional_division'

class SmkLqdWeightsRanges(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    initial_weight = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    price = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    final_weight = models.DecimalField(null=True, max_digits=65535, decimal_places=65535, blank=True)
    class Meta:
        db_table = 'smk_lqd_weights_ranges'


class SmkLqdFundsTotal(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    total = models.FloatField(null=True, blank=True)
    month = models.CharField(max_length=50)
    class Meta:
        db_table = 'smk_lqd_funds_total'

class SmkLqdRemoteAddr(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    addr = models.CharField(max_length=255, blank=True)
    subscriptions_count = models.IntegerField(default=1)
    class Meta:
        db_table = 'smk_lqd_remote_addr'

class SmkLqdSubsFlavours(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    flavour1 = models.ForeignKey(ProductProduct,db_column='flavour1', related_name='flavour_flavour1', null=True, blank=True)
    flavour10 = models.ForeignKey(ProductProduct, db_column='flavour10',related_name='flavour_flavour10', null=True, blank=True)
    flavour3 = models.ForeignKey(ProductProduct, db_column='flavour3',related_name='flavour_flavour3',null=True, blank=True)
    flavour2 = models.ForeignKey(ProductProduct, db_column='flavour2',null=True, related_name='flavour_flavour2',blank=True)
    flavour5 = models.ForeignKey(ProductProduct, db_column='flavour5',related_name='flavour_flavour5',null=True, blank=True)
    flavour4 = models.ForeignKey(ProductProduct, db_column='flavour4',related_name='flavour_flavour4',null=True, blank=True)
    flavour7 = models.ForeignKey(ProductProduct, db_column='flavour7',related_name='flavour_flavour7',null=True, blank=True)
    flavour6 = models.ForeignKey(ProductProduct, db_column='flavour6',related_name='flavour_flavour6',null=True, blank=True)
    flavour9 = models.ForeignKey(ProductProduct, db_column='flavour9',related_name='flavour_flavour9',null=True, blank=True)
    flavour8 = models.ForeignKey(ProductProduct, db_column='flavour8',related_name='flavour_flavour8',null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_subs_flavours'

    def change_flavour(self, product_id, number):
        if number == 1 or str(number) == '1':
            self.flavour1_id = product_id
        elif number == 2 or str(number) == '2':
            self.flavour2_id = product_id
        elif number == 3 or str(number) == '3':
            self.flavour3_id = product_id
        elif number == 4 or str(number) == '4':
            self.flavour4_id = product_id
        elif number == 5 or str(number) == '5':
            self.flavour5_id = product_id
        elif number == 6 or str(number) == '6':
            self.flavour6_id = product_id
        elif number == 7 or str(number) == '7':
            self.flavour7_id = product_id
        elif number == 8 or str(number) == '8':
            self.flavour8_id = product_id
        elif number == 9 or str(number) == '9':
            self.flavour9_id = product_id
        elif number == 10 or str(number) == '10':
            self.flavour10_id = product_id
        else:
            return False
        self.save()
        return True

class SmkLqdSubsFreeFlavours(models.Model):
    create_uid = models.ForeignKey(ResUsers, db_column='create_uid', related_name='free_flavour_create_user', null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    write_uid = models.ForeignKey(ResUsers, db_column='write_uid', related_name='free_flavour_update_user1',null=True,  blank=True)
    flavour1 = models.ForeignKey(ProductProduct,db_column='flavour1', related_name='free_flavour_flavour1',null=True, blank=True)
    flavour3 = models.ForeignKey(ProductProduct,db_column='flavour3', related_name='free_flavour_flavour3',null=True, blank=True)
    flavour2 = models.ForeignKey(ProductProduct,db_column='flavour2', related_name='free_flavour_flavour2',null=True, blank=True)
    flavour4 = models.ForeignKey(ProductProduct, db_column='flavour4',related_name='free_flavour_flavour4',null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_subs_free_flavours'

    def change_flavour(self, product_id, number):
        if number == 1 or str(number) == '1':
            self.flavour1_id = product_id
        elif number == 2 or str(number) == '2':
            self.flavour2_id = product_id
        elif number == 3 or str(number) == '3':
            self.flavour3_id = product_id
        elif number == 4 or str(number) == '4':
            self.flavour4_id = product_id
        else:
            return False
        self.save()
        return True

class SmkLqdSubsDailyRefund(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    #create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    status = models.CharField(max_length=1, blank=True)
    signup_date = models.DateField(null=True, blank=True)
    refund_date = models.DateField(null=True, blank=True)
    refunds_due = models.IntegerField(default=0,null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_subs_daily_refund'

class SmkLqdSubsPriceGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    price = models.FloatField(null=True, blank=True)
    name = models.CharField(max_length=255, blank=True)
    class Meta:
        db_table = 'smk_lqd_subs_price_group'

class SmkLqdSubsStandingOrder(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True,  blank=True)
    cancelled_on = models.DateField(null=True, blank=True)
    free_vaporizer_count = models.IntegerField(null=True, blank=True)
    successful_billing_count = models.IntegerField(null=True, blank=True)
    last_batchid = models.CharField(max_length=50, blank=True)
    snail_mail_counter = models.IntegerField(default=2)
    customer = models.ForeignKey(ResPartner, db_column='customer')
    last_order_date = models.DateField(null=True, blank=True)
    request_cancellation = models.BooleanField( default=False)
    cohort_index = models.IntegerField(null=True, blank=True)
    next_delivery = models.DateField(null=True, blank=True)
    next_printed = models.DateField(null=True, blank=True)
    last_printed = models.DateField(null=True, blank=True)
    last_delivery = models.DateField(null=True, blank=True)
    next_billing = models.DateField(null=True, blank=True)
    last_billing = models.DateField(null=True, blank=True)
    remote_addr = models.ForeignKey(SmkLqdRemoteAddr, db_column='remote_addr',null=True,  blank=True)
    cancelled = models.BooleanField( default=False)
    order_counter = models.IntegerField(null=True, blank=True)
    battery_counter = models.IntegerField(null=True, blank=True)
    potential_fraud = models.BooleanField( default=False)
    flavours = models.ForeignKey(SmkLqdSubsFlavours, db_column='flavours', null=True, blank=True)
    free_flavours = models.ForeignKey(SmkLqdSubsFreeFlavours, db_column='free_flavours',null=True, blank=True)
    cigarette_smoked_per_day = models.IntegerField(default=0)
    status = models.CharField(max_length=1,default='y')
    fraud = models.CharField(max_length=5,default='n')
    customer_name = models.CharField(max_length=200,null=True)
    customer_zip_code = models.CharField(max_length=50,null=True)
    customer_email = models.CharField(max_length=200,null=True)
    created_date = models.DateField(null=True, blank=True)
    daily_refund_ids = models.ForeignKey(SmkLqdSubsDailyRefund, null=True, db_column='daily_refund_ids', blank=True)
    subs_paypal_id = models.CharField(max_length=25, null=True, blank=True)
    auto_cancelled = models.CharField(max_length=5,default='n')
    reactivated = models.CharField(max_length=5,default='n')
    reactivated_date = models.DateField(null=True, blank=True)
    price_group = models.ForeignKey(SmkLqdSubsPriceGroup, db_column='price_group', null=True, blank=True)

    class Meta:
        db_table = 'smk_lqd_subs_standing_order'




class SmkLqdCohort(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    month15 = models.CharField(max_length=30, blank=True)
    month12 = models.CharField(max_length=30, blank=True)
    month5 = models.CharField(max_length=30, blank=True)
    total = models.CharField(max_length=30, blank=True)
    month51 = models.CharField(max_length=30, blank=True)
    month8 = models.CharField(max_length=30, blank=True)
    month9 = models.CharField(max_length=30, blank=True)
    month55 = models.CharField(max_length=30, blank=True)
    month1 = models.CharField(max_length=30, blank=True)
    cohort_index = models.CharField(max_length=30)
    month3 = models.CharField(max_length=30, blank=True)
    month4 = models.CharField(max_length=30, blank=True)
    month32 = models.CharField(max_length=30, blank=True)
    month6 = models.CharField(max_length=30, blank=True)
    month7 = models.CharField(max_length=30, blank=True)
    month23 = models.CharField(max_length=30, blank=True)
    month22 = models.CharField(max_length=30, blank=True)
    month21 = models.CharField(max_length=30, blank=True)
    month20 = models.CharField(max_length=30, blank=True)
    month27 = models.CharField(max_length=30, blank=True)
    month26 = models.CharField(max_length=30, blank=True)
    month25 = models.CharField(max_length=30, blank=True)
    month24 = models.CharField(max_length=30, blank=True)
    month52 = models.CharField(max_length=30, blank=True)
    month29 = models.CharField(max_length=30, blank=True)
    month28 = models.CharField(max_length=30, blank=True)
    month17 = models.CharField(max_length=30, blank=True)
    month53 = models.CharField(max_length=30, blank=True)
    month49 = models.CharField(max_length=30, blank=True)
    month48 = models.CharField(max_length=30, blank=True)
    month50 = models.CharField(max_length=30, blank=True)
    month41 = models.CharField(max_length=30, blank=True)
    month40 = models.CharField(max_length=30, blank=True)
    month43 = models.CharField(max_length=30, blank=True)
    month42 = models.CharField(max_length=30, blank=True)
    month45 = models.CharField(max_length=30, blank=True)
    month44 = models.CharField(max_length=30, blank=True)
    month47 = models.CharField(max_length=30, blank=True)
    month46 = models.CharField(max_length=30, blank=True)
    month18 = models.CharField(max_length=30, blank=True)
    month13 = models.CharField(max_length=30, blank=True)
    month60 = models.CharField(max_length=30, blank=True)
    month56 = models.CharField(max_length=30, blank=True)
    month57 = models.CharField(max_length=30, blank=True)
    month14 = models.CharField(max_length=30, blank=True)
    month19 = models.CharField(max_length=30, blank=True)
    month30 = models.CharField(max_length=30, blank=True)
    month31 = models.CharField(max_length=30, blank=True)
    month54 = models.CharField(max_length=30, blank=True)
    month33 = models.CharField(max_length=30, blank=True)
    month34 = models.CharField(max_length=30, blank=True)
    month35 = models.CharField(max_length=30, blank=True)
    month36 = models.CharField(max_length=30, blank=True)
    month37 = models.CharField(max_length=30, blank=True)
    month38 = models.CharField(max_length=30, blank=True)
    month39 = models.CharField(max_length=30, blank=True)
    month10 = models.CharField(max_length=30, blank=True)
    month11 = models.CharField(max_length=30, blank=True)
    month16 = models.CharField(max_length=30, blank=True)
    month2 = models.CharField(max_length=30, blank=True)
    month58 = models.CharField(max_length=30, blank=True)
    month59 = models.CharField(max_length=30, blank=True)
    class Meta:
        db_table = 'smk_lqd_cohort'

class SmkLqdSnailBatch(models.Model):
    id = models.AutoField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    batch_id = models.CharField(max_length=50)
    status = models.CharField(max_length=10, blank=True)
    email_count = models.IntegerField(null=True, blank=True)
    generated_date = models.DateField()
    class Meta:
        db_table = 'smk_lqd_snail_batch'

class SmkLqdSnailLetter(models.Model):
    id = models.AutoField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    days_before_order = models.IntegerField(null=True, blank=True)
    specific_order = models.IntegerField(null=True, blank=True)
    frequency = models.CharField(max_length=5, blank=True)
    name = models.CharField(max_length=30)
    beginning_in = models.IntegerField(null=True, blank=True)
    html_template = models.ForeignKey(SmkLqdHtmlTemplates, null=True, db_column='html_template', blank=True)
    status =  models.CharField(max_length=5, blank=True)
    letter_text = models.TextField()
    class Meta:
        db_table = 'smk_lqd_snail_letter'

class SmkLqdSnailMail(models.Model):
    id = models.AutoField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    customer = models.ForeignKey(ResPartner, db_column='customer')
    status = models.CharField(max_length=5, blank=True)
    generated_date = models.DateField(null=True, blank=True)
    email_id = models.CharField(max_length=100)
    customer_active = models.CharField(max_length=5, blank=True)
    batch_ids = models.ForeignKey(SmkLqdSnailBatch, null=True, db_column='batch_ids', blank=True)
    customer_name = models.CharField(max_length=150, blank=True)
    letter = models.ForeignKey(SmkLqdSnailLetter, null=True, db_column='letter', blank=True)
    class Meta:
        db_table = 'smk_lqd_snail_mail'

class SmkLqdPrizeRegistration(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    username = models.CharField(max_length=50)
    status = models.CharField(max_length=10, blank=True)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    signup_date = models.DateField(null=True, blank=True)
    email = models.CharField(max_length=50)
    town = models.CharField(max_length=50, blank=True)
    hbn = models.CharField(max_length=50, blank=True)
    name = models.CharField(max_length=90, blank=True)
    city = models.CharField(max_length=50, blank=True)
    street = models.CharField(max_length=200, blank=True)
    postcode = models.CharField(max_length=50, blank=True)
    order_printed_on = models.DateField(null=True, blank=True)
    order_printed = models.CharField(max_length=2,default='n')

    cloud_size = models.CharField(max_length=2, blank=True)
    class Meta:
        db_table = 'smk_lqd_prize_registration'

class SmkLqdEmailAccount(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    email_addr = models.CharField(max_length=255)
    name = models.CharField(max_length=100)
    class Meta:
        db_table = 'smk_lqd_email_account'

class IrMailServer(models.Model):
    #create_uid = models.ForeignKey('ResUsers', null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey('ResUsers', null=True, db_column='write_uid', blank=True)
    name = models.CharField(max_length=64)
    sequence = models.IntegerField(null=True, blank=True)
    smtp_port = models.IntegerField()
    smtp_host = models.CharField(max_length=128)
    smtp_user = models.CharField(max_length=64, blank=True)
    smtp_pass = models.CharField(max_length=64, blank=True)
    smtp_debug = models.BooleanField(default=False)
    smtp_encryption = models.CharField(max_length=10)
    class Meta:
        db_table = 'ir_mail_server'

class SmkLqdNotificationEmail(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    email_from_addr = models.ForeignKey(SmkLqdEmailAccount, null=True, db_column='email_from_addr', blank=True)
    status = models.CharField(max_length=5, blank=True)
    hour = models.CharField(max_length=5, blank=True)
    email_template = models.ForeignKey(SmkLqdHtmlTemplates, null=True, db_column='email_template', blank=True)
    email_content = models.TextField()
    email_condition_listb = models.CharField(max_length=5, blank=True)
    email_condition_lista = models.CharField(max_length=5, blank=True)
    email_list = models.CharField(max_length=5, blank=True)
    frequency = models.CharField(max_length=5, blank=True)
    email_condition_value = models.IntegerField(null=True, blank=True)
    email_subject = models.CharField(max_length=255)
    beginning_in = models.IntegerField(null=True, blank=True)
    email_server = models.ForeignKey(IrMailServer, null=True, db_column='email_server', blank=True)
    order_number = models.IntegerField(null=True, blank=True)
    minute = models.CharField(max_length=10, blank=True)
    name = models.CharField(max_length=100, blank=True)
    require_addr = models.CharField(max_length=5,default='n')
    addressee = models.CharField(max_length=255,default='')
    class Meta:
        db_table = 'smk_lqd_notification_email'

class SmkLqdBoardCancelledGraph1(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    cancelled_count = models.IntegerField(default=0, null=True, blank=True)
    new_subs = models.IntegerField(default=0, null=True, blank=True)
    month = models.CharField(max_length=50, blank=True)
    class Meta:
        db_table = 'smk_lqd_board_cancelled_graph1'

class SmkLqdBoardOrder(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    pending_orders = models.IntegerField(null=True, blank=True)
    tomorrow_orders = models.IntegerField(null=True, blank=True)
    todays_new_orders = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_board_order'

class SmkLqdBoardSubscriptions(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    todays_new_subs = models.IntegerField(null=True, blank=True)
    cancelled_subscriber = models.IntegerField(null=True, blank=True)
    can_be_cancelled_today = models.IntegerField(null=True, blank=True)
    total_subs = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_board_subscriptions'



class SmkLqdBoardCancelledGraph2(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    count = models.IntegerField(null=True, blank=True)
    signup_method = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = 'smk_lqd_board_cancelled_graph2'

class SmkLqdRecurringPaymentSettings(models.Model):
    id = models.IntegerField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    days_per_period = models.IntegerField(default=61,null=True, blank=True)
    printing_minus_days = models.IntegerField(default=10,null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_recurring_payment_settings'


class SmkLqdBoardCancelledGraph3(models.Model):
    #id = models.IntegerField(primary_key=True)
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    price_group = models.CharField(max_length=200, blank=True)
    count = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_board_cancelled_graph3'


class LambertCrmCancelledList(models.Model):
    # id = models.IntegerField(primary_key=True)
    # create_uid = models.ForeignKey('ResUsers', null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    # write_uid = models.ForeignKey('ResUsers', null=True, db_column='write_uid', blank=True)
    customer = models.ForeignKey('SmkLqdSubsStandingOrder', null=True, db_column='customer', blank=True)
    status = models.CharField(max_length=255, blank=True)
    last_delivery = models.DateField(null=True, blank=True)
    name = models.CharField(max_length=150, blank=True)
    next_delivery = models.DateField(null=True, blank=True)
    email = models.CharField(max_length=50, blank=True)
    class Meta:
        db_table = 'lambert_crm_cancelled_list'

class LambertCrmPreoffer(models.Model):
    # id = models.IntegerField(primary_key=True)
    # create_uid = models.ForeignKey('ResUsers', null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    # write_uid = models.ForeignKey('ResUsers', null=True, db_column='write_uid', blank=True)
    customer = models.ForeignKey('SmkLqdSubsStandingOrder', db_column='customer')
    name = models.CharField(max_length=150)
    notes = models.TextField(blank=True)
    tag = models.CharField(max_length=255, blank=True)
    type = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=50)
    pending = models.CharField(max_length=255, blank=True)
    stage = models.CharField(max_length=255, blank=True)
    class Meta:
        db_table = 'lambert_crm_preoffer'


class SmkLqdVaucherCode(models.Model):
    # id = models.IntegerField(primary_key=True)
    # create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    # write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    code = models.CharField(max_length=10)
    percent = models.IntegerField(default=50)
    expired_on = models.DateField(null=True, blank=True)
    expired = models.CharField(max_length=1, blank=True)
    created = models.DateField(null=True, blank=True)
    class Meta:
        db_table = 'smk_lqd_vaucher_code'

class SmkLqdPrizeRegistrationA1(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    username = models.CharField(max_length=90)
    town = models.CharField(max_length=50, blank=True)
    hbn = models.CharField(max_length=50, blank=True)
    signup_date = models.DateField(null=True, blank=True)
    name = models.CharField(max_length=90, blank=True)
    city = models.CharField(max_length=50, blank=True)
    street = models.CharField(max_length=200, blank=True)
    status = models.CharField(max_length=3, blank=True)
    postcode = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=90)
    fraud = models.CharField(max_length=3, blank=True)
    # remote_addr = models.ForeignKey('SmkLqdRemoteAddr', null=True, db_column='remote_addr', related_name='remote_addr', blank=True)
    # remote_addr2 = models.ForeignKey('SmkLqdRemoteAddr', null=True, db_column='remote_addr2', related_name='remote_addr2', blank=True)
    email_link = models.CharField(max_length=255,null=True, blank=True)
    cloud_size = models.CharField(max_length=2, blank=True)
    strength = models.IntegerField(default=0, blank=True)
    class Meta:
        db_table = 'smk_lqd_prize_registration_a1'

class SmkLqdSubsCustomer(models.Model):
    # create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    # write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    username = models.CharField(max_length=90)
    town = models.CharField(max_length=50, blank=True)
    last_order_date = models.DateField(null=True, blank=True)
    name = models.CharField(max_length=90, blank=True)
    city = models.CharField(max_length=50, blank=True)
    street = models.CharField(max_length=200, blank=True)
    postcode = models.CharField(max_length=50, blank=True)
    hbn = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=200, blank=True)
    created_date = models.DateField(null=True, blank=True)
    email = models.CharField(max_length=90)
    first_order_date = models.DateField(null=True, blank=True)
    customer = models.ForeignKey(ResPartner, db_column='customer')
    free_bottle_count = models.IntegerField(default=0)
    introduced_peoples = models.IntegerField(default=0)
    last_free_bottle_count = models.IntegerField(default=0)
    cloud_size = models.CharField(max_length=2, blank=True)
    class Meta:
        db_table = 'smk_lqd_subs_customer'
        
class SmkLqdExpressNotificationEmail(models.Model):    
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    email_from_addr = models.ForeignKey(SmkLqdEmailAccount, null=True, db_column='email_from_addr', blank=True)
    status = models.CharField(max_length=10, blank=True)
    email_template = models.ForeignKey('SmkLqdHtmlTemplates', null=True, db_column='email_template', blank=True)
    email_content = models.TextField()
    email_condition_listb = models.CharField(max_length=10, blank=True)
    email_list = models.CharField(max_length=10, blank=True)
    addressee = models.CharField(max_length=255, blank=True)
    require_addr = models.CharField(max_length=10, blank=True)
    email_server = models.ForeignKey(IrMailServer, null=True, db_column='email_server', blank=True)
    minute = models.CharField(max_length=10, blank=True)
    name = models.CharField(max_length=50, blank=True)
    hour = models.CharField(max_length=10, blank=True)
    email_condition_lista1 = models.CharField(max_length=10, blank=True)
    email_condition_lista2 = models.CharField(max_length=10, blank=True)
    frequency = models.CharField(max_length=10, blank=True)
    email_subject = models.CharField(max_length=255)
    email_condition_listb2 = models.CharField(max_length=10, blank=True)
    email_header = models.CharField(max_length=10, blank=True)
    frequency2 = models.CharField(max_length=10, blank=True)
    beginning = models.CharField(max_length=10, blank=True)
    beginning2 = models.CharField(max_length=10, blank=True)
    class Meta:
        db_table = 'smk_lqd_express_notification_email'


class SmkLqdFraudItem(models.Model):
    #create_uid = models.ForeignKey(ResUsers, null=True, db_column='create_uid', blank=True)
    create_date = models.DateTimeField(null=True, blank=True)
    write_date = models.DateTimeField(null=True, blank=True)
    #write_uid = models.ForeignKey(ResUsers, null=True, db_column='write_uid', blank=True)
    record_id = models.IntegerField(null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    remote_addr = models.ForeignKey('SmkLqdRemoteAddr', null=True, db_column='remote_addr', blank=True)
    name = models.CharField(max_length=255, blank=True)
    member_of = models.CharField(max_length=1, blank=True)
    class Meta:
        db_table = 'smk_lqd_fraud_item'