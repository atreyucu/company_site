import cronjobs

@cronjobs.register
def vaucher_code_20_percent_discount_generator():
    from backend.models import SmkLqdVaucherCode
    import datetime

    import uuid

    code = SmkLqdVaucherCode()

    temp_str = str(uuid.uuid4()).split('-')[-1][0:6]


    code.code = temp_str
    code.percent = 20
    code.expired_on = datetime.date.today() + datetime.timedelta(days=21)
    code.expired = 'n'
    code.created = datetime.date.today()

    code.save()

@cronjobs.register
def vaucher_code_10_percent_discount_generator():
    from backend.models import SmkLqdVaucherCode
    import datetime

    import uuid

    code = SmkLqdVaucherCode()

    temp_str = str(uuid.uuid4()).split('-')[-1][0:6]


    code.code = temp_str
    code.percent = 10
    code.expired_on = datetime.date.today() + datetime.timedelta(days=21)
    code.expired = 'n'
    code.created = datetime.date.today()

    code.save()


@cronjobs.register
def vaucher_code_expirer():
    from backend.models import SmkLqdVaucherCode
    import datetime

    for code in SmkLqdVaucherCode.objects.filter(expired='n'):
        if code.expired_on == datetime.date.today():
            code.expired = 'y'
            code.save()



@cronjobs.register
def automated_email_sender():
    from backend.models import SmkLqdExpressNotificationEmail, SmkLqdPrizeRegistrationA1, SmkLqdPrizeRegistration, SmkLqdSubsCustomer
    from common.emails import send_email
    from common.emails import get_emails_params
    import datetime
    for email in SmkLqdExpressNotificationEmail.objects.filter(status='y', hour=str(datetime.datetime.now().hour), minute=str(datetime.datetime.now().minute)).exclude(email_list='c'):
        if email.email_list == 'd' and  email.email_condition_lista2 == 'ca22' and SmkLqdSubsCustomer.objects.filter(email=reg.email).count() == 0:
            for reg in SmkLqdPrizeRegistrationA1.objects.filter(status='y').filter(fraud='n'):
                freq = int(email.frequency2)
                if (datetime.date.today() - reg.signup_date).days == freq:
                    send_email(email,to=reg.email,params=get_emails_params(registrationa1=reg))
        elif email.email_list == 'd' and  email.email_condition_lista2 == 'ca23' and SmkLqdSubsCustomer.objects.filter(email=reg.email).count() == 0:
            for reg in SmkLqdPrizeRegistrationA1.objects.filter(status='y').filter(fraud='n'):
                freq = int(email.frequency2)
                begin = int(email.beginning2)
                if ((datetime.date.today() - (reg.signup_date + datetime.timedelta(days=begin))).days)%freq == 0:
                    send_email(email,to=reg.email,params=get_emails_params(registrationa1=reg))
        elif email.email_list == 'b' and email.email_condition_listb == 'cb2':
            freq = int(email.frequency)
            for cust in SmkLqdSubsCustomer.objects.all():
                if (datetime.date.today() - cust.created_date).days == freq and (((email.email_condition_listb2 == 'cb1') and cust.introduced_peoples > 1) or ((email.email_condition_listb2 == 'cb2') and cust.introduced_peoples == 0) or email.email_condition_listb2 == 'cb3'):
                    send_email(email,to=cust.email,params=get_emails_params(smkcustomer=cust))
        elif email.email_list == 'b' and email.email_condition_listb == 'cb3':
            freq = int(email.frequency)
            begin = int(email.beginning)
            for cust in SmkLqdSubsCustomer.objects.all():
                if ((datetime.date.today() - (cust.created_date + datetime.timedelta(days=begin))).days)%freq == 0 and (((email.email_condition_listb2 == 'cb1') and cust.introduced_peoples > 1) or ((email.email_condition_listb2 == 'cb2') and cust.introduced_peoples == 0) or email.email_condition_listb2 == 'cb3'):
                    send_email(email,to=cust.email,params=get_emails_params(smkcustomer=cust))

@cronjobs.register
def automated_email_sender2():
    from backend.models import SmkLqdExpressNotificationEmail, SmkLqdPrizeRegistrationA1, SmkLqdPrizeRegistration, SmkLqdSubsCustomer
    from common.emails import send_email
    from common.emails import get_emails_params
    import datetime
    for email in SmkLqdExpressNotificationEmail.objects.filter(status='y').exclude(email_list='c'):
        if email.email_list == 'd' and  email.email_condition_lista2 == 'ca22':
            try:
                for reg in SmkLqdPrizeRegistrationA1.objects.filter(status='y').filter(fraud='n'):
                    freq = int(email.frequency2)
                    print 'Unconfirmed 1' + email.email_subject + ' ' + 'RESTA ' + str((datetime.date.today() - reg.signup_date).days)
                    if (datetime.date.today() - reg.signup_date).days == freq:
                        print 'sent ' + email.email_subject + 'to ' + reg.email
                        send_email(email,to=reg.email,params=get_emails_params(registrationa1=reg))
            except Exception,ex:
                print 'Unconfirmed' + '1' + str(ex)
        elif email.email_list == 'd' and  email.email_condition_lista2 == 'ca23':
            try:
                for reg in SmkLqdPrizeRegistrationA1.objects.filter(status='y').filter(fraud='n'):
                    print 'Unconfirmed 2' + email.email_subject + ' ' + 'RESTA ' + str(((datetime.date.today() - (reg.signup_date + datetime.timedelta(days=begin))).days)%freq)
                    freq = int(email.frequency2)
                    begin = int(email.beginning2)
                    if ((datetime.date.today() - (reg.signup_date + datetime.timedelta(days=begin))).days)%freq == 0:
                        print 'send ' + str(freq) + ' ' + str(begin) + ' ciclo2' + ' ' + str(reg.signup_date)
                        send_email(email,to=reg.email,params=get_emails_params(registrationa1=reg))
            except Exception,ex:
                print 'Unconfirmed' + '2' + str(ex)
        elif email.email_list == 'b' and email.email_condition_listb == 'cb2':
            try:
                freq = int(email.frequency)
                for cust in SmkLqdSubsCustomer.objects.all():
                    print 'Customer1' + email.email_subject + ' ' + 'RESTA ' + str((datetime.date.today() - cust.created_date).days)
                    print "email_condition_listb2: " + email.email_condition_listb2 + " introduced_peoples: " + str(cust.introduced_peoples)
                    if (datetime.date.today() - cust.created_date).days == freq and (((email.email_condition_listb2 == 'cb1') and cust.introduced_peoples > 0) or ((email.email_condition_listb2 == 'cb2') and cust.introduced_peoples == 0) or email.email_condition_listb2 == 'cb3'):
                        print 'Send ' + str(email.email_subject) + 'to ' + 'customer: ' + cust.name
                        send_email(email,to=cust.email,params=get_emails_params(smkcustomer=cust))
            except Exception,ex:
                print 'Customer' + '1' + str(ex)
        elif email.email_list == 'b' and email.email_condition_listb == 'cb3':
            try:
                freq = int(email.frequency)
                begin = int(email.beginning)
                for cust in SmkLqdSubsCustomer.objects.all():
                    print 'Customer2' + email.email_subject + ' ' + 'RESTA ' + str(((datetime.date.today() - (cust.created_date + datetime.timedelta(days=begin))).days)%freq)
                    print "email_condition_listb2: " + email.email_condition_listb2 + " introduced_peoples: " + str(cust.introduced_peoples)
                    if ((datetime.date.today() - (cust.created_date + datetime.timedelta(days=begin))).days)%freq == 0 and (((email.email_condition_listb2 == 'cb1') and cust.introduced_peoples > 0) or ((email.email_condition_listb2 == 'cb2') and cust.introduced_peoples == 0) or email.email_condition_listb2 == 'cb3'):
                        print 'Send ' + str(email.email_subject) + 'to ' + 'customer: ' + cust.name
                        send_email(email,to=cust.email,params=get_emails_params(smkcustomer=cust))
            except Exception,ex:
                print 'Customer' + '2' + str(ex)


@cronjobs.register
def update_cohort():
    from common.models import Profile
    from backend.models import SmkLqdCohort
    import lambert_smokingliquid.settings as settings
    import datetime

    cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE

    cohort_list = range(cohort_time_delta.days/30 + 2)

    del cohort_list[0]

    SmkLqdCohort.objects.all().delete()

    for index in cohort_list:
        cohort_row_arr = [0]*62

        total = 0

        for profile in Profile.objects.filter(cohort_index=index):
            for order_date in profile.monthodered_set.all():
                cohort_row_arr[order_date.month_index] += 1
                total += 1

        cohort_row_arr[60] = index
        cohort_row_arr[61] = total

        str_fields = """month1,
                        month2,
                        month3,
                        month4,
                        month5,
                        month6,
                        month7,
                        month8,
                        month9,
                        month10,
                        month11,
                        month12,
                        month13,
                        month14,
                        month15,
                        month16,
                        month17,
                        month18,
                        month19,
                        month20,
                        month21,
                        month22,
                        month23,
                        month24,
                        month25,
                        month26,
                        month27,
                        month28,
                        month29,
                        month30,
                        month31,
                        month32,
                        month33,
                        month34,
                        month35,
                        month36,
                        month37,
                        month38,
                        month39,
                        month40,
                        month41,
                        month42,
                        month43,
                        month44,
                        month45,
                        month46,
                        month47,
                        month48,
                        month49,
                        month50,
                        month51,
                        month52,
                        month53,
                        month54,
                        month55,
                        month56,
                        month57,
                        month58,
                        month59,
                        month60,
                        cohort_index,
                        total"""


        temp_str = ''

        for value in cohort_row_arr:
            temp_str += "'%s',"%(str(value))

        str_values = temp_str[:len(temp_str)-1]



        str_query = 'INSERT INTO  smk_lqd_cohort(%s) VALUES (%s)'%(str_fields, str_values)

        #print(str_query)

        from django.db import connection, transaction

        cursor = connection.cursor()
        cursor.execute(str_query)
        transaction.commit_unless_managed()

@cronjobs.register()
def create_user():
    from backend.models import  ResPartner
    from backend.models import  ResPartnerTitle, SmkLqdSubsCustomer, SmkLqdInvites, SmkLqdRegionalDivision
    from common.models import Profile, MonthOdered
    from inviter.models import UserInviter
    from django.contrib.auth.models import User
    import lambert_smokingliquid.settings as settings
    import datetime

    email = 'lamblecli@aol.com'
    first_name = 'Clive'
    last_name = 'Lambert'
    street = 'Street test'
    town = 'Town test'
    city = 'City test'
    postcode = '12345'
    country = 'UK'

    if User.objects.filter(email=email).count() == 0:
        django_user = User()
        django_user.username = email
        django_user.first_name = first_name
        django_user.last_name = last_name
        django_user.email = email
        django_user.is_active = True
        django_user.set_password('c1234c')
        django_user.save()

        #customer creation
        customer = ResPartner()
        customer.username = email
        customer.password_blank = '1234passwd'
        customer.title = ResPartnerTitle.objects.all()[0]
        customer.name = '%s %s'%(first_name,last_name)
        customer.active = True
        customer.customer = True
        customer.supplier = False
        customer.save()

        profile = Profile()
        profile.customer = customer
        profile.user = django_user
        profile.reg_division = SmkLqdRegionalDivision.objects.get(id=1)
        cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE
        profile.cohort_index = cohort_time_delta.days/30 + 1
        profile.save()

        month_ordered = MonthOdered()
        month_ordered.profile = profile
        month_ordered.month_index = cohort_time_delta.days/30
        month_ordered.ordered_date = datetime.date.today()
        month_ordered.save()


        smkcustomer = SmkLqdSubsCustomer()
        smkcustomer.email = email
        smkcustomer.name = '%s %s'%(first_name, last_name)
        smkcustomer.last_order_date = datetime.date.today()
        smkcustomer.first_order_date = datetime.date.today()
        smkcustomer.customer = customer
        smkcustomer.username = email
        smkcustomer.postcode = postcode
        smkcustomer.street = street
        smkcustomer.town = town
        smkcustomer.city = city
        smkcustomer.country = country
        smkcustomer.save()


        user_inviter = UserInviter()
        user_inviter.full_name = '%s %s'%(first_name,last_name)
        user_inviter.user_id = django_user.id
        user_inviter.email = email

        invite = SmkLqdInvites()
        import datetime
        invite.sent_on = datetime.date.today()
        invite.sender = customer
        invite.was_followed = False
        invite.channel = 'fb'
        invite.save()

        host_name = 'heroesvape.uk'

        host_name = 'http://%s'%(host_name)

        referal = host_name +  '/invite_proxy/' + str(invite.id)

        user_inviter.referal_code = referal

        user_inviter.save(using='inviter')

@cronjobs.register()
def create_user2():
    from backend.models import  ResPartner
    from backend.models import  ResPartnerTitle, SmkLqdSubsCustomer, SmkLqdInvites, SmkLqdRegionalDivision
    from common.models import Profile, MonthOdered
    from inviter.models import UserInviter
    from django.contrib.auth.models import User
    import lambert_smokingliquid.settings as settings
    import datetime

    email = 'clive.lambert@outlook.com'
    first_name = 'Clive'
    last_name = 'Lambert'
    street = 'Street test'
    town = 'Town test'
    city = 'City test'
    postcode = '12345'
    country = 'UK'

    if User.objects.filter(email=email).count() == 0:
        django_user = User()
        django_user.username = email
        django_user.first_name = first_name
        django_user.last_name = last_name
        django_user.email = email
        django_user.is_active = True
        django_user.set_password('12345')
        django_user.save()

        #customer creation
        customer = ResPartner()
        customer.username = email
        customer.password_blank = '12345'
        customer.title = ResPartnerTitle.objects.all()[0]
        customer.name = '%s %s'%(first_name,last_name)
        customer.active = True
        customer.customer = True
        customer.supplier = False
        customer.save()

        profile = Profile()
        profile.customer = customer
        profile.user = django_user
        profile.reg_division = SmkLqdRegionalDivision.objects.get(id=1)
        cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE
        profile.cohort_index = cohort_time_delta.days/30 + 1
        profile.save()

        month_ordered = MonthOdered()
        month_ordered.profile = profile
        month_ordered.month_index = cohort_time_delta.days/30
        month_ordered.ordered_date = datetime.date.today()
        month_ordered.save()


        smkcustomer = SmkLqdSubsCustomer()
        smkcustomer.email = email
        smkcustomer.name = '%s %s'%(first_name, last_name)
        smkcustomer.last_order_date = datetime.date.today()
        smkcustomer.first_order_date = datetime.date.today()
        smkcustomer.customer = customer
        smkcustomer.username = email
        smkcustomer.postcode = postcode
        smkcustomer.street = street
        smkcustomer.town = town
        smkcustomer.city = city
        smkcustomer.country = country
        smkcustomer.save()


        user_inviter = UserInviter()
        user_inviter.full_name = '%s %s'%(first_name,last_name)
        user_inviter.user_id = django_user.id
        user_inviter.email = email

        invite = SmkLqdInvites()
        import datetime
        invite.sent_on = datetime.date.today()
        invite.sender = customer
        invite.was_followed = False
        invite.channel = 'fb'
        invite.save()

        host_name = 'heroesvape.uk'

        host_name = 'http://%s'%(host_name)

        referal = host_name +  '/invite_proxy/' + str(invite.id)

        user_inviter.referal_code = referal

        user_inviter.save(using='inviter')


@cronjobs.register
def stats_collector():
    from backend.models import SmkLqdBoardOrder, SmkLqdBoardSubscriptions, SmkLqdBoardCancelledGraph2, SmkLqdOrderOrder, SmkLqdSubsCustomer
    from common.models import Profile
    import datetime

    pending_orders = SmkLqdOrderOrder.objects.filter(printed='n').count()
    today_orders = SmkLqdOrderOrder.objects.filter(created_on=datetime.date.today()).count()


    SmkLqdBoardOrder.objects.all().delete()
    board_order = SmkLqdBoardOrder()
    board_order.pending_orders = pending_orders
    board_order.todays_new_orders = today_orders
    board_order.save()

    SmkLqdBoardSubscriptions.objects.all().delete()
    total_subs = SmkLqdSubsCustomer.objects.all().count()
    today_new_subs = SmkLqdSubsCustomer.objects.filter(first_order_date=datetime.date.today()).count()

    board_subs = SmkLqdBoardSubscriptions()
    board_subs.total_subs = total_subs
    board_subs.todays_new_subs = today_new_subs
    board_subs.save()


    SmkLqdBoardCancelledGraph2.objects.all().delete()

    temp_graph2 = SmkLqdBoardCancelledGraph2()
    temp_graph2.signup_method = 'Through a referral'
    temp_graph2.count = Profile.objects.filter(follow_referal=True).count()
    temp_graph2.save()

    temp_graph2 = SmkLqdBoardCancelledGraph2()
    temp_graph2.signup_method = 'Signup visiting the site'
    temp_graph2.count = SmkLqdSubsCustomer.objects.all().count() - Profile.objects.filter(follow_referal=True).count()
    temp_graph2.save()

@cronjobs.register
def age_popup_expirer():
    from common.models import AgeVerifyAddr
    import datetime

    for age in AgeVerifyAddr.objects.filter(expire_date=datetime.date.today()):
        age.delete()


@cronjobs.register
def delete_unconfirmed():
    from backend.models import SmkLqdPrizeRegistrationA1
    import datetime

    for reg in SmkLqdPrizeRegistrationA1.objects.all():
        if (datetime.date.today() - reg.signup_date).days == 60:
            reg.delete()


@cronjobs.register
def delete_duplicated():
    from backend.models import SmkLqdPrizeRegistrationA1, SmkLqdSubsCustomer

    for reg in SmkLqdPrizeRegistrationA1.objects.filter(fraud=True):
        if SmkLqdSubsCustomer.objects.filter(email=reg.email).count()>0:
            cust = SmkLqdSubsCustomer.objects.get(email=reg.email)
            cust.delete()


@cronjobs.register
def order_totals_resume():
    from backend.models import SmkLqdFundsTotal, SmkLqdOrderOrder
    import datetime

    SmkLqdFundsTotal.objects.all().delete()

    this_month_date =  datetime.date.today()

    temp_first = this_month_date.replace(day=1)

    last_month_date = temp_first - datetime.timedelta(days=1)

    temp_first = last_month_date.replace(day=1)

    last_month_date2 = temp_first - datetime.timedelta(days=1)

    temp_first = last_month_date2.replace(day=1)

    last_month_date3 = temp_first - datetime.timedelta(days=1)

    this_month = this_month_date.month
    this_month_str = this_month_date.strftime('%B')
    last_month = last_month_date.month
    last_month_str = last_month_date.strftime('%B')
    last_month2 = last_month_date2.month
    last_month2_str = last_month_date2.strftime('%B')
    last_month3 = last_month_date3.month
    last_month3_str = last_month_date3.strftime('%B')

    this_month_total = 0
    for order in SmkLqdOrderOrder.objects.filter(created_on__month=this_month):
        this_month_total+=order.order_total_percent

    last_month_total = 0
    for order in SmkLqdOrderOrder.objects.filter(created_on__month=last_month):
        last_month_total+=order.order_total_percent

    last_month2_total = 0
    for order in SmkLqdOrderOrder.objects.filter(created_on__month=last_month2):
        last_month2_total+=order.order_total_percent

    last_month3_total = 0
    for order in SmkLqdOrderOrder.objects.filter(created_on__month=last_month3):
        last_month3_total+=order.order_total_percent

    total1 = SmkLqdFundsTotal()
    total1.month = this_month_str
    total1.total = this_month_total
    total1.save()

    total2 = SmkLqdFundsTotal()
    total2.month = last_month_str
    total2.total = last_month_total
    total2.save()

    total3 = SmkLqdFundsTotal()
    total3.month = last_month2_str
    total3.total = last_month2_total
    total3.save()

    total4 = SmkLqdFundsTotal()
    total4.month = last_month3_str
    total4.total = last_month3_total
    total4.save()


@cronjobs.register
def update_order_regional_divisions():
    from backend.models import SmkLqdOrderOrder, ResPartner

    for user in ResPartner.objects.all():
        if user.customer and user.profile_set.all()[0].reg_division.id != 1:
            SmkLqdOrderOrder.objects.filter(customer_email=user.username).update(regional_division=user.profile_set.all()[0].reg_division)


@cronjobs.register
def funds_raised_stats():
    from common.models import FundsRaisedStats

    from backend.models import SmkLqdOrderOrder, SmkLqdRegionalDivision
    import datetime

    FundsRaisedStats.objects.all().delete()

    divisions2 = SmkLqdRegionalDivision.objects.exclude(id=1)

    this_month = datetime.date.today().month

    first = datetime.date.today().replace(day=1)

    last_month = (first - datetime.timedelta(days=1)).month

    for div in divisions2:
        this_month_orders = SmkLqdOrderOrder.objects.filter(regional_division=div).filter(created_on__month=this_month)
        last_month_orders = SmkLqdOrderOrder.objects.filter(regional_division=div).filter(created_on__month=last_month)

        this_month_value = 0.00
        last_month_value=0.00

        for order in this_month_orders:
            this_month_value += order.order_total_percent

        for order in last_month_orders:
            last_month_value += order.order_total_percent

        stats_obj = FundsRaisedStats()
        stats_obj.name = div.name
        stats_obj.this_month = this_month_value
        stats_obj.last_month = last_month_value
        stats_obj.save()


@cronjobs.register
def delete_expirer_vaucher_code():
    from backend.models import SmkLqdVaucherCode

    SmkLqdVaucherCode.objects.filter(expired='y').delete()

@cronjobs.register
def update_price():
    from backend.models import ProductProduct

    for prod in ProductProduct.objects.all():
        price = prod.product_tmpl.list_price
        id = prod.id

        from django.db import connection

        print 'update product_product set price_extra=%s where id=%s;\n'%(str(price),str(id))

        cursor = connection.cursor()

        cursor.execute('update product_product set price_extra=%s where id=%s'%(str(price),str(id)))