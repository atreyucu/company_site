from django.conf.urls import patterns

urlpatterns = patterns('',
   (r'^$', 'backend.views.home',{'SSL':True}),
   # (r'^your_details/$', 'backend.views.your_details',{'SSL':True}),
   (r'^devices/$', 'backend.views.devices',{'SSL':True}),
   (r'^diy_liquid/$', 'backend.views.diy_liquid',{'SSL':True}),
   (r'^funds_raised/$', 'backend.views.funds_raised',{'SSL':True}),
   (r'^eliquid-10ml/$', 'backend.views.order_now',{'SSL':True}),
   (r'^eliquid-30ml/$', 'backend.views.eliquid_30',{'SSL':True}),
   (r'^order_now2/$', 'backend.views.order_now2',{'SSL':True}),
   (r'^save_your_details/$', 'backend.views.save_your_details',{'SSL':True}),
   (r'^contact_us/$', 'backend.views.contact_us',{'SSL':True}),
   (r'^save_conact_us/$', 'backend.views.save_conact_us',{'SSL':True}),
   (r'^your_details/$', 'backend.views.free_liquid',{'SSL':True}),
   (r'^twitter_popup$', 'backend.views.twitter_popup',{'SSL':True}),
   (r'^refresh_strength/(?P<flavour>\w+)$','backend.views.get_flavours',{'SSL':True}),
   (r'^update_strength/(?P<f_id>\d+)$','backend.views.update_user_free_strength',{'SSL':True}),
   (r'^add2cart/(?P<item_id>\d+)/(?P<item_type>\d{1})$','backend.views.add2cart',{'SSL':True}),
   (r'^update_item_list/$', 'backend.views.update_item_list',{'SSL':True}),
   (r'^update_cart_total/$', 'backend.views.update_cart_total',{'SSL':True}),
   (r'^update_cart_delivery/$', 'backend.views.update_delivery_cost',{'SSL':True}),
   (r'^update_total_saving/$', 'backend.views.calc_saving_total',{'SSL':True}),
   (r'^validate_code/(?P<code>\w+)$','backend.views.validate_code',{'SSL':True}),
   (r'^remove_from_cart/(?P<item_id>\d+)$','backend.views.remove_from_cart',{'SSL':True}),
   (r'^flavours_from_cloud/(?P<cloud>\w+)/(?P<flavour>\w+)$','backend.views.get_flavours_from_cloud_group',{'SSL':True}),
   (r'^check_product_availability/(?P<p_id>\d+)$','backend.views.product_availability',{'SSL':True}),
   (r'^get_product_description/(?P<p_id>\d+)$','frontend.views.get_product_description',{'SSL':True}),
   (r'^strengths_from_product/(?P<tmpl_id>\d+)$','frontend.views.get_strengths_from_product',{'SSL':True}),
   (r'^stablish_division/(?P<div_id>\d+)$','backend.views.stablish_division',{'SSL':True}),
   (r'^get_division_information/(?P<div_id>\d+)$','backend.views.get_division_information',{'SSL':True}),
)
