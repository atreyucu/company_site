#Signals
from django.db.models.signals import post_save
from django.dispatch import receiver
from common.emails import send_email

from models import SmkLqdPrizeRegistrationA1, SmkLqdExpressNotificationEmail, SmkLqdPrizeRegistration, SmkLqdSubsCustomer



@receiver(post_save,sender=SmkLqdPrizeRegistrationA1)
def email_lista1_handler(sender, instance, using, **kwargs):
    if not instance.fraud:
        emails = SmkLqdExpressNotificationEmail.objects.filter(email_list='a').filter(email_condition_lista1='ca11').filter(status='y')

        for email in emails:
            send_email(email,to=instance.email)

@receiver(post_save,sender=SmkLqdPrizeRegistration)
def email_lista2_handler(sender, instance, using, **kwargs):
    emails = SmkLqdExpressNotificationEmail.objects.filter(email_list='d').filter(email_condition_lista1='ca21').filter(status='y')

    for email in emails:
        send_email(email, to=instance.email)


@receiver(post_save,sender=SmkLqdSubsCustomer)
def email_listab_handler(sender, instance, using, **kwargs):
    emails = SmkLqdExpressNotificationEmail.objects.filter(email_list='b').filter(email_condition_lista1='cb1').filter(status='y')

    for email in emails:
        send_email(email, to=instance.email)

