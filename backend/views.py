from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from backend.models import  ResPartner, ResCountry, ResPartnerTitle, ResCountryState, SmkLqdMessage


@login_required()
def home(request):
    import lambert_smokingliquid.settings as settings
    from backend.models import SmkLqdInvites
    fb_app_id = settings.INV_FB_APP_ID
    fb_app_dialog_img = settings.INV_FB_DIALOG_IMG
    invite = SmkLqdInvites()
    import datetime
    invite.sent_on = datetime.date.today()
    invite.sender = request.user.profile.customer
    invite.was_followed = False
    invite.channel = 'fb'
    invite.save()
    host_name = request.META['HTTP_HOST']
    if request.is_secure():
        host_name = 'https://%s'%(host_name)
    else:
        host_name = 'http://%s'%(host_name)
    absolute_url = host_name +  '/invite/' + str(invite.id)
    data = {'fb_app_id': fb_app_id, 'fb_invite_id':str(invite.id),'fb_app_dialog_img': fb_app_dialog_img, 'host_name': host_name, 'absolute_url': absolute_url}

    return render_to_response('users_area/index.html',{'fb_data': data}, context_instance=RequestContext(request))

@login_required()
def order_now(request):
   from backend.models import ProductProduct

   menthol_strength = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,cloud_category='2').order_by('id')
   apple_strength = ProductProduct.objects.filter(name_template='Apple',is_flavour=True,cloud_category='2').order_by('id')
   orange_strength = ProductProduct.objects.filter(name_template='Chocolate',is_flavour=True,cloud_category='2').order_by('id')
   currant_strength = ProductProduct.objects.filter(name_template='Tobacco',is_flavour=True,cloud_category='2').order_by('id')
   coffee_strength = ProductProduct.objects.filter(name_template='Coffee',is_flavour=True,cloud_category='2').order_by('id')
   cola_strength = ProductProduct.objects.filter(name_template='Cola',is_flavour=True,cloud_category='2').order_by('id')
   vanilla_strength = ProductProduct.objects.filter(name_template='Vanilla',is_flavour=True,cloud_category='2').order_by('id')
   aniseed_strength = ProductProduct.objects.filter(name_template='Aniseed',is_flavour=True,cloud_category='2').order_by('id')


   cloud_groups = {'1': 'BIG Cloud 30/70 PG/VG', '2': 'Normal Cloud 70/30 PG/VG'}

   if(request.session.has_key('cart')):
       cart = request.session['cart']
       cart_total = cart['display_total']
       selected_flavours = cart['flavours']
       selected_flavours2 = cart['flavours2']
       additional = cart['additional']
   else:
       cart_total = '0.00'
       selected_flavours = {}
       selected_flavours2 = {}
       additional = {}

   code = cart['discount_code']

   count = cart['count']

   from backend.models import SmkLqdVaucherCode

   valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

   request.session['from_page'] = request.path_info

   from backend.models import ProductProduct

   product = ProductProduct.objects.filter(is_flavour=True)[0]

   temp_total =  float(product.price_extra)*(cart['flavour_count'])

   product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

   temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

   total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

   from common.weights import calc_delivery_cost

   delivery_cost = 0

   if len(additional.keys()) == 0:
       delivery_cost += float(0.00)
   elif cart['diy_weight'] == 0:
       delivery_cost = 0.00
   else:
       delivery_cost = calc_delivery_cost(cart['diy_weight'])

   return render_to_response('users_area/order_now.html', {'is_logged_in': request.user.is_authenticated(),
                                                           'menthol':menthol_strength,
                                                           'apple': apple_strength,
                                                           'orange': orange_strength,
                                                           'currant': currant_strength,
                                                           'coffee': coffee_strength,
                                                           'cola': cola_strength,
                                                           'vanilla': vanilla_strength,
                                                           'aniseed': aniseed_strength,
                                                           'cart_total':cart_total,
                                                           'selected_flavours':selected_flavours,
                                                           'selected_flavours2':selected_flavours2,
                                                           'additional':additional,
                                                           'code': code,
                                                           'valid_code':valid_code,
                                                           'cloud_groups': cloud_groups,
                                                           'count':count,
                                                           'delivery_cost': '%.2f'%delivery_cost,
                                                           'total_saving':'%.2f'%total_saving}, context_instance = RequestContext(request))



@login_required()
def order_now2(request):
    if request.session.has_key('from_page'):
        from_page = request.session['from_page']

        del request.session['from_page']

        if (request.session.has_key('cart')):
            cart = request.session['cart']
            cart_total = cart['display_total']
            selected_flavours = cart['flavours']
            selected_flavours2 = cart['flavours2']
            additional = cart['additional']
        else:
            cart_total = '0.00'
            selected_flavours = {}
            selected_flavours2 = {}
            additional = {}

        from backend.models import ProductProduct

        products = ProductProduct.objects.filter(is_other_suplements=True).order_by('smk_order')

        count = cart['count']

        from backend.models import SmkLqdVaucherCode

        valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

        from backend.models import ProductProduct

        product = ProductProduct.objects.filter(is_flavour=True)[0]

        temp_total =  float(product.price_extra)*(cart['flavour_count'])

        product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

        temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

        total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

        from common.weights import calc_delivery_cost

        delivery_cost = 0

        if len(additional.keys()) == 0:
            delivery_cost += float(0.00)
        elif cart['diy_weight'] == 0:
            delivery_cost = 0.00
        else:
            delivery_cost = calc_delivery_cost(cart['diy_weight'])

        return render_to_response('users_area/order_now2.html', {'is_logged_in': request.user.is_authenticated(),
                                                                 'cart_total': cart_total,
                                                                 'selected_flavours': selected_flavours,
                                                                 'selected_flavours2': selected_flavours2,
                                                                 # 'vaporizer': vaporizer,
                                                                 # 'smoking_device': smoking_device,
                                                                 'count': count,
                                                                 'products':products,
                                                                 'from_page': from_page,
                                                                 'valid_code':valid_code,
                                                                 'total_saving':'%.2f'%total_saving,
                                                                 'delivery_cost': '%.2f'%delivery_cost,
                                                                 'additional': additional, },
                                  context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('/usersadmin/eliquid')

@login_required()
def free_liquid(request):
    message = ''
    error_message = False
    if request.session.has_key('your_details_msg'):
        message = request.session['your_details_msg']
        del request.session['your_details_msg']

    if request.session.has_key('error_message'):
        error_message = request.session['error_message']
        del request.session['error_message']

    profile = request.user.profile
    customer = profile.customer
    email = customer.username

    from backend.models import SmkLqdInvites, ProductProduct
    introduced = request.user.profile.customer.smklqdsubscustomer_set.all()[0].introduced_peoples
    last_free_bottles = request.user.profile.customer.smklqdsubscustomer_set.all()[0].last_free_bottle_count
    free_bottles = request.user.profile.customer.smklqdsubscustomer_set.all()[0].free_bottle_count

    from common.models import FreeSelected

    free_selected = FreeSelected.objects.filter(user=request.user)

    if free_selected:
        free_selected = free_selected[0]

        product = free_selected.flavour.name_template

        strength = free_selected.flavour.id

        cloud = free_selected.cloud

        flavours = ProductProduct.objects.filter(name_template=product,is_flavour=True,cloud_category=cloud).order_by('id')
    else:
        product = ''

        strength = 0

        cloud = ''

        flavours = None

    cloud_groups = {'1': 'BIG Cloud 30/70 PG/VG', '2': 'Normal Cloud 70/30 PG/VG'}

    return render_to_response('users_area/free_liquid.html',{'introduced':introduced,
                                                             'last_free_bottles':last_free_bottles,
                                                             'free_bottles':free_bottles,
                                                             'strength_selected':strength,
                                                             'cloud_selected': cloud,
                                                             'product':product,
                                                             'flavours': flavours,
                                                             'email':email,
                                                             'cloud_groups': cloud_groups,
                                                             'message': message,
                                                             'error_message': error_message,
                                                             }, context_instance=RequestContext(request))

@login_required()
def get_flavours(request,flavour):
    if request.is_ajax():
        from backend.models import ProductProduct
        strength = ProductProduct.objects.filter(name_template=flavour,is_flavour=True).order_by('id')
        return render_to_response('users_area/strengths_part.html',{'flavours':strength})
    else:
        return HttpResponse('Wrong Request')

@login_required()
def update_user_free_strength(request,f_id):
    if request.is_ajax():
        from backend.models import ProductProduct
        prod = ProductProduct.objects.get(id=f_id)

        from common.models import FreeSelected

        if FreeSelected.objects.filter(user=request.user).count() == 0:
            free_selected = FreeSelected()
            free_selected.flavour = prod
            free_selected.stregth = prod.product_strength
            free_selected.user = request.user
            free_selected.cloud = prod.cloud_category
            free_selected.save()
        else:
            free_selected = FreeSelected.objects.get(user=request.user)
            free_selected.flavour = prod
            free_selected.stregth = prod.product_strength
            free_selected.cloud = prod.cloud_category
            free_selected.save()



        import json

        dict = {'success':True}

        return HttpResponse(json.dumps(dict),mimetype='application/json')
    else:
        return HttpResponse('Wrong Request')
   

@login_required()
def your_details(request):
    countries = ResCountry.objects.all()
    countries_data = []

    for c in countries:
        countries_temp = {}
        countries_temp['codeid'] = c.id
        countries_temp['code_name'] = c.name
        countries_data.append(countries_temp)

    titles = ResPartnerTitle.objects.filter(domain='smoking')
    titles_data = []
    for t in titles:
        titles_data_temp = {}
        titles_data_temp['titleid'] = t.id
        titles_data_temp['title'] = t.shortcut
        titles_data.append(titles_data_temp)


    profile = request.user.profile
    customer = profile.customer
    ResPartner().respartneraddress_set.filter(type='delivery')

    private_address = customer.respartneraddress_set.filter(type='default')
    private_address_data = {}
    if len(private_address) > 0:
        private_address = private_address[0]
        private_address_data['email'] = customer.username
         #your_address = ResPartnerAddress()
        private_address_data['your_title'] = private_address.title_id
        private_address_data['your_firstname'] = private_address.name
        private_address_data['your_secondname'] = private_address.second_name
        private_address_data['your_hbn'] = private_address.hbn
        private_address_data['your_streetaddress'] = private_address.street
        private_address_data['your_secondlineaddress'] = private_address.street2
        private_address_data['your_towncity'] = private_address.city
        private_address_data['your_county'] = private_address.state_id
        private_address_data['your_country'] = private_address.country_id
        private_address_data['your_postcode'] = private_address.zip

        all_your_county = ResCountryState.objects.filter(country_id = private_address.country_id)
        all_your_county_data = []
        for yc in all_your_county:
            all_your_county_temp = {}
            all_your_county_temp['codeid'] = yc.id
            all_your_county_temp['code_name'] = yc.name
            all_your_county_data.append(all_your_county_temp)

    delivery_address = customer.respartneraddress_set.filter(type='delivery')
    delivery_address_data = {}
    all_delivery_county_data = []
    if len(delivery_address) > 0:
        delivery_address = delivery_address[0]
        delivery_address_data['delivery_title'] = delivery_address.title_id
        delivery_address_data['delivery_firstname'] = delivery_address.name
        delivery_address_data['delivery_secondname'] = delivery_address.second_name
        delivery_address_data['delivery_hbn'] = delivery_address.hbn
        delivery_address_data['delivery_streetaddress'] = delivery_address.street
        delivery_address_data['delivery_secondlineaddress'] = delivery_address.street2
        delivery_address_data['delivery_towncity'] = delivery_address.city
        delivery_address_data['delivery_county'] = delivery_address.state_id
        delivery_address_data['delivery_country'] = delivery_address.country_id
        delivery_address_data['delivery_postcode'] = delivery_address.zip


        all_delivery_county = ResCountryState.objects.filter(country_id = delivery_address.country_id)
        all_delivery_county_data = []
        for yc in all_delivery_county:
            all_delivery_county_temp = {}
            all_delivery_county_temp['codeid'] = yc.id
            all_delivery_county_temp['code_name'] = yc.name
            all_delivery_county_data.append(all_delivery_county_temp)

    billing_address = customer.respartneraddress_set.filter(type = 'billing')
    billing_address_data = {}
    all_your_county_data = []
    all_billing_county_data = []
    if len(billing_address) > 0:
        billing_address = billing_address[0]
        billing_address_data['billing_title'] = billing_address.title_id
        billing_address_data['billing_firstname'] = billing_address.name
        billing_address_data['billing_secondname'] = billing_address.second_name
        billing_address_data['billing_hbn'] = billing_address.hbn
        billing_address_data['billing_streetaddress'] = billing_address.street
        billing_address_data['billing_secondlineaddress'] = billing_address.street2
        billing_address_data['billing_towncity'] = billing_address.city
        billing_address_data['billing_county'] = billing_address.state_id
        billing_address_data['billing_country'] = billing_address.country_id
        billing_address_data['billing_postcode'] = billing_address.zip


        all_billing_county = ResCountryState.objects.filter(country_id = billing_address.country_id)
        all_billing_county_data = []
        for yc in all_billing_county:
            all_billing_county_temp = {}
            all_billing_county_temp['codeid'] = yc.id
            all_billing_county_temp['code_name'] = yc.name
            all_billing_county_data.append(all_billing_county_temp)

    return render_to_response('users_area/your_details.html',
                              {'countries': countries_data,
                               'titles': titles_data,
                               'private_address': private_address_data,
                               'delivery_address': delivery_address_data,
                               'billing_address': billing_address_data,

                               'your_counties': all_your_county_data,
                               'delivery_counties':all_delivery_county_data,
                               'billing_counties':all_billing_county_data
                              }, context_instance=RequestContext(request))


@login_required()
def save_your_details(request):
    try:
        password_updated = False
        email_updated = False
        if request.method == 'POST':
            django_user = request.user
            profile = django_user.profile
            customer = profile.customer
            smkcustomer = customer.smklqdsubscustomer_set.all()[0]

            if request.POST.has_key('section') and  request.POST['section'] == 'main':
                if request.POST['password'] and request.POST['password'] != '' and request.POST['password'] == request.POST['re-password']:
                    django_user.set_password(request.POST['password'])
                    customer.password_blank = request.POST['password']
                    password_updated = True
                else:
                    request.session['your_details_msg'] = "The passwords didn't match"
                    request.session['error_message'] = True
                    return HttpResponseRedirect('/usersadmin/your_details')


                email =  request.POST['email']


                if email:
                    if UserInviter.objects.filter(user_id=django_user.id).count() > 0:
                        user_inv = UserInviter.objects.get(user_id=django_user.id)
                        user_inv.email = email
                        user_inv.save()

                    django_user.username = request.POST['email']
                    django_user.email = request.POST['email']
                    django_user.save()

                    customer.username = request.POST['email']
                    customer.save()

                    smkcustomer.email = request.POST['email']
                    smkcustomer.save()
                    email_updated = True

                    from inviter.models import UserInviter



        if email_updated and password_updated:
            request.session['your_details_msg'] = "The email and credentials have been updated"
            request.session['error_message'] = False
        elif not email_updated and password_updated:
            request.session['your_details_msg'] = "The credentials have been updated"
            request.session['error_message'] = False
        elif email_updated and not password_updated:
            request.session['your_details_msg'] = "The email have been updated"
            request.session['error_message'] = False

        return HttpResponseRedirect('/usersadmin/your_details')

    except Exception, ex:
        return HttpResponseRedirect('/usersadmin/your_details')

@login_required()
def contact_us(request):
    show_delivery_message = request.session.has_key('show_delivery_message_alert') and request.session['show_delivery_message_alert']

    if request.session.has_key('show_delivery_message_alert'):
        request.session.__delitem__('show_delivery_message_alert')

    return render_to_response('users_area/contact_us.html', {'show_delivery_message': show_delivery_message}, context_instance=RequestContext(request))


@login_required()
def save_conact_us(request):
    subject = request.POST['subject']
    body = request.POST['body']

    try:
        profile = request.user.profile
        customer = profile.customer
    except ObjectDoesNotExist:
        return HttpResponse('Error, please contact with your administrator')

    import datetime

    message = SmkLqdMessage()
    message.body = body
    message.subject = subject
    message.sender = customer
    message.sender_email = customer.username
    message.sender_name = customer.name
    message.from_site = 'sld'
    message.sent_on = datetime.datetime.now()
    message.save()
    request.session['show_delivery_message_alert'] = True
    # from common.emails import send_email

    # send_email('new_message')
    return redirect('/usersadmin/contact_us/')


@login_required()
def twitter_popup(request):
    if(request.is_ajax()):
        try:
            profile = request.user.profile
            customer = profile.customer
        except ObjectDoesNotExist:
            return HttpResponse('Error, please contact with your administrator')


        from backend.models import SmkLqdInvites
        import json
        import datetime

        invite = SmkLqdInvites()
        invite.sent_on = datetime.date.today()
        invite.sender = customer
        invite.followed_on = datetime.date.today()
        invite.was_followed = False
        invite.channel = 'tw'
        invite.save()
        host_name = request.META['HTTP_HOST']
        if request.is_secure():
            host_name = 'https://%s'%(host_name)
        else:
            host_name = 'http://%s'%(host_name)
        result_dict = {'host_name': host_name, 'inv_id': invite.id}
        json = json.dumps(result_dict)
        return HttpResponse(json, content_type="application/json")
    else:
        return HttpResponse('Only ajax request')


def diy_liquid(request):
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct, ProductTemplate

    products_dict = {}

    for tpl in ProductTemplate.objects.all():
        products = ProductProduct.objects.filter(is_diy=True).filter(product_tmpl=tpl).order_by('product_size__id')

        if not products_dict.has_key(tpl.id) and products:
            products_dict[tpl.id] = [tpl.name,products[0].product_image,tpl.description,products]

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    request.session['from_page'] = request.path_info

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('users_area/diy_liquid.html', {'is_logged_in': request.user.is_authenticated(),
                                                             'cart_total': cart_total,
                                                             'selected_flavours': selected_flavours,
                                                             'selected_flavours2': selected_flavours2,
                                                             # 'vaporizer': vaporizer,
                                                             # 'vaporizer_availability' : vaporizer_availability,
                                                             # 'smoking_device': smoking_device,
                                                             # 'smoking_device_availablity': smoking_device_availablity,
                                                             'products': products_dict,
                                                             'count': count,
                                                             'valid_code':valid_code,
                                                             'total_saving':'%.2f'%total_saving,
                                                             'delivery_cost': '%.2f'%delivery_cost,
                                                             'additional': additional}, context_instance = RequestContext(request))


def devices(request):
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct

    products = ProductProduct.objects.filter(is_device=True).order_by('smk_order')

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('users_area/devices.html', {'is_logged_in': request.user.is_authenticated(),
                                                          'cart_total': cart_total,
                                                          'selected_flavours': selected_flavours,
                                                          'selected_flavours2': selected_flavours2,
                                                          # 'vaporizer': vaporizer,
                                                          # 'vaporizer_availability' : vaporizer_availability,
                                                          # 'smoking_device': smoking_device,
                                                          # 'smoking_device_availablity': smoking_device_availablity,
                                                          'products': products,
                                                          'count': count,
                                                          'valid_code':valid_code,
                                                          'total_saving':'%.2f'%total_saving,
                                                          'delivery_cost': '%.2f'%delivery_cost,
                                                          'additional': additional},
                              context_instance=RequestContext(request))



def add2cart(request,item_id,item_type):
    import json

    if request.is_ajax():
        cart = request.session['cart']

        from backend.models import ProductProduct, SmkLqdVaucherCode

        cloud_groups = {'1': '30/70', '2': '70/30'}


        if(int(item_type) == 1):
            product = ProductProduct.objects.get(id=item_id)

            flavours = cart['flavours']

            if(flavours.has_key(item_id)):
                flavours[item_id]['count'] += 1;
            else:
                flavours[item_id] = {'name':'%s-%s-%s'%(product.product_tmpl.description, cloud_groups[product.cloud_category] , product.product_strength.display_name),'count': 1, 'price':'%.2f'%product.price_extra}

            cart['flavours'] = flavours

            cart['count'] += 1

            cart['flavour_count'] += 1

            if(cart['discount_code'] != ''):
                valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

                if(valid_code):
                    code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                    percent = code.percent

                    discount_value = 10*(100-(percent+0.1))/100

                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            else:
                if cart['flavour_count'] % 4 != 0:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                else:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        elif(int(item_type) == 2):
            product = ProductProduct.objects.get(id=item_id)

            flavours = cart['flavours2']

            if(flavours.has_key(item_id)):
                flavours[item_id]['count'] += 1;
            else:
                flavours[item_id] = {'name':'%s-%s'%(product.product_tmpl.name, product.product_strength.display_name),'count': 1, 'price':'%.2f'%product.price_extra}

            cart['flavours2'] = flavours

            cart['count'] += 1

            cart['flavour2_count'] += 1

            if cart['flavour2_count'] % 3 != 0:
                cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3) + float(product.price_extra)*(cart['flavour2_count'] % 3)
            else:
                cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        elif(int(item_type) == 3):
            product = ProductProduct.objects.get(id=int(item_id))

            additional = cart['additional']

            from backend.models import SmkLqdWeightsRanges

            last_weight = SmkLqdWeightsRanges.objects.all().order_by('-initial_weight')[0]

            if cart['diy_weight'] + float(product.smk_weight) > float(last_weight.final_weight):
                response_dict = {'success': False, 'count': cart['count'], 'msg': 'Not available product ammount, please contact us for a quote', 'diy_ex':True}

                reponse_json = json.dumps(response_dict)

                return HttpResponse(reponse_json,mimetype='application/json')


            if(additional.has_key(item_id)):
                 additional[item_id]['count'] += 1;
            else:
                 additional[item_id] = {'name':'%s-%s'%(product.name_template,product.product_size.display_name),'count': 1, 'price':'%.2f'%product.price_extra}

            cart['additional'] = additional

            cart['count'] += 1

            cart['additional_total'] += float(product.price_extra)

            temp_weight =  cart['diy_weight']

            from common.weights import calc_delivery_cost

            if temp_weight == 0:
                old_delivery_cost = 0
            else:
                old_delivery_cost = calc_delivery_cost(temp_weight)

            cart['diy_weight'] += float(product.smk_weight)

            cart['additional_total'] -= float(old_delivery_cost)

            cart['additional_total'] += float(calc_delivery_cost(cart['diy_weight']))

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']
        else:
            product = ProductProduct.objects.get(id=int(item_id))

            additional = cart['additional']


            if(additional.has_key(item_id)):
                 additional[item_id]['count'] += 1;
            else:
                 additional[item_id] = {'name':product.name_template,'count': 1, 'price':'%.2f'%product.price_extra}

            cart['additional'] = additional

            cart['count'] += 1

            cart['additional_total'] += float(product.price_extra)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        request.session['cart'] = cart

        response_dict = {'success': True, 'count': cart['count']}

        reponse_json = json.dumps(response_dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def update_item_list(request):
    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']
            selected_flavours = cart['flavours']
            selected_flavours2 = cart['flavours2']
            additional = cart['additional']
        else:
            selected_flavours = {}
            selected_flavours2 = {}
            additional = {}

        return render_to_response('common/basket_item_list.html',{'selected_flavours':selected_flavours,
                                                                  'selected_flavours2':selected_flavours2,
                                                          'additional':additional}, context_instance = RequestContext(request))
    else:
        return HttpResponse('Wrong Request...')

def update_cart_total(request):
    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']
            total = cart['display_total']
        else:
            total = 0.00

        return render_to_response('common/basket_order_total.html',{'cart_total':total,}, context_instance = RequestContext(request))
    else:
        return HttpResponse('Wrong Request...')

def calc_saving_total(request):
    if request.is_ajax():
        from backend.models import SmkLqdVaucherCode

        cart = request.session['cart']

        valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

        request.session['order_now'] = True

        from backend.models import ProductProduct

        product = ProductProduct.objects.filter(is_flavour=True)[0]

        temp_total =  float(product.price_extra)*(cart['flavour_count'])

        product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

        temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

        total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

        return render_to_response('common/total_saving.html',{'valid_code':valid_code,'total_saving':'%.2f'%total_saving})
    else:
        return HttpResponse('Wrong Request...')

def remove_from_cart(request,item_id):
    from backend.models import ProductProduct, SmkLqdVaucherCode

    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']

            if(cart['flavours'].has_key(item_id)):
                if(cart['flavours'][item_id]['count'] == 1):
                    del cart['flavours'][item_id]
                else:
                    cart['flavours'][item_id]['count'] -= 1

                cart['count'] -= 1
                cart['flavour_count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                #cart['total'] = 0

                if(cart['discount_code'] != ''):
                    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

                    if(valid_code):
                        code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                        percent = code.percent

                        discount_value = 10*(100-(percent+0.1))/100

                        if cart['flavour_count'] % 4 != 0:
                            cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                        else:
                            cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                    else:
                        if cart['flavour_count'] % 4 != 0:
                            cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                        else:
                            cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            if(cart['flavours2'].has_key(item_id)):
                if(cart['flavours2'][item_id]['count'] == 1):
                    del cart['flavours2'][item_id]
                else:
                    cart['flavours2'][item_id]['count'] -= 1

                cart['count'] -= 1
                cart['flavour2_count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                if cart['flavour2_count'] % 3 != 0:
                    cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3) + float(product.price_extra)*(cart['flavour2_count'] % 3)
                else:
                    cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3)


            if(cart['additional'].has_key(item_id)):
                if(cart['additional'][item_id]['count'] == 1):
                    del cart['additional'][item_id]
                else:
                    cart['additional'][item_id]['count'] -= 1

                cart['count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                if(len(cart['additional'].keys()) == 0):
                    cart['additional_total'] = float(0.00)
                    cart['diy_weight'] = float(0.00)
                elif (product.is_diy):
                    temp_weight_value = cart['diy_weight']

                    from common.weights import calc_delivery_cost

                    cart['additional_total'] -= float(calc_delivery_cost(temp_weight_value))

                    cart['diy_weight'] = float(cart['diy_weight']) - float(product.smk_weight)

                    cart['additional_total'] += float(calc_delivery_cost(cart['diy_weight']))

                    cart['additional_total'] -= float(product.price_extra)
                else:
                    cart['additional_total'] -= float(product.price_extra)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']


        import json

        request.session['cart'] = cart

        response_dict = {'success': True, 'count': cart['total']}

        reponse_json = json.dumps(response_dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def validate_code(request, code):
    from backend.models import ProductProduct, SmkLqdVaucherCode

    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']

            temp_response = False

            if code:
                valid_code = SmkLqdVaucherCode.objects.filter(code=code).filter(expired='n').count() > 0


                if(valid_code):
                    cart['discount_code'] = code
                    temp_response = True
                else:
                    cart['discount_code'] = ''
            else:
                cart['discount_code'] = ''



            product = ProductProduct.objects.filter(is_flavour=True)[0]

            if(cart['discount_code'] != ''):
                valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0


                if(valid_code):
                    code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                    percent = code.percent

                    discount_value = 10*(100-(percent+0.1))/100

                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            else:
                if cart['flavour_count'] % 4 != 0:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                else:
                    cart['flavour_total']=  10*(cart['flavour_count'] / 4)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

            import json

            request.session['cart'] = cart

            response_dict = {'success': temp_response}

            reponse_json = json.dumps(response_dict)

            return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')

def get_flavours_from_cloud_group(request,cloud,flavour):
    if request.is_ajax():
        from backend.models import ProductProduct
        flavours = ProductProduct.objects.filter(name_template=flavour,is_flavour=True,cloud_category=cloud).order_by('id')

        return render_to_response('common/flavour_select_options.html',{'flavours':flavours})
    else:
        return HttpResponse('Wrong Request...')

def product_availability(request,p_id):
    if request.is_ajax():
        from backend.models import ProductProduct
        import json

        try:
            product = ProductProduct.objects.get(id=p_id)
        except Exception, ex:
            dict = {'available': False}

            reponse_json = json.dumps(dict)

            return HttpResponse(reponse_json,mimetype='application/json')


        availability =  product.stock_qty > 0

        dict = {'available': availability}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def eliquid_30(request):
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct, ProductStrength, ProductTemplate

    products = ProductProduct.objects.filter(is_flavour2=True)

    products_dict = {}

    for tmpl in ProductTemplate.objects.all():
        if tmpl.id in [product.product_tmpl.id for product in products] and not products_dict.has_key(tmpl.id):
            available_products_count = ProductProduct.objects.filter(product_tmpl=tmpl).filter(stock_qty=0).count()
            all_products_count = ProductProduct.objects.filter(product_tmpl=tmpl).count()
            products_dict[tmpl.id] = [tmpl.name,all_products_count == available_products_count]

    strengths = {}

    for st in ProductStrength.objects.all():
        available_st = ProductProduct.objects.filter(is_flavour2=True).filter(product_strength=st).count()

        if available_st > 0 and not strengths.has_key(st.id):
            strengths[st.id] = st.display_name


    product_img = products[0].product_image

    count = cart['count']

    request.session['from_page'] = request.path_info

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    request.session['from_page'] = request.path_info

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('users_area/eliquid_30ml.html', {'is_logged_in': request.user.is_authenticated(),
                                                               'cart_total': cart_total,
                                                               'selected_flavours': selected_flavours,
                                                               'selected_flavours2': selected_flavours2,
                                                               'products': products_dict,
                                                               'strengths': strengths,
                                                               'count': count,
                                                               'additional': additional,
                                                               'product_img': product_img,
                                                               'valid_code':valid_code,
                                                               'total_saving':'%.2f'%total_saving,
                                                               'delivery_cost': '%.2f'%cart['diy_weight'],
                                                               'show_age_dialog': show_age_dialog}, context_instance = RequestContext(request))


def get_product_description(request,p_id):
    if request.is_ajax():
        from backend.models import ProductTemplate
        import json

        try:
            product = ProductTemplate.objects.get(id=p_id)
        except Exception, ex:
            dict = {'description': ''}

            reponse_json = json.dumps(dict)

            return HttpResponse(reponse_json,mimetype='application/json')


        dict = {'description': product.description}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def get_strengths_from_product(request,tmpl_id):
    if request.is_ajax():
        from backend.models import ProductTemplate, ProductProduct

        products = ProductProduct.objects.filter(product_tmpl=ProductTemplate.objects.get(id=tmpl_id))

        import json

        dict = {}

        for prod in products:
            dict[prod.id] = [prod.product_strength.display_name,prod.stock_qty>0]


        return render_to_response('common/available_strengths.html',{'strengths':dict})
    else:
        return HttpResponse('Wrong Request...')


def funds_raised(request):
    from backend.models import SmkLqdRegionalDivision

    divisions = SmkLqdRegionalDivision.objects.all()

    selected_div = request.user.profile.reg_division

    hide_link = selected_div.id == 1

    from common.models import FundsRaisedStats

    stats_resume = {}

    for funds in FundsRaisedStats.objects.all():
        stats_resume[funds.id] = [funds.name,'%.2f'%funds.this_month,'%.2f'%funds.last_month]


    return render_to_response('users_area/funds_raised.html',{'divisions':divisions, 'hide_link': hide_link, 'selected_div': selected_div, 'stats_resume':stats_resume},context_instance = RequestContext(request))


def stablish_division(request,div_id):
     if request.is_ajax():
        from backend.models import SmkLqdRegionalDivision
        import json

        div = SmkLqdRegionalDivision.objects.get(id=div_id)

        signing_user_prof = request.user.profile

        signing_user_prof.reg_division = div

        signing_user_prof.save()

        dict = {'success': True}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')


     else:
        return HttpResponse('Wrong Request...')


def get_division_information(request, div_id):
    if request.is_ajax():
        from backend.models import SmkLqdRegionalDivision
        import json

        div = SmkLqdRegionalDivision.objects.get(id=div_id)

        dict = {'name': div.name, 'fb_link': div.fb_link}

        reponse_json = json.dumps(dict)
        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')

def update_delivery_cost(request):
    if request.is_ajax():
        delivery_cost = 0

        if(request.session.has_key('cart')):
            cart = request.session['cart']

            additional = cart['additional']

            from common.weights import calc_delivery_cost

            if len(additional.keys()) == 0:
                delivery_cost += float(0.00)
            elif cart['diy_weight'] == 0:
                delivery_cost = 0.00
            else:
                delivery_cost = calc_delivery_cost(cart['diy_weight'])

        import json

        dict = {'delivery_cost': '%.2f'%delivery_cost}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')

    else:
        return HttpResponse('Wrong Request...')
