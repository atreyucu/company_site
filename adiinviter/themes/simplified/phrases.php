<?php

$phrases = array(
	'en' => array(
		'adimt_add_friend_btn_txt' => 'Add',
		'adimt_add_invite_btn_txt' => 'Invite',

		'fa_top_head_in_guest_user_friend' => 'Some of your contacts are already here. Create an account to add them as friends.',
		'fa_top_head_in_guest_user' => 'Some of your contacts are already here.',
		'fa_top_head_in_user_friend' => 'Select which contacts to add as friends from the list below.',
		'fa_top_head_in_user' => 'Some of your contacts are already here.',

		'adimt_choose_another_service_msg' => 'You can also try <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a> to import your contacts.',

		'fa_top_head2_in_guest_user_friend' => 'Some of your contacts are already here. <a href="[register_url]" class="adi_link">Create an account</a> to add them as friends or import your contacts from <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a>.',
		'fa_top_head2_in_guest_user' => 'Some of your contacts are already here. You can also import your contacts from <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a>.',
		'fa_top_head2_in_user_friend' => 'Select which contacts to add as friends from the list below or import your contacts from <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a>.',
		'fa_top_head2_in_user' => 'Some of your contacts are already here. You can also import your contacts from <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a>.',



		'adi_search_user_default_text' => 'Search Contacts..',
		'adi_search_user_submit_btn_text' => 'Go',

		'adi_goto_nonreg_conts_btn_txt' => 'Skip',
		'adi_reimport_btn_txt' => 'Cancel',
		'adi_add_all_as_friends_btn_txt' => 'Add All',


		'adi_view_sample_invitation_link_txt' => 'View <a class="adi_link adi_invite_preview_link" href="#">invitation sample</a>',


		'adi_invite_sender_sub_head_txt' => 'Select which contacts to invite from the list below or import your contacts from <a href="" class="adi_link adi_goto_inviter_page_link">another email account</a>.',

		'adi_invite_all_contacts_btn_txt' => 'Invite All',


		'adi_invite_history_subhead2_txt' => 'Only you can see your imported contacts and sent invitations.',
		'adi_invite_history_download_csv_txt' => ' You can <a href="" class="adi_link adi_download_csv">download</a> your imported contacts in a CSV file.',
		'adi_invite_history_header2_txt' => 'Manage Invites and Contacts',

		'adi_invite_history_no_conts_msg_txt' => 'You have not invited any contacts yet. Click below button to get started.',
		'adi_ih_goto_inpage_model_link_txt' => 'Import Contacts',

		'adi_ih_status_column_txt' => 'Status',
		'adi_ih_issued_date_column_txt' => 'Issued',

		'adi_ih_pagination_note_txt' => 'Page [page_no] of [total_pages]',
		'adi_ih_pagination_loading_txt' => 'Please wait..',

		'adi_import_top_head_txt' => 'Import Your Contacts',
		'adi_import_top_subhead_txt' => 'Get Started By Searching Your Contacts',

		'adi_lg_secutiry_head_txt' => 'Your Contacts Are Safe With Us!',
		'adi_lg_secutiry_paragraph_txt' => 'Import Your Contacts Securely and Email them a Link In 2 EASY Steps (your login details will not be stored).',
		'adi_lg_secutiry_head2_txt' => 'Import Your Contacts Securely and Email them a Link In 2 EASY Steps',
		'adi_lg_secutiry_paragraph2_txt' => '(your login details will not be stored).',

		'adi_other_importer_service_head_txt' => 'Other Email Service',
		'adi_contact_file_importer_head_txt' => 'Upload Contacts File',
		'adi_manual_inviter_head_txt' => 'Enter Contacts Manually',

		'adi_service_importer_head_txt' => '<span class="adi_service_name_val"></span> Account',
		'adi_service_importer_subhead_txt' => 'Login to your <span class="adi_service_name_val adi_fwb"></span> account.',
		'adi_service_importer_subhead2_txt' => 'Login to your <span class="adi_service_name_val adi_fwb"></span> account to begin importing.',

		'adi_service_importer_subhead3_txt' => 'Login to your email account.',
		'adi_service_importer_subhead_alt_txt' => 'Enter your email account details to begin importing.',

		'adi_email_field_default_text' => 'Email Address',
		'adi_password_field_default_text' => 'Password',

		'adi_import_contacts_btn_txt' => 'Import',
		'adi_cancel_btn_txt' => 'Cancel',
		'adi_importi_contacts_loading_text' => 'Importing Contacts..',

		'adi_contact_file_sect_head_txt' => 'Upload Contacts File',
		'adi_contact_file_sect_subhead_txt' => 'CSV, VCF, LDIF, TXT',
		'adi_contact_file_label_txt' => 'Select Contacts File :',

		'adi_manual_inviter_subhead_txt' => 'Enter Your Contacts',
		'adi_manaul_inviter_field_label_txt' => 'Enter comma-separated email addresses of your contacts.',

		'adi_non_reg_search_results_txt' => '<span class="adi_search_results_cnt">0</span> search results for <span class="adi_search_results_query">""</span>',
		'adi_non_reg_reset_search_btn_txt' => 'Back',
		'adi_non_reg_no_results_txt' => '0 Contacts found.',

		'adi_non_reg_pagination_txt' => 'Page <span class="adi_txt adi_current_page">[page_no]</span> of <span class="adi_txt adi_total_pages">[total_pages]</span>',



		'adi_reg_search_results_txt' => '<span class="adi_search_results_cnt">0</span> search results for <span class="adi_search_results_query">""</span>',
		'adi_reg_reset_search_btn_txt' => 'Back',
		'adi_reg_no_results_txt' => '0 Contacts found.',

		'adi_reg_pagination_txt' => 'Page <span class="adi_txt adi_current_page">[page_no]</span> of <span class="adi_txt adi_total_pages">[total_pages]</span>',

		'adi_expand_serv_btn_txt' => 'Import',

		'adi_google_subhead_txt' => 'Import contacts from a Google account.',
		'adi_yahoo_subhead_txt' => 'Import contacts from a Yahoo! account.',
		'adi_outlook_subhead_txt' => 'Import contacts from an Outlook account.',
		'adi_aol_subhead_txt' => 'Import contacts from an AOL account.',
		'adi_mailchimp_subhead_txt' => 'Import contacts from a MailChimp account.',
		'adi_mail_com_subhead_txt' => 'Import contacts from a Mail.com account.',
		'adi_facebook_subhead_txt' => 'Import contacts from a Facebook account.',
		'adi_linkedin_subhead_txt' => 'Import contacts from a Linkedin account.',
		'adi_qq_com_subhead_txt' => 'Import contacts from a QQ.com account.',
		'adi_twitter_subhead_txt' => 'Import contacts from a Twitter account.',

		'adi_other_service_head_txt' => 'Other Account',
		'adi_other_service_subhead_txt' => 'Import contacts from other email account.',

		'adi_contfile_head_txt' => 'Contact File',
		'adi_contfile_subhead_txt' => 'Upload a contacts file from your desktop.',

		'adi_manualinv_head_txt' => 'Enter Contacts',
		'adi_manualinv_subhead_txt' => 'Enter comma-separated email addresses.',

		'adi_linkedin_imp_step1_alt_label' => '1. ',
		'adi_linkedin_imp_step2_alt_label' => '2. ',


		


	),
);


?>