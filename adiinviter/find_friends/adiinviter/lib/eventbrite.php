<?php
/*******************************************************************************************
 * AdiInviter Pro 1.1 Stable Release by AdiInviter, Inc.                                   *
 *-----------------------------------------------------------------------------------------*
 *                                                                                         *
 * https://www.adiinviter.com                                                              *
 *                                                                                         *
 * Copyright � 2008-2014, AdiInviter, Inc. All rights reserved.                            *
 * You may not redistribute this file or its derivatives without written permission.       *
 *                                                                                         *
 * Support Email: support@adiinviter.com                                                   *
 * Sales Email: sales@adiinviter.com                                                       * 
 *                                                                                         *
 *-------------------------------------LICENSE AGREEMENT-----------------------------------*
 * 1. You are allowed to use AdiInviter Pro software and its code only on domain(s) for    * 
 *    which you have purchased a valid and legal license from www.adiinviter.com.          *
 * 2. You ARE NOT allowed to REMOVE or MODIFY the copyright text within the .php files     *
 *    themselves.                                                                          *
 * 3. You ARE NOT allowed to DISTRIBUTE the contents of any of the included files.         *
 * 4. You ARE NOT allowed to COPY ANY PARTS of the code and/or use it for distribution.    *
 ******************************************************************************************/

class eventbrite extends AdiInviter_COR
{
	private $login_ok=false;
	public $showContacts=true;
	protected $timeout=30;
	public $debug_array=array(
			'initial_get'=>'submitForm',
			'login_post'=>'Log out',
			'get_contacts'=>'abCheck'
			);

	public function login($user,$pass)
	{
		$this->resetDebugger();
		$this->service='eventbrite';
		$this->service_user=$user;
		$this->service_password=$pass;

		if (!$this->init()) return false;
		$res = $this->get('https://www.eventbrite.com/login', true);
		preg_match('/csrftoken=([^;]+)/i', $res, $match);
		$this->csrftoken = isset($match[1]) ? $match[1] : '';

		$form_action='https://www.eventbrite.com/ajax/login/';
		$post_elements=array(
			'email'		=> $user,
			'password'	 => $pass,
			'referrer'	 => '',
			'forward'	  => '',
			'remember_me'  => 'false',
			'auth_backend' => 'user_password',
		);
		$headers = array(
			'Host' => 'www.eventbrite.com',
			'Origin' => 'https://www.eventbrite.com',
			'Referer' => 'https://www.eventbrite.com/login/',
			'X-CSRFToken' => $this->csrftoken,
			'X-Requested-With' => 'XMLHttpRequest',
		);
		$res = $this->post($form_action, $post_elements, false,true,false, $headers);
		if(strpos($res, '"success": true') === false) {
			return false;
		}
		$this->login_ok = 'https://www.eventbrite.com/contacts/';
		return true;
	}

	public function getMyContacts()
	{
		$url = $this->login_ok;
		$list_selector = '/lists\:\s*([^\r\n]+)/i';
		eval($this->token_str);
		return $contacts;
	}

	public function logout()
	{
		$logout_url = "https://www.eventbrite.com/logout/";
		$res = $this->get($logout_url,true);
		return true;
	}
}
?>