# Django settings for lambert_smokingliquid project.
import os
import datetime

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'media/templates'),
)

DEBUG = False
TEMPLATE_DEBUG = DEBUG


ENABLED_SSL = True


ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'smoking2',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'smokingadmin',
        'PASSWORD': 'sm0k1ngl1qu1d*-+/',
        'HOST': '127.0.0.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = '/var/www/lambertsmokingliquid.co.uk/web/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT,'media/fonts/'),
    os.path.join(PROJECT_ROOT,'media/css/'),
    os.path.join(PROJECT_ROOT,'media/js/'),
    os.path.join(PROJECT_ROOT,'media/images/'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'p!jk7k6a#&%krm^6yd6+49)5+)b8+@4d^*15t$2_k4tk%5389+'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'common.middlewares.SSLRedirect',
    'common.middlewares.CartChecker',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'lambert_smokingliquid.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'lambert_smokingliquid.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'south',
    'captcha',
    'cronjobs',
    'frontend',
    'backend',
    'common',
)

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

AUTHENTICATION_BACKENDS=('common.auth_backends.GeneralAuthBackend','django.contrib.auth.backends.ModelBackend')

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


CAPTCHA_FONT_SIZE = 28

CAPTCHA_LETTER_ROTATION = (-20,20)

CAPTCHA_NOISE_FUNCTIONS = ""


EMAIL_HOST = '87.117.217.104'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'Smoking_liquid@lambertsmokingliquid.co.uk'
EMAIL_HOST_PASSWORD = 'IWyvqNbJD4'

SLD_FROM_USER = 'Smoking_liquid@lambertsmokingliquid.co.uk'
USE_SSL = False

#defaul value
EMAIL_USE_TLS = False


PUBLIC_SUBJECT_MESSAGE = 'SLD contact us message'

EMAIL_IMAGE = os.path.join(PROJECT_ROOT, 'media/images/email/header_email.jpg')
EMAIL_IMAGE_DIRECTORY_PATH = os.path.join(PROJECT_ROOT, 'media/images/email')

LOGIN_REDIRECT_URL = '/usersadmin/'

PAYPAL_API_USERNAME = 'payment_api1.lambertsmokingliquid.co.uk'
PAYPAL_API_PASSWORD = 'QSYMDHTR4U9EEAHC'
PAYPAL_API_SIGNATURE= 'AVpZ5eBnuH4uKGfbY56UEZ1C0e7lARJaZGKnFaSsXUi1P7eMJjlzXVDZ'


PAYPAL_RETURN_URL = 'http://heroesvape.uk/paypal_return_url'

PAYPAL_FREE_RETURN_URL = 'http://heroesvape.uk/paypal_free_return_url'

PAYPAL_CANCEL_URL = 'http://heroesvape.uk/paypal_cancel_url'

PAYPAL_FREE_CANCEL_URL = 'http://heroesvape.uk/paypal_free_cancel_url'

PAYPAL_EMAIL_PERSONAL = 'Smoking_liquid@lambertsmokingliquid.co.uk'

PAYPAL_API_ENVIRONMENT = 'PRODUCTION'

DEFAULT_ORDER_NUMBER_PER_BATCH = 50

INV_FB_APP_ID = '552873124805448'

INV_FB_DIALOG_IMG = 'http://heroesvape.uk/static/social_invites/facebook.jpg'


COHORT_START_DATE = datetime.date(2015,12,1)



