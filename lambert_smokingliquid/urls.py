from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^', include('frontend.urls')),
                       url(r'^usersadmin/', include('backend.urls')),
                       url(r'^captcha/', include('captcha.urls')),
                       (r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'common/login.html','SSL':True}),
                       (r'^accounts/logout/$', 'common.views.logout_view',{'SSL':True}),
)
