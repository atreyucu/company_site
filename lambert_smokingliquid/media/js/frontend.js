function add2cart(element,item_type){
    var rel_element = $("#" + element.attr('rel-select'));
    if(rel_element.prop('nodeName') == 'SELECT'){
        var str_query = "#" + element.attr('rel-select') + " option:selected";
        var item_id = $(str_query).attr('value');
    }
    else if(rel_element.prop('nodeName') == 'INPUT' && rel_element.attr('type') == 'hidden') {
        item_id = rel_element.val();
    }
    else if(rel_element.prop('nodeName') == 'INPUT' && rel_element.attr('type') == 'text') {
        item_id = rel_element.attr('id');
    }

    if(item_id) {
        $.get('/add2cart/' + item_id + '/' + item_type, function (data) {
            if (data.success) {
                updatecartitems();
                updatecarttotal();
                updatecartdelivery();
                updatetotalsaving();


                if(data.count != 0){
                    $('#procced_button').removeAttr('disabled');
                }

                show_add_message(element,'The product was successfully added.',0);
            }
            else if(!data.sucess && data.diy_ex){
                $('#weight_modal').modal('show');
            }
            else{
                show_add_message(element,'Please select a Liquid Strength from the drop down.',1);
            }
        });
    }
    else{
        show_add_message(element,'Please select a Liquid Strength from the drop down.',1);
    }
}

function updatecartitems(){
    $.get('/update_item_list',function(data){
        $('.cart-list ol').html(data);
    });
}


function updatecarttotal(){
    $.get('/update_cart_total',function(data){
        $('#cart_total').html(data);
    });
}

function updatecartdelivery(){
    $.get('/update_cart_delivery',function(data){
        $('#cart_delivery').html(data.delivery_cost);
    });
}



function updatetotalsaving(){
    if ($('#total_saving').length > 0) {
        $.get('/update_total_saving', function (data) {
            $('#total_saving').html(data);
        });
    }
}



function remove_from_cart(item_id){
    $.get('/remove_from_cart/' + item_id, function(data){
        if(data.success){
            updatecartitems();
            updatecarttotal();
            updatecartdelivery();
            updatetotalsaving();

            if(data.count == 0){
                $('#procced_button').attr('disabled','disabled');
            }
        }
    });
}

function validate_code(){
    var code = $('#input_code').val();

    if (code) {
        $.get('/validate_code/' + code, function (data) {
            if (data.success) {
                updatecarttotal();
                updatecartdelivery();
                updatetotalsaving();
                $('#img_valid_code').show();
            }
            else {
                $('#img_valid_code').hide();
                updatecarttotal();
                updatecartdelivery();
                updatetotalsaving();
            }
        });
    }
}


function show_add_message(element,message,error){
    var scroll_return_top = element.offset().top;
    var scroll_top = $('.cart-list').offset().top - 100;
    $("html, body").animate({
        scrollTop: scroll_top
    },1000);
    if(error == 1){
        $('#message').removeClass('alert-success');
        $('#message').addClass('alert-danger');
    }
    else{
        $('#message').removeClass('alert-danger');
        $('#message').addClass('alert-success');
    }
    $('#message').children('strong').html(message);
    $('#message').fadeIn(2000).delay(5000).fadeOut();

    /*setTimeout(function(){
         $("html, body").animate({
            scrollTop: scroll_return_top
        },1000);
    },8000);*/

}

function get_flavours_from_cloud_group(element){
    var flavour = element.attr('name');
    var cloud = element.val();
    var parent_elemet = element.attr('parent_select');

    $.get('/flavours_from_cloud/'+ cloud + '/' + flavour,function(data){
        $('#' + parent_elemet).html(data);
    });
}

function check_availability(element){
    var pid = element.val();
    var element_id = element.attr('id')
    var button_element = $('a[rel-select='+ element_id +']');

    if(isNaN(pid)){
        button_element.attr('disabled','disabled');
    }
    else{
        $.get('/check_product_availability/'+pid,function(data){
            if(data.available){
                button_element.removeAttr('disabled');
            }
            else{
                button_element.attr('disabled','disabled');
            }
        })
    }
}


function check_availability2(element){
    var pid = element.val();
    var element_id = element.attr('name')
    var button_element = $('#button-add');

    if(isNaN(pid)){
        button_element.attr('disabled','disabled');
    }
    else{
        $.get('/check_product_availability/'+pid,function(data){
            if(data.available){
                button_element.removeAttr('disabled');
            }
            else{
                button_element.attr('disabled','disabled');
            }
        });
    }
}


function check_availability3(element){
    var pid = element.val();
    var element_id = element.attr('name')
    var button_element = $('#button-add2');

    if(isNaN(pid)){
        button_element.attr('disabled','disabled');
    }
    else{
        $.get('/check_product_availability/'+pid,function(data){
            if(data.available){
                button_element.removeAttr('disabled');
            }
            else{
                button_element.attr('disabled','disabled');
            }
        });
    }
}

function get_strengths_from_product(element){
    var tmpl_id = element.val();

    $.get('/strengths_from_product/'+tmpl_id,function(data){
        $('#eliquid30ml_strength').html(data);
        $('#eliquid30ml_strength2').html(data);
    });

    if($("#eliquid30ml_strength option:selected").attr("value")){
        $('#button-add').removeAttr('disabled');
    }
    else{
        $('#button-add').attr('disabled','disabled');
    }

    $.get('/get_product_description/'+tmpl_id,function(data){
            $('#product_description').html(data.description);
    });
}