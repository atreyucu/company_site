$(function(){
    $(".menu-btn").on("click", function(e){
        e.preventDefault();
        $(".index-menu").fadeToggle();
    });
    var box = $(".box-equalized");
    var maxh=0;
    $.each(box, function() {
        if ($(this).height() > maxh) {
            maxh=$(this).height();
        }
    });
    $.each(box, function() {
        $(this).height(maxh);
    });

    /*$(".banner").css({
        "background-image" : "url(" + $(".banner > img").attr("src") + ")"
    });*/

    var row = $('.equalize');
    $.each(row, function() {
        var maxh=0;
        $.each($(this).find('div[class^="col-"]'), function() {
            if($(this).height() > maxh)
                maxh=$(this).height();
        });
        $.each($(this).find('div[class^="col-"]'), function() {
            $(this).height(maxh);
        });
    });
    $(".green-arrow").on("click", function() {
        $("html, body").animate({
            scrollTop: $(this).offset().top + 90
        },1000);
    });

    $('#ageModal').modal('hide');

    //$('#modal_age_ok').attr('href','/validate_age/');
    //$('#modal_age_ok2').attr('href','/validate_age/');

    $('#modal_age_ok').click(function(){
        Cookies.set("HV_AGE_TOKEN","True", {"path":"/"});
        $(location).attr('href','/validate_age/');
    });


    $('#modal_age_ok2').click(function(){
        Cookies.set("HV_AGE_TOKEN","True", {"path":"/"});
        $(location).attr('href','/validate_age/');
    });


    $('#public_contact_submit').click(function(){
        $('#public_contact_form').submit();
    })
});