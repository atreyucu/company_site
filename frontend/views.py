from django.http.response import Http404
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt

def index(request):
    show_age_dialog = request.session['SHOW_AGE_DIALOG']
    # return  HttpResponse(show_age_dialog)
    return render_to_response('frontend/index.html', {'is_logged_in': request.user.is_authenticated(), 'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))

def about(request):
    show_age_dialog = request.session['SHOW_AGE_DIALOG']
    return render_to_response('frontend/about_us.html', {'is_logged_in': request.user.is_authenticated(), 'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))

def order_now(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/eliquid/')
    from backend.models import ProductProduct

    menthol_strength = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,cloud_category='2').order_by('id')
    apple_strength = ProductProduct.objects.filter(name_template='Apple',is_flavour=True,cloud_category='2').order_by('id')
    orange_strength = ProductProduct.objects.filter(name_template='Chocolate',is_flavour=True,cloud_category='2').order_by('id')
    currant_strength = ProductProduct.objects.filter(name_template='Tobacco',is_flavour=True,cloud_category='2').order_by('id')
    coffee_strength = ProductProduct.objects.filter(name_template='Coffee',is_flavour=True,cloud_category='2').order_by('id')
    cola_strength = ProductProduct.objects.filter(name_template='Cola',is_flavour=True,cloud_category='2').order_by('id')
    vanilla_strength = ProductProduct.objects.filter(name_template='Vanilla',is_flavour=True,cloud_category='2').order_by('id')
    aniseed_strength = ProductProduct.objects.filter(name_template='Aniseed',is_flavour=True,cloud_category='2').order_by('id')

    cloud_groups = {'1': 'BIG Cloud 30/70 PG/VG', '2': 'Normal Cloud 70/30 PG/VG'}

    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    code = cart['discount_code']

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    request.session['from_page'] = request.path_info

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('frontend/order_now.html', {'is_logged_in': request.user.is_authenticated(),
                                                          'menthol':menthol_strength,
                                                          'apple': apple_strength,
                                                          'orange': orange_strength,
                                                          'currant': currant_strength,
                                                          'coffee': coffee_strength,
                                                          'cola': cola_strength,
                                                          'vanilla': vanilla_strength,
                                                          'aniseed': aniseed_strength,
                                                          'cart_total':cart_total,
                                                          'selected_flavours':selected_flavours,
                                                          'selected_flavours2':selected_flavours2,
                                                          'additional':additional,
                                                          'code': code,
                                                          'count':count,
                                                          'cloud_groups': cloud_groups,
                                                          'valid_code':valid_code,
                                                          'total_saving':'%.2f'%total_saving,
                                                          'show_age_dialog': show_age_dialog,
                                                          'delivery_cost': '%.2f'%delivery_cost,
                                                          }, context_instance = RequestContext(request))

def order_now2(request):
    if request.session.has_key('from_page'):
        from_page = request.session['from_page']

        del request.session['from_page']

        if(request.session.has_key('cart')):
            cart = request.session['cart']
            cart_total = cart['display_total']
            selected_flavours = cart['flavours']
            selected_flavours2 = cart['flavours2']
            additional = cart['additional']
        else:
            cart_total = '0.00'
            selected_flavours = {}
            selected_flavours2 = {}
            additional = {}

        from backend.models import ProductProduct

        products = ProductProduct.objects.filter(is_other_suplements=True).order_by('smk_order')

        from backend.models import SmkLqdVaucherCode

        valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

        from backend.models import ProductProduct

        product = ProductProduct.objects.filter(is_flavour=True)[0]

        temp_total =  float(product.price_extra)*(cart['flavour_count'])

        product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

        temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

        total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

        count = cart['count']

        # smoking_device = ProductProduct.objects.get(default_code='G-1').id
        #
        # smoking_device_availablity = ProductProduct.objects.get(default_code='G-1').stock_qty > 0
        #
        # vaporizer = ProductProduct.objects.get(default_code='F-1').id
        #
        # vaporizer_availability = ProductProduct.objects.get(default_code='F-1').stock_qty > 0

        show_age_dialog = request.session['SHOW_AGE_DIALOG']

        from common.weights import calc_delivery_cost

        delivery_cost = 0

        if len(additional.keys()) == 0:
            delivery_cost += float(0.00)
        elif cart['diy_weight'] == 0:
            delivery_cost = 0.00
        else:
            delivery_cost = calc_delivery_cost(cart['diy_weight'])


        return render_to_response('frontend/order_now2.html', {'is_logged_in': request.user.is_authenticated(),
                                                               'cart_total':cart_total,
                                                               'selected_flavours':selected_flavours,
                                                               'selected_flavours2':selected_flavours2,
                                                               # 'vaporizer': vaporizer,
                                                               # 'vaporizer_availability' : vaporizer_availability,
                                                               # 'smoking_device': smoking_device,
                                                               # 'smoking_device_availablity': smoking_device_availablity,
                                                               'products': products,
                                                               'count':count,
                                                               'from_page':from_page,
                                                               'additional':additional,
                                                               'valid_code':valid_code,
                                                               'total_saving':'%.2f'%total_saving,
                                                               'delivery_cost': '%.2f'%delivery_cost,
                                                               'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('/eliquid')

def free_bottle(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/')
    if request.method == 'POST':
        name = request.POST['name']
        hbn = request.POST['hbn']
        street = request.POST['street']
        town = request.POST['town']
        city = request.POST['city']
        postcode = request.POST['postcode']
        email = request.POST['email']
        cloud_size = request.POST['cloud_size']
        strength = request.POST['strength']

        from backend.models import SmkLqdPrizeRegistrationA1, SmkLqdRemoteAddr, SmkLqdFraudItem, SmkLqdInvites, ResPartner

        from common.fraud import AnalyticFraud, RemoreAddrUtil

        import datetime

        from backend.models import ProductProduct

        import uuid

        product = ProductProduct.objects.filter(name_template=strength,is_flavour=True,cloud_category=cloud_size).order_by('id')[0]

        registration = SmkLqdPrizeRegistrationA1()
        registration.name = name
        registration.hbn = hbn
        registration.street = street
        registration.signup_date = datetime.date.today()
        registration.town = town
        registration.city = city
        registration.postcode = postcode
        registration.email = email
        registration.username = email
        registration.cloud_size = cloud_size
        registration.strength = product.id

        addr = RemoreAddrUtil().process_request(request)

        if SmkLqdRemoteAddr.objects.filter(addr=addr).count() != 0:
            addr_obj = SmkLqdRemoteAddr.objects.get(addr=addr)
            addr_obj.subscriptions_count += 1
            addr_obj.save()
        else:
            addr_obj = SmkLqdRemoteAddr()
            addr_obj.addr = addr
            addr_obj.subscriptions_count = 1
            addr_obj.save()

        if request.session.has_key('follow_invite'):
            invite = SmkLqdInvites.objects.get(id=request.session['follow_invite'])

            invite.was_followed = True
            invite.followed_on = datetime.datetime.now()
            invite.save()

            customer2 = ResPartner.objects.get(id=invite.sender.id)

            smk_customer2 = customer2.smklqdsubscustomer_set.all()[0]

            smk_customer2.introduced_peoples += 1

            smk_customer2.save()

        fraud_mark = AnalyticFraud().analice_fraud(addr,email,postcode)

        str_code = str(uuid.uuid4()).replace('-','')

        secure = request.is_secure()

        protocol = secure and "https" or "http"

        host_name = request.META['HTTP_HOST']

        free_bottle_link = "%s://%s%s%s" % (protocol,host_name,'/order_free_bottle/',str_code)

        registration.status = 'y'
        registration.email_link = free_bottle_link
        registration.save()

        if fraud_mark:
            registration.fraud = 'y'

            registration.save()

            fitem = SmkLqdFraudItem()
            fitem.email = email
            fitem.name = name
            fitem.member_of = 'u'
            fitem.record_id = registration.id
            fitem.remote_addr = addr_obj
            fitem.save()
        else:
            registration.fraud = 'n'
            registration.save()

        from common.models import UnconfirmedCodes

        str_code2 = str(uuid.uuid4()).replace('-','')

        unco_code = UnconfirmedCodes()
        unco_code.code = str_code2
        unco_code.unconfirmed_id = registration.id
        unco_code.save()


        from common.models import FreeBottleCode

        code = FreeBottleCode()
        code.code = str_code
        code.a1_reg = registration
        code.valid = True
        code.save()

        check_ok = not fraud_mark

        from common.emails import send_email_by_name

        if not fraud_mark:
             send_email_by_name('please_confirm',to=email,params={'link': free_bottle_link})
        else:
            send_email_by_name('fraud_request',to=email,params={})


        request.session['free_bottle'] = check_ok

        return HttpResponseRedirect('/free')

    host_name = request.META['HTTP_HOST']

    if request.is_secure():
        host_name = 'https://%s'%(host_name)
    else:
        host_name = 'http://%s'%(host_name)

    absolute_url = host_name +  '/free_bottle'

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    return render_to_response('frontend/free_bottle.html', {'is_logged_in': request.user.is_authenticated(), 'absolute_url':absolute_url, 'show_age_dialog': show_age_dialog}, context_instance = RequestContext(request))

def contact(request):
    from backend.models import SmkLqdMessage
    from frontend.forms import CaptchaForm
    from lambert_smokingliquid import settings
    import datetime

    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/contact_us/')
    if request.POST:
        form = CaptchaForm(request.POST)
        name = request.POST['name']
        email = request.POST['email']
        message_text = request.POST['message']
        if form.is_valid():
            message = SmkLqdMessage()
            message.body = message_text
            message.subject = settings.PUBLIC_SUBJECT_MESSAGE
            message.sender_email = email
            message.sender_name = name
            message.from_site = 'sld'
            message.is_public = True
            message.sent_on = datetime.datetime.now()
            message.type = 'p'
            message.save()

            from common.emails import send_email_by_name

            send_email_by_name('new_message')

            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 1, 'show_delivery_message': True},context_instance = RequestContext(request))
        else:
            return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': 0, 'name':name, 'email':email, 'message':message_text},context_instance = RequestContext(request))
    else:
        form = CaptchaForm()
        show_age_dialog = request.session['SHOW_AGE_DIALOG']
        return render_to_response('frontend/contact_us.html', {'form': form, 'captcha': -1, 'show_age_dialog':show_age_dialog},context_instance = RequestContext(request))


def passwd_recover(request):
    from backend.models import ResPartner
    from django.core.exceptions import ObjectDoesNotExist
    try:
        if request.POST:
            email_address = request.POST['email']
            customer = ResPartner.objects.get(username = email_address)
            from common.emails import send_email_by_name

            try:
                send_email_by_name('password',to=email_address,params={'username':email_address,'password':customer.password_blank})
            except Exception:
                return HttpResponseRedirect('/')
            return render_to_response("common/login_recover.html", context_instance=RequestContext(request))

        else:
            return render_to_response("common/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))

    except ObjectDoesNotExist, e:
        return render_to_response("common/password_recover.html", {"sucefull":False, 'message': 'The username does not exist'}, context_instance=RequestContext(request))
    except Exception, e:
        return render_to_response("common/password_recover.html", {"sucefull":False}, context_instance=RequestContext(request))

@csrf_exempt
def paypal_cancel_url(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/')
    else:
        return HttpResponseRedirect('/')

@csrf_exempt
def paypal_return_url(request):
    if request.GET.has_key('token') and request.GET.has_key('PayerID'):
        cart = request.session['cart']

        cloud_groups = {'1': '30/70', '2': '70/30'}

        token = request.GET['token']
        payerid = request.GET['PayerID']

        from paypal.exceptions import PayPalAPIResponseError, PayPalConfigError
        from paypal.interface import PayPalInterface
        import common.paypal_conf as paypal_manager

        from common.models import MonthOdered

        from common.emails import send_email_by_name

        interface = PayPalInterface(config=paypal_manager.CONFIG)



        getexp_response = interface.get_express_checkout_details(token=token)

        do_express_response = interface.do_express_checkout_payment(token=token,
                                                                    paymentaction='Sale',
                                                                    payerid=payerid,
                                                                    amt=cart['total'],
                                                                    currencycode='GBP'
                                                                    )

        from django.contrib.auth.models import User
        from backend.models import ResPartner, ResPartnerTitle, SmkLqdSubsFlavours
        from backend.models import SmkLqdOrderOrder, SmkLqdOrderItem, SmkLqdOrderBatch, ProductProduct, SmkLqdRegionalDivision
        from backend.models import SmkLqdPrizeRegistrationA1, SmkLqdSubsCustomer, SmkLqdInvites
        from inviter.models import UserInviter
        from common.models import Profile
        import lambert_smokingliquid.settings as settings
        import datetime

        email = getexp_response.EMAIL
        first_name = getexp_response.FIRSTNAME
        last_name = getexp_response.LASTNAME
        street = getexp_response.PAYMENTREQUEST_0_SHIPTOSTREET
        town = getexp_response.PAYMENTREQUEST_0_SHIPTOCITY
        city = getexp_response.PAYMENTREQUEST_0_SHIPTOCITY
        postcode = getexp_response.PAYMENTREQUEST_0_SHIPTOZIP
        country = getexp_response.PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME

        if User.objects.filter(email=email).count() == 0:
            django_user = User()
            django_user.username = email
            django_user.first_name = first_name
            django_user.last_name = last_name
            django_user.email = email
            django_user.is_active = True
            django_user.set_password('1234passwd')
            django_user.save()

            #customer creation
            customer = ResPartner()
            customer.username = email
            customer.password_blank = '1234passwd'
            customer.title = ResPartnerTitle.objects.all()[0]
            customer.name = '%s %s'%(first_name,last_name)
            customer.active = True
            customer.customer = True
            customer.supplier = False
            customer.save()

            profile = Profile()
            profile.customer = customer
            profile.user = django_user
            cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE
            profile.cohort_index = cohort_time_delta.days/30 + 1
            profile.reg_division = SmkLqdRegionalDivision.objects.get(id=1)
            profile.save()

            month_ordered = MonthOdered()
            month_ordered.profile = profile
            month_ordered.month_index = cohort_time_delta.days/30
            month_ordered.ordered_date = datetime.date.today()
            month_ordered.save()

            smkcustomer = SmkLqdSubsCustomer()
            smkcustomer.email = email
            smkcustomer.name = '%s %s'%(first_name, last_name)
            smkcustomer.last_order_date = datetime.date.today()
            smkcustomer.first_order_date = datetime.date.today()
            smkcustomer.created_date = datetime.date.today()
            smkcustomer.customer = customer
            smkcustomer.username = email
            smkcustomer.postcode = postcode
            smkcustomer.street = street
            smkcustomer.town = town
            smkcustomer.city = city
            smkcustomer.country = country

            SmkLqdPrizeRegistrationA1.objects.filter(email=email).delete()

            smkcustomer.save()

            user_inviter = UserInviter()
            user_inviter.full_name = '%s %s'%(first_name,last_name)
            user_inviter.user_id = django_user.id
            user_inviter.email = email

            invite = SmkLqdInvites()
            invite.sent_on = datetime.date.today()
            invite.sender = customer
            invite.was_followed = False
            invite.channel = 'fb'
            invite.save()

            host_name = request.META['HTTP_HOST']

            host_name = 'http://%s'%(host_name)

            referal = host_name +  '/invite/' + str(invite.id)

            user_inviter.referal_code = referal

            user_inviter.save()

            send_email_by_name('account_details',to=email,params={'password':'1234passwd'})
        else:
            customer = ResPartner.objects.get(username=email)
            smkcustomer = customer.smklqdsubscustomer_set.all()[0]
            smkcustomer.last_order_date = datetime.date.today()
            smkcustomer.save()

            profile = customer.profile_set.all()[0]

            cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE

            month_ordered = MonthOdered()
            month_ordered.profile = profile
            month_ordered.month_index = cohort_time_delta.days/30
            month_ordered.ordered_date = datetime.date.today()
            month_ordered.save()


        order = SmkLqdOrderOrder()
        order.create_date = datetime.datetime.now()
        order.created_on = datetime.date.today()
        order.customer = customer

        from common.weights import calc_delivery_cost

        delivery_cost = calc_delivery_cost(cart['diy_weight'])


        from backend.models import SmkLqdWeightsRanges

        first_weight_range = SmkLqdWeightsRanges.objects.all().order_by('price')[0]

        if cart['diy_weight'] > 2:
            order.print_postage = 'n'
        else:
            order.print_postage = 'y'

        order.customer_delivery_street = street
        order.customer_delivery_city = city
        order.customer_delivery_county = town
        order.customer_delivery_country = country
        order.customer_delivery_postcode = postcode


        order.require_envelope = 'n'
        order.envelope_printed = 'n'
        order.printed = 'n'
        order.manual = 'n'
        order.free_order = 'n'
        order.regional_division = SmkLqdRegionalDivision.objects.get(id=1)
        order.customer_name = '%s %s'%(first_name,last_name)
        import random
        import time

        order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
        order.customer_email = email
        order.customer_fax = ''
        order.customer_mobile = ''
        order.customer_phone = ''
        order.order_total = cart['total']
        order.order_total_percent = cart['total']*5/100

        if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').count() > 0:
            if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].printed == 'y':
                batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0]
                batch.order_count += 1
                batch.save()
            else:
                batch = SmkLqdOrderBatch()
                batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                batch.created_on = datetime.date.today()
                batch.potential_fraud = 'n'
                batch.require_envelope = 'n'
                batch.manual = 'n'
                batch.order_count = 1
                batch.envelope_printed = 'n'
                batch.printed = 'n'
                batch.free_order = 'n'
                batch.save()
        else:
            batch = SmkLqdOrderBatch()
            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            batch.created_on = datetime.date.today()
            batch.potential_fraud = 'n'
            batch.require_envelope = 'n'
            batch.manual = 'n'
            batch.order_count = 1
            batch.envelope_printed = 'n'
            batch.printed = 'n'
            batch.free_order = 'n'
            batch.save()

        order.batch_ids = batch
        order.regional_division = profile.reg_division
        order.save()

        flavours = cart['flavours']

        for key, prod in flavours.iteritems():
            temp_product = ProductProduct.objects.get(id=key)
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = prod['count']
            item.product_name = temp_product.product_tmpl.description +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
            item.product_description = temp_product.product_tmpl.description
            item.save()

        flavours2 = cart['flavours2']

        for key, prod in flavours2.iteritems():
            temp_product = ProductProduct.objects.get(id=key)
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = prod['count']
            item.product_name = temp_product.product_tmpl.name +  '-'  + temp_product.product_strength.display_name
            item.product_description = temp_product.product_tmpl.description
            item.save()

        additional = cart['additional']

        for key, prod in additional.iteritems():
            temp_product = ProductProduct.objects.get(id=key)
            item = SmkLqdOrderItem()
            item.order_ids = order
            item.product = temp_product
            item.line_total = prod['count']
            if temp_product.is_diy:
                item.product_name = temp_product.name_template + '-' + temp_product.product_size.display_name
            else:
                item.product_name = temp_product.name_template
            item.product_description = temp_product.product_tmpl.description
            item.save()


        temp_product = ProductProduct.objects.get(default_code='H-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()

        temp_product = ProductProduct.objects.get(default_code='I-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()

        try:
            if smkcustomer.free_bottle_count > 0:
                from common.models import FreeSelected

                if FreeSelected.objects.filter(user=customer.profile_set.all()[0].user).count() > 0:
                    free_selected = FreeSelected.objects.filter(user=customer.profile_set.all()[0].user)[0]

                    tem_product = free_selected.flavour
                else:
                    tem_product = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True).order_by('id')[0]


                item = SmkLqdOrderItem()
                item.order_ids = order
                item.product = tem_product
                item.line_total = smkcustomer.free_bottle_count
                item.product_name = temp_product.name_template
                item.product_description = temp_product.product_tmpl.description
                item.save()

                smkcustomer.last_free_bottle_count = smkcustomer.free_bottle_count
                smkcustomer.free_bottle_count = 0
                smkcustomer.save()


            send_email_by_name('new_order_received',to=email,params={})

            if profile.affiliate_customer != 0:
                affiliate_customer_owner = ResPartner.objects.get(id=profile.affiliate_customer)

                aff_smk_customer_owner = affiliate_customer_owner.smklqdsubscustomer_set.all()[0]

                aff_smk_customer_owner.free_bottle_count += 1

                aff_smk_customer_owner.save()

                send_email_by_name('another_free_bottle',to=aff_smk_customer_owner.email)

            if request.session.has_key('follow_invite'):
                invite = SmkLqdInvites.objects.get(id=request.session['follow_invite'])

                invite.was_followed = True
                invite.followed_on = datetime.datetime.now()
                invite.save()

                profile.follow_referal = True

                profile.affiliate_customer = invite.sender.id

                profile.save()

                customer2 = ResPartner.objects.get(id=invite.sender.id)

                smk_customer2 = customer2.smklqdsubscustomer_set.all()[0]

                smk_customer2.free_bottle_count += 1

                smk_customer2.introduced_peoples += 1

                smk_customer2.save()

                del request.session['follow_invite']

                send_email_by_name('another_free_bottle',to=smk_customer2.email)
        except Exception, ex:
            if request.user.is_authenticated():
                request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}

                return HttpResponseRedirect('/usersadmin/')
            else:
                from django.contrib.auth import login, authenticate
                authenticated_user = authenticate(username = email, password = customer.password_blank)
                login(request,authenticated_user)

                request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}

                return HttpResponseRedirect('/usersadmin/')

        if request.user.is_authenticated():
            request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}

            return HttpResponseRedirect('/usersadmin/')
        else:
            from django.contrib.auth import login, authenticate
            authenticated_user = authenticate(username = email, password = customer.password_blank)
            login(request,authenticated_user)

            request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}

            return HttpResponseRedirect('/usersadmin/')
    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect('/usersadmin/eliquid/')
        else:
            return HttpResponseRedirect('/eliquid/')

def chackout(request):
    from paypal.exceptions import PayPalAPIResponseError, PayPalConfigError
    from paypal.interface import PayPalInterface
    import common.paypal_conf as paypal_manager

    if(request.session.has_key('cart')):
        cart = request.session['cart']
    else:
        return HttpResponseRedirect('/eliquid')

    if cart['count'] != 0:
        interface = PayPalInterface(config=paypal_manager.CONFIG)

        ammount = str(cart['total'])
        try:
            setexp_response = interface.set_express_checkout(
                    amt=ammount,
                    returnurl=paypal_manager.RETURN_URL, cancelurl=paypal_manager.CANCEL_URL,
                    paymentaction='Order',
                    # email=paypal_manager.EMAIL_PERSONAL,
                    currencycode='GBP'
            )

            token = setexp_response.token

            redirect_url = interface.generate_express_checkout_redirect_url(token)

            return HttpResponseRedirect(redirect_url)
        except PayPalConfigError, ex:
            return HttpResponse(ex)
        except PayPalAPIResponseError, ex:
            return HttpResponse(ex)
        except:
            return Http404
    else:
        return HttpResponseRedirect('/eliquid')


def add2cart(request,item_id,item_type):
    import json

    if request.is_ajax():
        cart = request.session['cart']

        from backend.models import ProductProduct, SmkLqdVaucherCode

        cloud_groups = {'1': '30/70', '2': '70/30'}

        update_delivery = False;


        if(int(item_type) == 1):
            product = ProductProduct.objects.get(id=item_id)

            flavours = cart['flavours']

            if(flavours.has_key(item_id)):
                flavours[item_id]['count'] += 1;
            else:
                flavours[item_id] = {'name':'%s-%s-%s'%(product.product_tmpl.description, cloud_groups[product.cloud_category] , product.product_strength.display_name),'count': 1,'price':'%.2f'%product.price_extra}

            cart['flavours'] = flavours

            cart['count'] += 1

            cart['flavour_count'] += 1

            if(cart['discount_code'] != ''):
                valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

                if(valid_code):
                    code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                    percent = code.percent

                    discount_value = 10*(100-(percent+0.1))/100

                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            else:
                if cart['flavour_count'] % 4 != 0:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                else:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        elif(int(item_type) == 2):
            product = ProductProduct.objects.get(id=item_id)

            flavours = cart['flavours2']

            if(flavours.has_key(item_id)):
                flavours[item_id]['count'] += 1;
            else:
                flavours[item_id] = {'name':'%s-%s'%(product.product_tmpl.name, product.product_strength.display_name),'count': 1,'price':'%.2f'%product.price_extra}

            cart['flavours2'] = flavours

            cart['count'] += 1

            cart['flavour2_count'] += 1

            if cart['flavour2_count'] % 3 != 0:
                cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3) + float(product.price_extra)*(cart['flavour2_count'] % 3)
            else:
                cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        elif(int(item_type) == 3):
            update_delivery = True

            product = ProductProduct.objects.get(id=int(item_id))

            additional = cart['additional']

            from backend.models import SmkLqdWeightsRanges

            last_weight = SmkLqdWeightsRanges.objects.all().order_by('-initial_weight')[0]

            if cart['diy_weight'] + float(product.smk_weight) > float(last_weight.final_weight):
                response_dict = {'success': False, 'count': cart['count'], 'msg': 'Not available product ammount, please contact us for a quote', 'diy_ex':True}

                reponse_json = json.dumps(response_dict)

                return HttpResponse(reponse_json,mimetype='application/json')


            if(additional.has_key(item_id)):
                 additional[item_id]['count'] += 1;
            else:
                 additional[item_id] = {'name':'%s-%s'%(product.name_template,product.product_size.display_name),'count': 1,'price':'%.2f'%product.price_extra}

            cart['additional'] = additional

            cart['count'] += 1

            cart['additional_total'] += float(product.price_extra)

            temp_weight =  cart['diy_weight']

            from common.weights import calc_delivery_cost

            if temp_weight == 0:
                old_delivery_cost = 0
            else:
                old_delivery_cost = calc_delivery_cost(temp_weight)

            cart['diy_weight'] += float(product.smk_weight)

            cart['additional_total'] -= float(old_delivery_cost)

            cart['additional_total'] += float(calc_delivery_cost(cart['diy_weight']))

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']
        else:
            product = ProductProduct.objects.get(id=int(item_id))

            additional = cart['additional']


            if(additional.has_key(item_id)):
                 additional[item_id]['count'] += 1;
            else:
                 additional[item_id] = {'name':product.name_template,'count': 1,'price':'%.2f'%product.price_extra}

            cart['additional'] = additional

            cart['count'] += 1

            cart['additional_total'] += float(product.price_extra)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

        request.session['cart'] = cart

        response_dict = {'success': True, 'count': cart['count'], 'update_delivery': update_delivery}

        reponse_json = json.dumps(response_dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')

def update_item_list(request):
    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']
            selected_flavours = cart['flavours']
            selected_flavours2 = cart['flavours2']
            additional = cart['additional']
        else:
            selected_flavours = {}
            selected_flavours2 = {}
            additional = {}

        return render_to_response('common/basket_item_list.html',{'selected_flavours':selected_flavours,
                                                                  'selected_flavours2':selected_flavours2,
                                                          'additional':additional}, context_instance = RequestContext(request))
    else:
        return HttpResponse('Wrong Request...')


def update_cart_total(request):
    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']
            total = cart['display_total']
        else:
            total = 0.00

        return render_to_response('common/basket_order_total.html',{'cart_total':total,}, context_instance = RequestContext(request))
    else:
        return HttpResponse('Wrong Request...')


def remove_from_cart(request,item_id):
    from backend.models import ProductProduct, SmkLqdVaucherCode

    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']

            if(cart['flavours'].has_key(item_id)):
                if(cart['flavours'][item_id]['count'] == 1):
                    del cart['flavours'][item_id]
                else:
                    cart['flavours'][item_id]['count'] -= 1

                cart['count'] -= 1
                cart['flavour_count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                #cart['total'] = 0

                if(cart['discount_code'] != ''):
                    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

                    if(valid_code):
                        code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                        percent = code.percent

                        discount_value = 10*(100-(percent+0.1))/100

                        if cart['flavour_count'] % 4 != 0:
                            cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                        else:
                            cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                    else:
                        if cart['flavour_count'] % 4 != 0:
                            cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                        else:
                            cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            if(cart['flavours2'].has_key(item_id)):
                if(cart['flavours2'][item_id]['count'] == 1):
                    del cart['flavours2'][item_id]
                else:
                    cart['flavours2'][item_id]['count'] -= 1

                cart['count'] -= 1
                cart['flavour2_count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                if cart['flavour2_count'] % 3 != 0:
                    cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3) + float(product.price_extra)*(cart['flavour2_count'] % 3)
                else:
                    cart['flavour2_total'] =  19*(cart['flavour2_count'] / 3)


            if(cart['additional'].has_key(item_id)):
                if(cart['additional'][item_id]['count'] == 1):
                    del cart['additional'][item_id]
                else:
                    cart['additional'][item_id]['count'] -= 1

                cart['count'] -= 1

                product = ProductProduct.objects.get(id=item_id)

                if(len(cart['additional'].keys()) == 0):
                    cart['additional_total'] = float(0.00)
                    cart['diy_weight'] = float(0.00)
                elif (product.is_diy):
                    temp_weight_value = cart['diy_weight']

                    from common.weights import calc_delivery_cost

                    cart['additional_total'] -= float(calc_delivery_cost(temp_weight_value))

                    cart['diy_weight'] = float(cart['diy_weight']) - float(product.smk_weight)

                    cart['additional_total'] += float(calc_delivery_cost(cart['diy_weight']))

                    cart['additional_total'] -= float(product.price_extra)
                else:
                    cart['additional_total'] -= float(product.price_extra)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']


        import json

        request.session['cart'] = cart

        response_dict = {'success': True, 'count': cart['total']}

        reponse_json = json.dumps(response_dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')



def validate_code(request, code):
    from backend.models import ProductProduct, SmkLqdVaucherCode

    if request.is_ajax():
        if(request.session.has_key('cart')):
            cart = request.session['cart']

            temp_response = False

            if code:
                valid_code = SmkLqdVaucherCode.objects.filter(code=code).filter(expired='n').count() > 0


                if(valid_code):
                    cart['discount_code'] = code
                    temp_response = True
                else:
                    cart['discount_code'] = ''
            else:
                cart['discount_code'] = ''



            product = ProductProduct.objects.filter(is_flavour=True)[0]

            if(cart['discount_code'] != ''):
                valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0


                if(valid_code):
                    code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n')[0]

                    percent = code.percent

                    discount_value = 10*(100-(percent+0.1))/100

                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  discount_value*(cart['flavour_count'] / 4)
                else:
                    if cart['flavour_count'] % 4 != 0:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                    else:
                        cart['flavour_total'] =  10*(cart['flavour_count'] / 4)

            else:
                if cart['flavour_count'] % 4 != 0:
                    cart['flavour_total'] =  10*(cart['flavour_count'] / 4) + float(product.price_extra)*(cart['flavour_count'] % 4)
                else:
                    cart['flavour_total']=  10*(cart['flavour_count'] / 4)

            cart['total'] = cart['flavour_total'] + cart['flavour2_total'] + cart['additional_total']

            cart['display_total'] = '%.2f'%cart['total']

            import json

            request.session['cart'] = cart

            response_dict = {'success': temp_response}

            reponse_json = json.dumps(response_dict)

            return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def calc_saving_total(request):
    if request.is_ajax():
        from backend.models import SmkLqdVaucherCode

        cart = request.session['cart']

        valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

        request.session['order_now'] = True

        from backend.models import ProductProduct

        product = ProductProduct.objects.filter(is_flavour=True)[0]

        temp_total =  float(product.price_extra)*(cart['flavour_count'])

        product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

        temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

        total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

        return render_to_response('common/total_saving.html',{'valid_code':valid_code,'total_saving':'%.2f'%total_saving})
    else:
        return HttpResponse('Wrong Request...')

def free_bottle2(request):
    return render_to_response('frontend/free_bottle2.html', {'is_logged_in': request.user.is_authenticated(),'check_ok':True}, context_instance = RequestContext(request))
    if request.session.has_key('free_bottle'):
        ckeck_ok = request.session['free_bottle']
        del request.session['free_bottle']
        return render_to_response('frontend/free_bottle2.html', {'is_logged_in': request.user.is_authenticated(),'check_ok':ckeck_ok}, context_instance = RequestContext(request))
    else:
        return HttpResponseRedirect('/free_bottle')


def free_bottle_email_link(request,code):
    from common.models import FreeBottleCode

    valid_code = FreeBottleCode.objects.filter(code=code).filter(valid=True).count() > 0

    if valid_code:
        code = FreeBottleCode.objects.get(code=code)

        from paypal.exceptions import PayPalAPIResponseError, PayPalConfigError
        from paypal.interface import PayPalInterface
        import common.paypal_conf as paypal_manager


        interface = PayPalInterface(config=paypal_manager.CONFIG)

        try:
            setexp_response = interface.set_express_checkout(
                    amt=0.74,
                    returnurl=paypal_manager.FREE_RETURN_URL, cancelurl=paypal_manager.FREE_CANCEL_URL,
                    paymentaction='Order',
                    # email=code.a1_reg.email,
                    currencycode='GBP'
            )

            token = setexp_response.token

            request.session[token] = code.code

            redirect_url = interface.generate_express_checkout_redirect_url(token)

            return HttpResponseRedirect(redirect_url)
        except PayPalConfigError, ex:
            return render_to_response('frontend/free_bottle_error.html',{})
            #return HttpResponse(ex)
        except PayPalAPIResponseError, ex:
            return render_to_response('frontend/free_bottle_error.html',{})
            #return HttpResponse(ex)
        except:
            return render_to_response('frontend/free_bottle_error.html',{})

    else:
        return HttpResponseRedirect('/')

def invite_proxy(request,invite_id):
    from django.db.models import ObjectDoesNotExist
    try:
        request.session['follow_invite'] = invite_id

        return HttpResponseRedirect('/free_bottle')
    except ObjectDoesNotExist:
        return HttpResponseRedirect('/')


@csrf_exempt
def paypal_free_return_url(request):
    if request.GET.has_key('token') and request.GET.has_key('PayerID'):

        cloud_groups = {'1': '30/70', '2': '70/30'}

        token = request.GET['token']
        payerid = request.GET['PayerID']

        from common.models import FreeBottleCode


        if request.session.has_key(token) and FreeBottleCode.objects.filter(code = request.session[token]).count() > 0:
            code = FreeBottleCode.objects.get(code = request.session[token])
        else:
            return HttpResponse('checking code')
            return HttpResponseRedirect('/free_bottle')

        del request.session[token]

        from paypal.exceptions import PayPalAPIResponseError, PayPalConfigError
        from paypal.interface import PayPalInterface
        import common.paypal_conf as paypal_manager

        from common.emails import send_email_by_name

        interface = PayPalInterface(config=paypal_manager.CONFIG)


        # try:
        getexp_response = interface.get_express_checkout_details(token=token)


        do_express_response = interface.do_express_checkout_payment(token=token,
                                                                    paymentaction='Sale',
                                                                    payerid=payerid,
                                                                    amt=0.74,
                                                                    currencycode='GBP')


        from backend.models import  SmkLqdPrizeRegistrationA1, SmkLqdOrderBatch, SmkLqdOrderItem, ProductProduct
        from backend.models import  ResPartnerTitle, SmkLqdSubsCustomer, SmkLqdInvites, ResPartner, SmkLqdFraudItem, SmkLqdRemoteAddr
        from common.models import Profile, MonthOdered
        from backend.models import SmkLqdRegionalDivision
        from inviter.models import UserInviter
        from django.contrib.auth.models import User
        import lambert_smokingliquid.settings as settings
        import datetime
        from common.fraud import AnalyticFraud, RemoreAddrUtil

        email = getexp_response.EMAIL
        first_name = getexp_response.FIRSTNAME
        last_name = getexp_response.LASTNAME
        street = getexp_response.PAYMENTREQUEST_0_SHIPTOSTREET
        town = getexp_response.PAYMENTREQUEST_0_SHIPTOCITY
        city = getexp_response.PAYMENTREQUEST_0_SHIPTOCITY
        postcode = getexp_response.PAYMENTREQUEST_0_SHIPTOZIP
        country = getexp_response.PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME

        selected_product = None

        if User.objects.filter(email=email).count() == 0:
            django_user = User()
            django_user.username = email
            django_user.first_name = first_name
            django_user.last_name = last_name
            django_user.email = email
            django_user.is_active = True
            django_user.set_password('1234passwd')
            django_user.save()

            #customer creation
            customer = ResPartner()
            customer.username = email
            customer.password_blank = '1234passwd'
            customer.title = ResPartnerTitle.objects.all()[0]
            customer.name = '%s %s'%(first_name,last_name)
            customer.active = True
            customer.customer = True
            customer.supplier = False
            customer.save()

            profile = Profile()
            profile.customer = customer
            profile.user = django_user
            cohort_time_delta = datetime.date.today() - settings.COHORT_START_DATE
            profile.reg_division = SmkLqdRegionalDivision.objects.get(id=1)
            profile.cohort_index = cohort_time_delta.days/30 + 1
            profile.save()

            month_ordered = MonthOdered()
            month_ordered.profile = profile
            month_ordered.month_index = cohort_time_delta.days/30
            month_ordered.ordered_date = datetime.date.today()
            month_ordered.save()

            smkcustomer = SmkLqdSubsCustomer()
            smkcustomer.email = email
            smkcustomer.name = '%s %s'%(first_name, last_name)
            smkcustomer.last_order_date = datetime.date.today()
            smkcustomer.first_order_date = datetime.date.today()
            smkcustomer.created_date = datetime.date.today()
            smkcustomer.customer = customer
            smkcustomer.username = email
            smkcustomer.postcode = postcode
            smkcustomer.street = street
            smkcustomer.town = town
            smkcustomer.city = city
            smkcustomer.country = country

            if SmkLqdPrizeRegistrationA1.objects.filter(id=code.a1_reg.id).count() > 0:
                temp_reg = SmkLqdPrizeRegistrationA1.objects.get(id=code.a1_reg.id)
                selected_product = ProductProduct.objects.get(id=temp_reg.strength)

            smkcustomer.save()


            user_inviter = UserInviter()
            user_inviter.full_name = '%s %s'%(first_name,last_name)
            user_inviter.user_id = django_user.id
            user_inviter.email = email

            invite = SmkLqdInvites()
            invite.sent_on = datetime.date.today()
            invite.sender = customer
            invite.was_followed = False
            invite.channel = 'fb'
            invite.save()

            host_name = request.META['HTTP_HOST']

            host_name = 'http://%s'%(host_name)

            referal = host_name +  '/invite/' + str(invite.id)

            user_inviter.referal_code = referal

            user_inviter.save()

            send_email_by_name('account_details',to=email,params={'password':'1234passwd'})
        else:
            customer = ResPartner.objects.get(username=email)
            smkcustomer = customer.smklqdsubscustomer_set.all()[0]
            smkcustomer.last_order_date = datetime.date.today()
            smkcustomer.save()

            addr = RemoreAddrUtil().process_request(request)

            if SmkLqdRemoteAddr.objects.filter(addr=addr).count() != 0:
                addr_obj = SmkLqdRemoteAddr.objects.get(addr=addr)
                addr_obj.subscriptions_count += 1
                addr_obj.save()
            else:
                addr_obj = SmkLqdRemoteAddr()
                addr_obj.addr = addr
                addr_obj.subscriptions_count = 1
                addr_obj.save()

            fitem = SmkLqdFraudItem()
            fitem.name = smkcustomer.name
            fitem.email = smkcustomer.email
            fitem.member_of = 'c'
            fitem.remote_addr = addr_obj
            fitem.save()

            from django.contrib.auth import login, authenticate
            authenticated_user = authenticate(username = smkcustomer.email, password = customer.password_blank)
            login(request,authenticated_user)

            return HttpResponseRedirect('/usersadmin/')

        reg_id = code.a1_reg.id

        code.a1_reg = None

        code.valid = False
        code.save()

        SmkLqdPrizeRegistrationA1.objects.filter(id=reg_id).delete()

        from backend.models import SmkLqdOrderOrder

        order = SmkLqdOrderOrder()
        order.create_date = datetime.datetime.now()
        order.created_on = datetime.date.today()
        order.customer =  customer

        order.customer_delivery_street = street
        order.customer_delivery_city = city
        order.customer_delivery_county = town
        order.customer_delivery_country = country
        order.customer_delivery_postcode = postcode
        order.print_postage = 'y'

        order.require_envelope = 'n'
        order.envelope_printed = 'n'
        order.printed = 'n'
        order.manual = 'n'
        order.free_order = 'y'
        order.regional_division = SmkLqdRegionalDivision.objects.get(id=1)
        order.customer_name = first_name + ' ' + last_name
        import random
        import time


        order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
        order.customer_email = email
        order.customer_fax = ''
        order.customer_mobile = ''
        order.customer_phone = ''
        order.order_total = 0.00

        if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').count() > 0:
            if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0].printed == 'y':
                batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0]
                batch.order_count += 1
                batch.save()
            else:
                batch = SmkLqdOrderBatch()
                batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
                batch.created_on = datetime.date.today()
                batch.potential_fraud = 'n'
                batch.require_envelope = 'n'
                batch.manual = 'n'
                batch.order_count = 1
                batch.envelope_printed = 'n'
                batch.printed = 'n'
                batch.free_order = 'y'
                batch.save()
        else:
            batch = SmkLqdOrderBatch()
            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            batch.created_on = datetime.date.today()
            batch.potential_fraud = 'n'
            batch.require_envelope = 'n'
            batch.manual = 'n'
            batch.order_count = 1
            batch.envelope_printed = 'n'
            batch.printed = 'n'
            batch.free_order = 'y'
            batch.save()

        order.batch_ids = batch
        order.save()

        #temp_product = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,).order_by('id')[0]
        temp_product = selected_product
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.product_tmpl.description +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
        item.product_description = temp_product.product_tmpl.description
        item.save()


        temp_product = ProductProduct.objects.get(default_code='H-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()

        temp_product = ProductProduct.objects.get(default_code='I-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()

        if request.session.has_key('follow_invite'):
            invite = SmkLqdInvites.objects.get(id=request.session['follow_invite'])

            profile.follow_referal = True

            profile.affiliate_customer = invite.sender.id

            profile.save()

            customer2 = ResPartner.objects.get(id=invite.sender.id)

            smk_customer2 = customer2.smklqdsubscustomer_set.all()[0]

            smk_customer2.introduced_peoples += 1

            smk_customer2.save()

            del request.session['follow_invite']


        from common.emails import send_email_by_name

        send_email_by_name('free_sample_generated',to=email,params={})

        from django.contrib.auth import login, authenticate
        authenticated_user = authenticate(username = email, password = customer.password_blank)
        login(request,authenticated_user)

        return HttpResponseRedirect('/usersadmin/')
            #return render_to_response('frontend/free_bottle3.html',{},context_instance = RequestContext(request))
        #
        # except PayPalAPIResponseError, ex:
        #     raise ex
        # except PayPalConfigError, ex:
        #     raise ex
        # except Exception, ex:
        #     raise ex
        #     return Http404
    else:
        if request.user.is_authenticated():
            return HttpResponseRedirect('/usersadmin/eliquid/')
        else:
            return HttpResponse('checking get parameters')
            return HttpResponseRedirect('/free_bottle')


@csrf_exempt
def paypal_free_cancel_url(request):
    if request.GET.has_key('token'):
        token = request.GET['token']
    if request.session.has_key(token):
        del request.session[token]
    return HttpResponseRedirect('/free_bottle/')


def get_flavours_from_cloud_group(request,cloud,flavour):
    if request.is_ajax():
        from backend.models import ProductProduct
        flavours = ProductProduct.objects.filter(name_template=flavour,is_flavour=True,cloud_category=cloud).order_by('id')

        return render_to_response('common/flavour_select_options.html',{'flavours':flavours})
    else:
        return HttpResponse('Wrong Request...')

def product_availability(request,p_id):
    if request.is_ajax():
        from backend.models import ProductProduct
        import json

        try:
            product = ProductProduct.objects.get(id=p_id)
        except Exception, ex:
            dict = {'available': False}

            reponse_json = json.dumps(dict)

            return HttpResponse(reponse_json,mimetype='application/json')


        availability =  product.stock_qty > 0

        dict = {'available': availability}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')

def devices(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/devices/')
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct

    products = ProductProduct.objects.filter(is_device=True).order_by('smk_order')

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    request.session['from_page'] = request.path_info

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('frontend/devices.html', {'is_logged_in': request.user.is_authenticated(),
                                                           'cart_total':cart_total,
                                                           'selected_flavours':selected_flavours,
                                                           'selected_flavours2':selected_flavours2,
                                                           # 'vaporizer': vaporizer,
                                                           # 'vaporizer_availability' : vaporizer_availability,
                                                           # 'smoking_device': smoking_device,
                                                           # 'smoking_device_availablity': smoking_device_availablity,
                                                           'products': products,
                                                           'count':count,
                                                           'additional':additional,
                                                           'valid_code':valid_code,
                                                            'total_saving':'%.2f'%total_saving,
                                                           'delivery_cost': '%.2f'%delivery_cost,
                                                           'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))


def diy_liquid(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/diy_liquid/')
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct, ProductTemplate

    products_dict = {}

    for tpl in ProductTemplate.objects.all():
        products = ProductProduct.objects.filter(is_diy=True).filter(product_tmpl=tpl).order_by('id')

        if not products_dict.has_key(tpl.id) and products:
            products_dict[tpl.id] = [tpl.name,products[0].product_image,tpl.description,products]

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    request.session['from_page'] = request.path_info

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('frontend/diy_liquid.html', {'is_logged_in': request.user.is_authenticated(),
                                                           'cart_total':cart_total,
                                                           'selected_flavours':selected_flavours,
                                                           'selected_flavours2':selected_flavours2,
                                                           # 'vaporizer': vaporizer,
                                                           # 'vaporizer_availability' : vaporizer_availability,
                                                           # 'smoking_device': smoking_device,
                                                           # 'smoking_device_availablity': smoking_device_availablity,
                                                           'products': products_dict,
                                                           'count':count,
                                                           'additional':additional,
                                                           'valid_code':valid_code,
                                                           'total_saving':'%.2f'%total_saving,
                                                           'delivery_cost': '%.2f'%delivery_cost,
                                                           'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))



def make_order(request):
    from backend.models import SmkLqdOrderOrder, SmkLqdSubsCustomer, SmkLqdOrderBatch, ProductProduct,  SmkLqdOrderItem
    import datetime

    customer = SmkLqdSubsCustomer.objects.get(id=1)
    order = SmkLqdOrderOrder()
    order.create_date = datetime.datetime.now()
    order.created_on = datetime.date.today()
    order.customer = customer.customer

    order.customer_delivery_street = customer.street
    order.customer_delivery_hbn = customer.hbn
    order.customer_delivery_city = customer.city
    order.customer_delivery_postcode = customer.postcode


    order.require_envelope = 'n'
    order.envelope_printed = 'n'
    order.printed = 'n'
    order.manual = 'n'
    order.customer_name = '%s %s'%('Clive','Lambert')
    import random
    import time

    email = 'clive.lambert@outlook.com'

    order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
    order.customer_email = email
    order.customer_fax = ''
    order.customer_mobile = ''
    order.customer_phone = ''
    order.order_total = 500.25

    import lambert_smokingliquid.settings as settings

    if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').count() > 0:
        if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].printed == 'y':
            batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0]
            batch.order_count += 1
            batch.save()
        else:
            batch = SmkLqdOrderBatch()
            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            batch.created_on = datetime.date.today()
            batch.potential_fraud = 'n'
            batch.require_envelope = 'n'
            batch.manual = 'n'
            batch.order_count = 1
            batch.envelope_printed = 'n'
            batch.printed = 'n'
            batch.free_order = 'n'
            batch.save()
    else:
        batch = SmkLqdOrderBatch()
        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
        batch.created_on = datetime.date.today()
        batch.potential_fraud = 'n'
        batch.require_envelope = 'n'
        batch.manual = 'n'
        batch.order_count = 1
        batch.envelope_printed = 'n'
        batch.printed = 'n'
        batch.free_order = 'n'
        batch.save()

    order.batch_ids = batch
    order.save()

    cloud_groups = {'1': '30/70', '2': '70/30'}

    temp_product = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,).order_by('id')[0]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 3
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,).order_by('id')[2]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.filter(name_template='Orange',is_flavour=True,).order_by('id')[3]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 2
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.filter(name_template='Coffee',is_flavour=True,).order_by('id')[3]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 4
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()


    temp_product = ProductProduct.objects.filter(name_template='Cola',is_flavour=True,).order_by('id')[1]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()


    temp_product = ProductProduct.objects.filter(name_template='Cola',is_flavour=True,).order_by('id')[5]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.filter(name_template='Cola',is_flavour=True,).order_by('id')[4]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.filter(name_template='Blackcurrant',is_flavour=True,).order_by('id')[2]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.get(default_code='H-1')
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.name_template
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.get(default_code='I-1')
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.name_template
    item.product_description = temp_product.product_tmpl.description
    item.save()

    return HttpResponse('Done')


def make_free_order(request):
    from backend.models import SmkLqdOrderOrder, SmkLqdOrderBatch, ProductProduct,  SmkLqdOrderItem, ResPartner
    import datetime

    customer = ResPartner.objects.get(id=36)
    order = SmkLqdOrderOrder()
    order.create_date = datetime.datetime.now()
    order.created_on = datetime.date.today()
    order.customer =  customer

    order.customer_delivery_street = 'testing'
    order.customer_delivery_hbn = '1234'
    order.customer_delivery_city = 'UK'
    order.customer_delivery_postcode = '123456'

    order.require_envelope = 'n'
    order.envelope_printed = 'n'
    order.printed = 'n'
    order.manual = 'n'
    order.customer_name = customer.name
    import random
    import time

    order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
    order.customer_email = customer.smklqdsubscustomer_set.all()[0].email
    order.customer_fax = ''
    order.customer_mobile = ''
    order.customer_phone = ''
    order.free_order = 'y'
    order.order_total = 0.00

    import lambert_smokingliquid.settings as settings

    if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').count() > 0:
        if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0].printed == 'y':
            batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='y').order_by('-id')[0]
            batch.order_count += 1
            batch.save()
        else:
            batch = SmkLqdOrderBatch()
            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            batch.created_on = datetime.date.today()
            batch.potential_fraud = 'n'
            batch.require_envelope = 'n'
            batch.manual = 'n'
            batch.order_count = 1
            batch.envelope_printed = 'n'
            batch.printed = 'n'
            batch.free_order = 'y'
            batch.save()
    else:
        batch = SmkLqdOrderBatch()
        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
        batch.created_on = datetime.date.today()
        batch.potential_fraud = 'n'
        batch.require_envelope = 'n'
        batch.manual = 'n'
        batch.order_count = 1
        batch.envelope_printed = 'n'
        batch.printed = 'n'
        batch.free_order = 'y'
        batch.save()

    order.batch_ids = batch
    order.save()

    cloud_groups = {'1': '30/70', '2': '70/30'}

    temp_product = ProductProduct.objects.filter(name_template='Menthol',is_flavour=True,).order_by('id')[0]
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.product_tmpl.description.replace(' ','&nbsp;') +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
    item.product_description = temp_product.product_tmpl.description
    item.save()


    temp_product = ProductProduct.objects.get(default_code='H-1')
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.name_template
    item.product_description = temp_product.product_tmpl.description
    item.save()

    temp_product = ProductProduct.objects.get(default_code='I-1')
    item = SmkLqdOrderItem()
    item.order_ids = order
    item.product = temp_product
    item.line_total = 1
    item.product_name = temp_product.name_template
    item.product_description = temp_product.product_tmpl.description
    item.save()

    return HttpResponse('Done')

def make_order2(request):
    from backend.models import SmkLqdOrderOrder, SmkLqdSubsCustomer, SmkLqdOrderBatch, ProductProduct,  SmkLqdOrderItem
    import datetime


    cart = request.session['cart']

    #customer = SmkLqdSubsCustomer.objects.get(id=1)
    customer = SmkLqdSubsCustomer.objects.get(id=8)
    order = SmkLqdOrderOrder()
    order.create_date = datetime.datetime.now()
    order.created_on = datetime.date.today()
    order.customer = customer.customer

    order.customer_delivery_street = customer.street
    order.customer_delivery_hbn = customer.hbn
    order.customer_delivery_city = customer.city
    order.customer_delivery_postcode = customer.postcode


    order.require_envelope = 'n'
    order.envelope_printed = 'n'
    order.printed = 'n'
    order.manual = 'n'
    order.customer_name = '%s %s'%('Clive','Lambert')
    import random
    import time

    email = 'clive.lambert@outlook.com'

    order.trans_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
    order.customer_email = email
    order.customer_fax = ''
    order.customer_mobile = ''
    order.customer_phone = ''
    order.order_total = cart['total']

    import lambert_smokingliquid.settings as settings

    if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').count() > 0:
        if SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].smklqdorderorder_set.count() <= settings.DEFAULT_ORDER_NUMBER_PER_BATCH and not SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0].printed == 'y':
            batch = SmkLqdOrderBatch.objects.filter(potential_fraud='n',require_envelope='n',free_order='n').order_by('-id')[0]
            batch.order_count += 1
            batch.save()
        else:
            batch = SmkLqdOrderBatch()
            batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
            batch.created_on = datetime.date.today()
            batch.potential_fraud = 'n'
            batch.require_envelope = 'n'
            batch.manual = 'n'
            batch.order_count = 1
            batch.envelope_printed = 'n'
            batch.printed = 'n'
            batch.free_order = 'n'
            batch.save()
    else:
        batch = SmkLqdOrderBatch()
        batch.batch_id = str(random.randrange(100,999)) + str(time.time().as_integer_ratio()[0])
        batch.created_on = datetime.date.today()
        batch.potential_fraud = 'n'
        batch.require_envelope = 'n'
        batch.manual = 'n'
        batch.order_count = 1
        batch.envelope_printed = 'n'
        batch.printed = 'n'
        batch.free_order = 'n'
        batch.save()

    order.batch_ids = batch
    order.save()

    cloud_groups = {'1': '30/70', '2': '70/30'}



    flavours = cart['flavours']

    for key, prod in flavours.iteritems():
        temp_product = ProductProduct.objects.get(id=key)
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = prod['count']
        item.product_name = temp_product.product_tmpl.description +  '-' + cloud_groups[temp_product.cloud_category] + '-' + temp_product.product_strength.display_name
        item.product_description = temp_product.product_tmpl.description
        item.save()

    additional = cart['additional']

    for key, prod in additional.iteritems():
        temp_product = ProductProduct.objects.get(id=key)
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = prod['count']
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()


        temp_product = ProductProduct.objects.get(default_code='H-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()

        temp_product = ProductProduct.objects.get(default_code='I-1')
        item = SmkLqdOrderItem()
        item.order_ids = order
        item.product = temp_product
        item.line_total = 1
        item.product_name = temp_product.name_template
        item.product_description = temp_product.product_tmpl.description
        item.save()



    return HttpResponse('Done')


def valid_age(request):
    request.session['VALIDATE_AGE'] = True

    return_url = request.META['HTTP_REFERER']

    return HttpResponseRedirect(return_url)

def privacy(request):
    return HttpResponse('Under construction')

def terms(request):
    return HttpResponse('Under construction')

def remove_unconfirmed(request,code):
    from common.models import UnconfirmedCodes

    from backend.models import SmkLqdPrizeRegistrationA1

    if UnconfirmedCodes.objects.filter(code=code).count() > 0:
        code = UnconfirmedCodes.objects.get(code=code)

        if SmkLqdPrizeRegistrationA1.objects.filter(id=code.unconfirmed_id):
            reg = SmkLqdPrizeRegistrationA1.objects.get(id=code.unconfirmed_id)
            reg.delete()

    return render_to_response('frontend/unconfirmed_remove.html',{}, context_instance = RequestContext(request))

def eliquid_30(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/usersadmin/eliquid-30ml/')
    if(request.session.has_key('cart')):
        cart = request.session['cart']
        cart_total = cart['display_total']
        selected_flavours = cart['flavours']
        selected_flavours2 = cart['flavours2']
        additional = cart['additional']
    else:
        cart_total = '0.00'
        selected_flavours = {}
        selected_flavours2 = {}
        additional = {}

    from backend.models import ProductProduct, ProductStrength, ProductTemplate

    products = ProductProduct.objects.filter(is_flavour2=True)

    products_dict = {}

    for tmpl in ProductTemplate.objects.all():
        if tmpl.id in [product.product_tmpl.id for product in products] and not products_dict.has_key(tmpl.id):
            available_products_count = ProductProduct.objects.filter(product_tmpl=tmpl).filter(stock_qty=0).count()
            all_products_count = ProductProduct.objects.filter(product_tmpl=tmpl).count()
            products_dict[tmpl.id] = [tmpl.name,all_products_count == available_products_count]

    strengths = {}

    for st in ProductStrength.objects.all():
        available_st = ProductProduct.objects.filter(is_flavour2=True).filter(product_strength=st).count()

        if available_st > 0 and not strengths.has_key(st.id):
            strengths[st.id] = st.display_name


    product_img = products[0].product_image

    count = cart['count']

    from backend.models import SmkLqdVaucherCode

    valid_code = SmkLqdVaucherCode.objects.filter(code=cart['discount_code']).filter(expired='n').count() > 0

    from backend.models import ProductProduct

    product = ProductProduct.objects.filter(is_flavour=True)[0]

    temp_total =  float(product.price_extra)*(cart['flavour_count'])

    product2 = ProductProduct.objects.filter(is_flavour2=True)[0]

    temp_total2 = float(product2.price_extra)*(cart['flavour2_count'])

    total_saving = (temp_total - cart['flavour_total']) + (temp_total2 - cart['flavour2_total'])

    request.session['from_page'] = request.path_info

    show_age_dialog = request.session['SHOW_AGE_DIALOG']

    from common.weights import calc_delivery_cost

    delivery_cost = 0

    if len(additional.keys()) == 0:
        delivery_cost += float(0.00)
    elif cart['diy_weight'] == 0:
        delivery_cost = 0.00
    else:
        delivery_cost = calc_delivery_cost(cart['diy_weight'])

    return render_to_response('frontend/eliquid_30ml.html', {'is_logged_in': request.user.is_authenticated(),
                                                             'cart_total':cart_total,
                                                             'selected_flavours':selected_flavours,
                                                             'selected_flavours2':selected_flavours2,
                                                             'products': products_dict,
                                                             'strengths': strengths,
                                                             'count':count,
                                                             'additional':additional,
                                                             'product_img':product_img,
                                                             'valid_code':valid_code,
                                                             'total_saving':'%.2f'%total_saving,
                                                             'delivery_cost': '%.2f'%delivery_cost,
                                                             'show_age_dialog':show_age_dialog}, context_instance = RequestContext(request))


def get_product_description(request,p_id):
    if request.is_ajax():
        from backend.models import ProductTemplate
        import json

        try:
            product = ProductTemplate.objects.get(id=p_id)
        except Exception, ex:
            dict = {'description': ''}

            reponse_json = json.dumps(dict)

            return HttpResponse(reponse_json,mimetype='application/json')




        dict = {'description': product.description}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')
    else:
        return HttpResponse('Wrong Request...')


def get_strengths_from_product(request,tmpl_id):
    if request.is_ajax():
        from backend.models import ProductTemplate, ProductProduct

        products = ProductProduct.objects.filter(product_tmpl=ProductTemplate.objects.get(id=tmpl_id))

        import json

        dict = {}

        for prod in products:
            dict[prod.id] = [prod.product_strength.display_name,prod.stock_qty>0]


        return render_to_response('common/available_strengths.html',{'strengths':dict})
    else:
        return HttpResponse('Wrong Request...')


def update_delivery_cost(request):
    if request.is_ajax():
        delivery_cost = 0

        if(request.session.has_key('cart')):
            cart = request.session['cart']

            additional = cart['additional']

            from common.weights import calc_delivery_cost

            if len(additional.keys()) == 0:
                delivery_cost += float(0.00)
            elif cart['diy_weight'] == 0:
                delivery_cost = 0.00
            else:
                delivery_cost = calc_delivery_cost(cart['diy_weight'])

        import json

        dict = {'delivery_cost': '%.2f'%delivery_cost}

        reponse_json = json.dumps(dict)

        return HttpResponse(reponse_json,mimetype='application/json')

    else:
        return HttpResponse('Wrong Request...')



