import lambert_smokingliquid.settings as settings
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.cache import patch_vary_headers
from django.utils.http import cookie_date

import time

SSL = 'SSL'


def request_is_secure(request):
    if request.is_secure():
        return True

    # Handle forwarded SSL (used at Webfaction)
    if 'HTTP_X_FORWARDED_SSL' in request.META:
        return request.META['HTTP_X_FORWARDED_SSL'] == 'on'

    if 'HTTP_X_SSL_REQUEST' in request.META:
        return request.META['HTTP_X_SSL_REQUEST'] == '1'

    return False

class SSLRedirect:
    def process_request(self, request):
        if request_is_secure(request):
            request.IS_SECURE=True
        return None

    def process_view(self, request, view_func, view_args, view_kwargs):
        if SSL in view_kwargs:
            secure = view_kwargs[SSL]
            del view_kwargs[SSL]
        else:
            secure = False

        if settings.DEBUG or not settings.ENABLED_SSL:
            return None

        if getattr(settings, "TESTMODE", False):
            return None

        if not secure == request_is_secure(request):
            return self._redirect(request, secure)

    def _redirect(self, request, secure):
        if settings.DEBUG and request.method == 'POST':
            raise RuntimeError(
            """Django can't perform a SSL redirect while maintaining POST data.
                Please structure your views so that redirects only occur during GETs.""")

        protocol = secure and "https" or "http"

        host_name = request.META['HTTP_HOST']

        newurl = "%s://%s%s" % (protocol,host_name,request.get_full_path())

        return HttpResponseRedirect(newurl)



class CartChecker(object):
    def  process_request(self, request):
        #request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}
        if request and not request.session.has_key('cart'):
            request.session['cart'] = {'flavours':{},'flavours2':{}, 'additional':{},'flavour2_count':0, 'diy_weight': 0, 'flavour_count': 0, 'count':0, 'flavour2_total': 0, 'flavour_total': 0, 'additional_total': 0, 'total':0, 'display_total':'0.00', 'discount_code': ''}

        return None

class AgeValidator(object):
    def  process_request(self, request):
        age_token = request.COOKIES.get('HV_AGE_TOKEN',None)

        from common.models import AgeVerifyAddr

        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')

        addr = ip

        verify_age = AgeVerifyAddr.objects.filter(addr=ip).count() > 0

        request.session['SHOW_AGE_DIALOG'] = not age_token and not verify_age

        #request.session['SHOW_AGE_DIALOG'] = not verify_age

        return None

    def process_response(self, request, response):
        if request and request.session and request.session.has_key('VALIDATE_AGE') and request.session['VALIDATE_AGE']:
            del request.session['VALIDATE_AGE']
            request.session.save()
            response.set_cookie(key='HV_AGE_TOKEN',value=True)
            from common.models import AgeVerifyAddr
            import datetime

            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[0]
            else:
                ip = request.META.get('REMOTE_ADDR')

            addr = ip

            if AgeVerifyAddr.objects.filter(addr=ip).count() == 0:
                verify_age = AgeVerifyAddr()

                verify_age.addr = addr

                verify_age.expire_date = datetime.date.today() + datetime.timedelta(days=1)

                verify_age.save()

        return response


class ADIIntegration(object):
    def process_response(self, request, response):
        try:
            if request.user and request.user.is_authenticated():
                adi_int = request.COOKIES.get('ADI_USER_ID',None)
                if not adi_int:
                    response.set_cookie(key='ADI_USER_ID',value=request.user.id)
        except AttributeError, ex:
            response.delete_cookie('ADI_USER_ID')


        return response

