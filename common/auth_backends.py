from django.contrib.auth.models import User, check_password
from backend.models import ResPartner

class GeneralAuthBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            customer = ResPartner.objects.get(username=username, password_blank=password)
        except ResPartner.DoesNotExist:
            return None
        try:
            user = User.objects.get(username=username)
            return user
        except User.DoesNotExist:
            user = User()
            user.username = username
            user.set_password(password)
            user.email = username
            user.save()
            return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


