def calc_delivery_cost(weight):
    from backend.models import SmkLqdWeightsRanges

    for w in SmkLqdWeightsRanges.objects.all().order_by('initial_weight'):
        if weight >= float(w.initial_weight) and weight < float(w.final_weight):
            return w.price

    return 0

