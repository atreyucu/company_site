class RemoreAddrUtil(object):
    FORWARDED_FOR_FIELDS = [
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED_HOST',
        'HTTP_X_FORWARDED_SERVER',
    ]

    def process_request(self, request):
        for field in self.FORWARDED_FOR_FIELDS:
            if field in request.META:
                return request.META[field]

        return request.META.get('REMOTE_ADDR')

class AnalyticFraud(object):
    def analice_fraud(self, addr, email, postcode):
        from backend.models import SmkLqdRemoteAddr
        if SmkLqdRemoteAddr.objects.filter(addr=addr).count() > 0:
            user_remote_addr = SmkLqdRemoteAddr.objects.get(addr=addr)
        else:
            return False

        from backend.models import SmkLqdPrizeRegistrationA1, SmkLqdSubsCustomer

        # if user_remote_addr.smklqdprizeregistrationa1_set.count() >= 1:
        #     return True

        email = email
        postcode = postcode

        temp_val1 = SmkLqdPrizeRegistrationA1.objects.filter(email=email).count() != 0

        temp_val2 = SmkLqdPrizeRegistrationA1.objects.filter(postcode=postcode).count() != 0


        if temp_val1 or temp_val2:
            return True

        temp_val1 = SmkLqdSubsCustomer.objects.filter(email=email).count() != 0

        temp_val2 = SmkLqdSubsCustomer.objects.filter(postcode=postcode).count() != 0

        if temp_val1 or temp_val2:
            return True

        return False