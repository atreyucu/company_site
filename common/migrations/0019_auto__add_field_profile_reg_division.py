# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Profile.reg_division'
        db.add_column(u'common_profile', 'reg_division',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['backend.SmkLqdRegionalDivision'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Profile.reg_division'
        db.delete_column(u'common_profile', 'reg_division_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'backend.productcategory': {
            'Meta': {'object_name': 'ProductCategory', 'db_table': "u'product_category'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'create_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'create_user'", 'null': 'True', 'db_column': "u'create_uid'", 'to': u"orm['backend.ResUsers']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'null': 'True', 'db_column': "u'parent'", 'blank': 'True'}),
            'parent_left': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'parent_right': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sequence': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'update_user'", 'null': 'True', 'db_column': "u'write_uid'", 'to': u"orm['backend.ResUsers']"})
        },
        u'backend.productproduct': {
            'Meta': {'object_name': 'ProductProduct', 'db_table': "u'product_product'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cloud_category': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_code': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_device': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_diy': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_flavour': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_flavour2': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_other_suplements': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name_template': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'price_extra': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'price_margin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'product_image': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product_size': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductSize']", 'null': 'True', 'db_column': "u'product_size'", 'blank': 'True'}),
            'product_strength': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductStrength']", 'null': 'True', 'db_column': "u'product_strength'", 'blank': 'True'}),
            'product_title': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'product_tmpl': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductTemplate']", 'db_column': "u'product_tmpl_id'"}),
            'smk_order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'stock_qty': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'variants': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productsize': {
            'Meta': {'object_name': 'ProductSize', 'db_table': "u'product_size'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productstrength': {
            'Meta': {'object_name': 'ProductStrength', 'db_table': "u'product_strength'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marketing_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'marketing_text2': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.producttemplate': {
            'Meta': {'object_name': 'ProductTemplate', 'db_table': "u'product_template'"},
            'categ': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductCategory']", 'db_column': "u'categ_id'"}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company_id'", 'blank': 'True'}),
            'cost_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_purchase': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_sale': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'loc_case': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_rack': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'loc_row': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'mes_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'procure_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'produce_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'product_manager': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'product_manager'", 'blank': 'True'}),
            'purchase_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'rental': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'sale_delay': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'sale_ok': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'standard_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'supply_method': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'uom': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_template_uom'", 'db_column': "u'uom_id'", 'to': u"orm['backend.ProductUom']"}),
            'uom_po': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'product_tamplete_uom_po'", 'db_column': "u'uom_po_id'", 'to': u"orm['backend.ProductUom']"}),
            'uos': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'product_template_uos'", 'null': 'True', 'db_column': "u'uos_id'", 'to': u"orm['backend.ProductUom']"}),
            'uos_coeff': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'volume': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'warranty': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'weight_net': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuom': {
            'Meta': {'object_name': 'ProductUom', 'db_table': "u'product_uom'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductUomCateg']", 'db_column': "u'category'"}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'factor': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'max_digits': '65535', 'decimal_places': '65535'}),
            'uom_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.productuomcateg': {
            'Meta': {'object_name': 'ProductUomCateg', 'db_table': "u'product_uom_categ'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescompany': {
            'Meta': {'object_name': 'ResCompany', 'db_table': "u'res_company'"},
            'account_no': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'account_no'", 'blank': 'True'}),
            'company_registry': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'currency_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'blank': 'True'}),
            'paper_format': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'partner_id': ('django.db.models.fields.IntegerField', [], {}),
            'rml_footer1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'company_rml_header'", 'db_column': "u'rml_header'", 'to': u"orm['backend.ResUsers']"}),
            'rml_header1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header2': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']", 'db_column': "u'rml_header2'"}),
            'rml_header3': ('django.db.models.fields.TextField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrency': {
            'Meta': {'object_name': 'ResCurrency', 'db_table': "u'res_currency'"},
            'accuracy': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'base': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resdepartment': {
            'Meta': {'object_name': 'ResDepartment', 'db_table': "u'res_department'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resowner': {
            'Meta': {'object_name': 'ResOwner', 'db_table': "u'res_owner'"},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartner': {
            'Meta': {'object_name': 'ResPartner', 'db_table': "u'res_partner'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company_id'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'credit_limit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'employee': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResOwner']", 'null': 'True', 'db_column': "u'owner'", 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'parent_id'", 'blank': 'True'}),
            'password_blank': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerTitle']", 'null': 'True', 'db_column': "u'title'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'user_id'", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'vat': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnertitle': {
            'Meta': {'object_name': 'ResPartnerTitle', 'db_table': "u'res_partner_title'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '46'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resusers': {
            'Meta': {'object_name': 'ResUsers', 'db_table': "u'res_users'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {}),
            'context_lang': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'context_tz': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'department_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResDepartment']", 'null': 'True', 'db_column': "u'department_ids'", 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'menu_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'menu_tips': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user_email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdprizeregistrationa1': {
            'Meta': {'object_name': 'SmkLqdPrizeRegistrationA1', 'db_table': "u'smk_lqd_prize_registration_a1'"},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cloud_size': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'email_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'fraud': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'hbn': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '90', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'signup_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'strength': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.smklqdregionaldivision': {
            'Meta': {'object_name': 'SmkLqdRegionalDivision', 'db_table': "u'smk_lqd_regional_division'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'fb_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'common.ageverifyaddr': {
            'Meta': {'object_name': 'AgeVerifyAddr'},
            'addr': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'expire_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'common.freebottlecode': {
            'Meta': {'object_name': 'FreeBottleCode'},
            'a1_reg': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdPrizeRegistrationA1']", 'null': 'True', 'blank': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'valid': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'common.freeselected': {
            'Meta': {'object_name': 'FreeSelected'},
            'cloud': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'flavour': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductProduct']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stregth': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ProductStrength']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'common.monthodered': {
            'Meta': {'object_name': 'MonthOdered'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_index': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'ordered_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['common.Profile']"})
        },
        u'common.profile': {
            'Meta': {'object_name': 'Profile'},
            'affiliate_customer': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cohort_index': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            'follow_referal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reg_division': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.SmkLqdRegionalDivision']", 'null': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'common.unconfirmedcodes': {
            'Meta': {'object_name': 'UnconfirmedCodes'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unconfirmed_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']