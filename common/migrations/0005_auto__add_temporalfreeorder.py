# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TemporalFreeOrder'
        db.create_table(u'common_temporalfreeorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('town', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hbn', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('signup_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=90, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('fraud', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'common', ['TemporalFreeOrder'])


    def backwards(self, orm):
        # Deleting model 'TemporalFreeOrder'
        db.delete_table(u'common_temporalfreeorder')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'backend.rescompany': {
            'Meta': {'object_name': 'ResCompany', 'db_table': "u'res_company'"},
            'account_no': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'account_no'", 'blank': 'True'}),
            'company_registry': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'currency_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128', 'blank': 'True'}),
            'paper_format': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'parent_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'partner_id': ('django.db.models.fields.IntegerField', [], {}),
            'rml_footer1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'company_rml_header'", 'db_column': "u'rml_header'", 'to': u"orm['backend.ResUsers']"}),
            'rml_header1': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'rml_header2': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCurrency']", 'db_column': "u'rml_header2'"}),
            'rml_header3': ('django.db.models.fields.TextField', [], {}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'write_uid': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.rescurrency': {
            'Meta': {'object_name': 'ResCurrency', 'db_table': "u'res_currency'"},
            'accuracy': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'base': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'rounding': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '65535', 'decimal_places': '65535', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resdepartment': {
            'Meta': {'object_name': 'ResDepartment', 'db_table': "u'res_department'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resowner': {
            'Meta': {'object_name': 'ResOwner', 'db_table': "u'res_owner'"},
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartner': {
            'Meta': {'object_name': 'ResPartner', 'db_table': "u'res_partner'"},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResCompany']", 'null': 'True', 'db_column': "u'company_id'", 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'credit_limit': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'ean13': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'employee': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResOwner']", 'null': 'True', 'db_column': "u'owner'", 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']", 'null': 'True', 'db_column': "u'parent_id'", 'blank': 'True'}),
            'password_blank': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'ref': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartnerTitle']", 'null': 'True', 'db_column': "u'title'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResUsers']", 'null': 'True', 'db_column': "u'user_id'", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'vat': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.respartnertitle': {
            'Meta': {'object_name': 'ResPartnerTitle', 'db_table': "u'res_partner_title'"},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '24'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '46'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'backend.resusers': {
            'Meta': {'object_name': 'ResUsers', 'db_table': "u'res_users'"},
            'action_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'company_id': ('django.db.models.fields.IntegerField', [], {}),
            'context_lang': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'context_tz': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'department_ids': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResDepartment']", 'null': 'True', 'db_column': "u'department_ids'", 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '64'}),
            'menu_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'menu_tips': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user_email': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'write_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'common.profile': {
            'Meta': {'object_name': 'Profile'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['backend.ResPartner']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'common.temporalfreeorder': {
            'Meta': {'object_name': 'TemporalFreeOrder'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fraud': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hbn': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '90', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'signup_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']