from lambert_smokingliquid import settings
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
import smtplib
from backend.models import SmkLqdExpressNotificationEmail, SmkLqdVaucherCode



def get_emails_params(smkcustomer=None, registrationa1=None, registration2=None):
    code10 = SmkLqdVaucherCode.objects.filter(percent=10).filter(expired='n').order_by('-id')[0]
    code20 = SmkLqdVaucherCode.objects.filter(percent=20).filter(expired='n').order_by('-id')[0]

    from backend.models import SmkLqdInvites

    import uuid

    str_code = str(uuid.uuid4()).replace('-','')

    if registrationa1:
        from common.models import FreeBottleCode

        code = FreeBottleCode()
        code.code = str_code
        code.a1_reg = registrationa1
        code.valid = True
        code.save()

        free_bottle_link = "%s://%s%s%s" %('http','heroesvape.uk','/order_free_bottle/',str_code)

        from common.models import UnconfirmedCodes

        if UnconfirmedCodes.objects.filter(unconfirmed_id=registrationa1.id):
            unco_code = UnconfirmedCodes.objects.get(unconfirmed_id=registrationa1.id)

            remove_address = "%s://%s%s%s" %('http','heroesvape.uk','/remove_unconfirmed/',unco_code.code)
        else:
            remove_address = ''
    else:
        free_bottle_link = ''
        remove_address = ''

    if smkcustomer:
        introduced = SmkLqdInvites.objects.filter(was_followed=True).filter(sender=smkcustomer.customer).count()
        last_free_bottles = smkcustomer.last_free_bottle_count
        free_bottles = smkcustomer.free_bottle_count
    else:
        introduced = 0
        last_free_bottles = 0
        free_bottles = 0

    return { 'code10':code10.code,
             'code20':code20.code,
             'current_free_bottles_earned': free_bottles,
             'introduced_peoples': introduced,
             'last_free_bottles_earned': last_free_bottles,
             'link':free_bottle_link,
             'remove_address':remove_address,}

def send_email(email, to=None, params=None):
    subject_value = email.email_subject
    email_content = email.email_content

    from jinja2 import Template

    if params:
        email_content_template = Template(email_content)
        email_content_rendered = email_content_template.render(params)
    else:
        email_content_rendered = email_content

    email_template = Template(email.email_template.template_content)

    html = email_template.render(subject=subject_value,
                                content=email_content_rendered)

    if(email.email_from_addr):
        from_email_addr = email.email_from_addr.email_addr
    else:
        from_email_addr = settings.EMAIL_HOST_USER

    if email.require_addr == 'y':
        to_addr = email.addressee
    else:
        to_addr = to

    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = subject_value
    msgRoot['From'] = from_email_addr
    msgRoot['To'] = to_addr
    msgRoot.preamble = subject_value

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    msgText = MIMEText(html, 'html')
    msgAlternative.attach(msgText)

    import os

    image = ''

    if email.email_header == '1':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'amanda.jpg')
    elif email.email_header == '2':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'clive.jpg')
    elif email.email_header == '3':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'free_liquid.jpg')
    elif email.email_header == '4':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'save_30_percent.jpg')
    elif email.email_header == '6':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'save_20_percent.jpg')
    else:
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'uk.jpg')

    fp = open(image, 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    msgImage.add_header('Content-ID', '<image1>')
    msgRoot.attach(msgImage)
    if settings.USE_SSL:
        try:
            smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass
    else:
        try:
            smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass




def send_email_by_name(email_name, to=None, params=None):
    email = SmkLqdExpressNotificationEmail.objects.get(name=email_name,email_list='c')
    subject_value = email.email_subject
    email_content = email.email_content

    from jinja2 import Template

    if params:
        email_content_template = Template(email_content)
        email_content_rendered = email_content_template.render(params)
    else:
        email_content_rendered = email_content

    email_template = Template(email.email_template.template_content)

    html = email_template.render(subject=subject_value,
                                content=email_content_rendered)

    if(email.email_from_addr):
        from_email_addr = email.email_from_addr.email_addr
    else:
        from_email_addr = settings.EMAIL_HOST_USER

    if email.require_addr == 'y':
        to_addr = email.addressee
    else:
        to_addr = to

    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = subject_value
    msgRoot['From'] = from_email_addr
    msgRoot['To'] = to_addr
    msgRoot.preamble = subject_value

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    html = unicode(html)

    msgText = MIMEText(html, 'html')
    msgAlternative.attach(msgText)

    import os

    image = ''

    if email.email_header == '1':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'amanda.jpg')
    elif email.email_header == '2':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'clive.jpg')
    elif email.email_header == '3':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'free_liquid.jpg')
    elif email.email_header == '4':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'save_30_percent.jpg')
    elif email.email_header == '6':
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'save_20_percent.jpg')
    else:
        image = os.path.join(settings.EMAIL_IMAGE_DIRECTORY_PATH, 'uk.jpg')

    fp = open(image, 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    msgImage.add_header('Content-ID', '<image1>')
    msgRoot.attach(msgImage)
    if settings.USE_SSL:
        try:
            smt = smtplib.SMTP_SSL(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass
    else:
        try:
            smt = smtplib.SMTP(host=email.email_server.smtp_host,port=email.email_server.smtp_port)
            if email.email_server.smtp_user:
                smt.login(email.email_server.smtp_user,email.email_server.smtp_pass)
            smt.sendmail(from_email_addr,[to_addr],msgRoot.as_string())
            smt.close()
        except Exception,ex:
            print ex
            pass



