from django.conf import settings

class DBRouter(object):
    """
    A router to control all database operations on models in the
    general_auth application.
    """
    def db_for_read(self, model, **hints):
        if model._meta.app_label in settings.DATABASES.keys():
            return model._meta.app_label
        else:
            return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label in settings.DATABASES.keys():
            return model._meta.app_label
        else:
            return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == obj2._meta.app_label:
           return True
        return None

    def allow_syncdb(self, db, model):
        if db == 'default':
            return model._meta.app_label != 'inviter'
        else:
            return model._meta.app_label == db
