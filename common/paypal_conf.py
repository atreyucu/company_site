import lambert_smokingliquid.settings as settings

from paypal import PayPalConfig

CONFIG = PayPalConfig(API_ENVIRONMENT=settings.PAYPAL_API_ENVIRONMENT,
                      API_USERNAME=settings.PAYPAL_API_USERNAME,
                      API_PASSWORD=settings.PAYPAL_API_PASSWORD,
                      API_SIGNATURE=settings.PAYPAL_API_SIGNATURE,
                      DEBUG_LEVEL=0)

RETURN_URL = settings.PAYPAL_RETURN_URL

FREE_RETURN_URL = settings.PAYPAL_FREE_RETURN_URL

CANCEL_URL = settings.PAYPAL_CANCEL_URL

FREE_CANCEL_URL = settings.PAYPAL_FREE_CANCEL_URL

EMAIL_PERSONAL = settings.PAYPAL_EMAIL_PERSONAL
