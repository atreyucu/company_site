from django.db import models
from django.contrib.auth.models import User

# Create your models here.
from backend.models import ResPartner, SmkLqdPrizeRegistrationA1, ProductStrength, ProductProduct, SmkLqdRegionalDivision


class Profile(models.Model):
    user = models.OneToOneField(User)
    customer = models.ForeignKey(ResPartner)
    cohort_index = models.IntegerField(default=1)
    follow_referal = models.BooleanField(default=False)
    affiliate_customer = models.IntegerField(default=0)
    reg_division = models.ForeignKey(SmkLqdRegionalDivision, null=True)

class MonthOdered(models.Model):
    profile = models.ForeignKey(Profile)
    month_index = models.IntegerField(default=1)
    ordered_date = models.DateField(null=True)

class FreeBottleCode(models.Model):
    code = models.CharField(max_length=255, blank=False, null=False)
    a1_reg = models.ForeignKey(SmkLqdPrizeRegistrationA1, null=True, blank=True)
    valid = models.BooleanField(default=True)

class FreeSelected(models.Model):
    stregth = models.ForeignKey(ProductStrength, null=True, blank=True)
    flavour = models.ForeignKey(ProductProduct, null=True, blank=True)
    cloud = models.CharField(max_length=1,default='1')
    user = models.ForeignKey(User, null=True, blank=True)

class AgeVerifyAddr(models.Model):
    addr = models.CharField(max_length=100)
    expire_date = models.DateField(null=True)

class UnconfirmedCodes(models.Model):
    code = models.CharField(max_length=255, blank=False, null=False)
    unconfirmed_id = models.IntegerField(default=0)


class FundsRaisedStats(models.Model):
    name = models.CharField(max_length=255)
    last_month = models.FloatField(default=0)
    this_month = models.FloatField(default=0)

