--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2016-07-12 15:28:40 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3043 (class 0 OID 130031)
-- Dependencies: 550
-- Data for Name: smk_lqd_weights_ranges; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY smk_lqd_weights_ranges (id, create_uid, create_date, write_date, write_uid, initial_weight, price, final_weight) FROM stdin;
1	1	2016-07-12 16:51:09.113904	\N	\N	0.0000	2.80	2.0000
2	1	2016-07-12 16:51:45.12116	\N	\N	2.0000	6.15	5.0000
3	1	2016-07-12 16:52:09.618424	\N	\N	5.0000	7.84	10.0000
4	1	2016-07-12 16:52:34.953167	\N	\N	10.0000	10.15	15.0000
\.


--
-- TOC entry 3048 (class 0 OID 0)
-- Dependencies: 549
-- Name: smk_lqd_weights_ranges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('smk_lqd_weights_ranges_id_seq', 4, true);


-- Completed on 2016-07-12 15:28:40 CDT

--
-- PostgreSQL database dump complete
--

