--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-10-30 14:42:16 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2967 (class 0 OID 45400)
-- Dependencies: 359
-- Data for Name: product_template; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_template (id, create_uid, create_date, write_date, write_uid, warranty, supply_method, uos_id, list_price, weight, standard_price, mes_type, uom_id, description_purchase, uos_coeff, purchase_ok, product_manager, company_id, name, state, loc_rack, uom_po_id, type, description, weight_net, volume, loc_row, description_sale, procure_method, cost_method, rental, sale_ok, sale_delay, loc_case, produce_delay, categ_id) FROM stdin;
1	1	2015-02-26 19:17:45.783992	\N	\N	\N	buy	\N	75.00	\N	30.00	fixed	4	\N	1.00	f	\N	1	Service on Timesheet	\N	\N	4	service	\N	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
8	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Smoking Pipe	\N	\N	11	consu	Smoking Pipe	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
9	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	USB Plug	\N	\N	11	consu	USB Plug	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
10	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Battery	\N	\N	11	consu	Battery	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
12	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	4.99	\N	1.00	fixed	11	\N	1.000	t	\N	1	Smoking Device and USB charger package	\N	\N	11	consu	Smoking Device (and USB charger)	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
11	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	4.99	\N	1.00	fixed	11	\N	1.000	t	\N	1	Vaporizer	\N	\N	11	consu	Smoking Vaporizer	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
2	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Menthol	\N	\N	10	consu	Menthol	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
3	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Apple	\N	\N	10	consu	Apple	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
4	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Orange	\N	\N	10	consu	Orange	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
5	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Grapefruit	\N	\N	10	consu	Grapefruit	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
6	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Coffee	\N	\N	10	consu	Coffee	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
7	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Cola	\N	\N	10	consu	Cola	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
\.


--
-- TOC entry 3006 (class 0 OID 0)
-- Dependencies: 360
-- Name: product_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_template_id_seq', 11, true);


-- Completed on 2015-10-30 14:42:16 CDT

--
-- PostgreSQL database dump complete
--

