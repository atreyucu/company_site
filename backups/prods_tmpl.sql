--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-11-20 10:12:49 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2995 (class 0 OID 78070)
-- Dependencies: 359
-- Data for Name: product_template; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_template (id, create_uid, create_date, write_date, write_uid, warranty, supply_method, uos_id, list_price, weight, standard_price, mes_type, uom_id, description_purchase, uos_coeff, purchase_ok, product_manager, company_id, name, state, loc_rack, uom_po_id, type, description, weight_net, volume, loc_row, description_sale, procure_method, cost_method, rental, sale_ok, sale_delay, loc_case, produce_delay, categ_id) FROM stdin;
13	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	Big savings leaflet	\N	\N	11	consu	Big savings leaflet	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
14	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	30% off voucher	\N	\N	11	consu	30% off voucher	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
\.


--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 360
-- Name: product_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_template_id_seq', 11, true);


-- Completed on 2015-11-20 10:12:49 CST

--
-- PostgreSQL database dump complete
--

