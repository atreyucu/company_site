--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2016-01-26 09:30:48 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3006 (class 0 OID 117852)
-- Dependencies: 367
-- Data for Name: product_template; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_template (id, create_uid, create_date, write_date, write_uid, warranty, supply_method, uos_id, list_price, weight, standard_price, mes_type, uom_id, description_purchase, uos_coeff, purchase_ok, product_manager, company_id, name, state, loc_rack, uom_po_id, type, description, weight_net, volume, loc_row, description_sale, procure_method, cost_method, rental, sale_ok, sale_delay, loc_case, produce_delay, categ_id) FROM stdin;
19	1	2015-12-04 16:07:26.049493	2016-01-16 17:45:54.3908	1	0	buy	\N	24.99	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Nicotine Base Liquid (250ml):	\N	\N	1	consu	This 250ml bottle of Nicotine base liquid is ideal for vapers who like to make their own liquid, it is sourced from the largest nicotine manufacturer in Europe and produced under full pharmaceutical cGMP conditions (current pharmaceutical Good Manufacturing Practice). This product is supplied as a 72mg/ml (7.2%) base liquid (50/50 pg/vg) and is for experienced vapers only, it is not an e-liquid and must be diluted before use. 	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
12	1	2015-02-26 19:20:26.058933	2016-01-21 11:15:13.753585	1	\N	buy	\N	9.95	\N	1.00	fixed	11	\N	1.000	t	\N	1	Veg Glycerine (1 Litre):	\N	\N	11	consu	1 Litre Bottle - Vegetable Glycerine (or VG as its typically referred to) is a clear liquid which is mainly responsible for creating the vapour cloud. As with our other liquids we only source our VG from qualified suppliers whose production methods are tested to meet both EP/ and BP certification.	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
11	1	2015-02-26 19:20:26.058933	2016-01-23 17:45:51.149528	1	\N	buy	\N	7.99	\N	1.00	fixed	11	\N	1.000	t	\N	1	5 x Spare Spartan Vaporizer	\N	\N	11	consu	If you use multiple flavours we recommend you invest in a pack of 5 spare vaporizers so you can have a different one for each flavour, this is because the flavour can remain in the tube even after being cleaned out. Vaporizers do have a shelf life, most will last several weeks unless\nmisused. 	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
15	1	2015-12-04 15:51:49.001002	2016-01-23 17:50:39.10878	1	0	buy	\N	7.99	0.00	1.00	fixed	1	\N	1.000	t	\N	1	The Spartan ... Ideal For New Vapers.	\N	\N	1	consu	The Spartan is a small basic device which has proven ideal for people just starting out vaping who dont want to invest much money until they know that vaping is really for them. The battery is the main part, it can be charged 100's of times and once fully charged it can last the average user a full day. The vaporizer can hold 1.6ml of liquid and we recommend using a different vaporizer for each flavour you use (see spares below). Most of the vaporizers will last a couple of weeks although some have been reported to have shorter life's when used incorrectly. The Spartan also comes with its own USB charger so you have everything you need to get started on a budget.	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
16	1	2015-12-04 15:51:56.496749	2016-01-24 12:25:01.678963	1	0	buy	\N	34.99	0.00	1.00	fixed	1	\N	1.000	t	\N	1	The Apache ... Because First Impressions Count.	\N	\N	1	consu	We have nicknamed this device 'The Apache' In homage to the battle tested Apache Helicopter. This device is well designed, well built and highly reliable. Sleek in appearance this 100% Original Eleaf basic is a real attention grabber. In addition to its impressive 2300mAh battery capacity it boasts a singular magnetic connector, a replaceable 1.8ml atomizer all contained within a built-in structure making it easy to use and great to look at. This device is ideal for vapours looking for a genuine product capable of sub ohm performance that delivers every time. (replaceable atomizer heads available when needed).	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
1	1	2015-02-26 19:17:45.783992	\N	\N	\N	buy	\N	75.00	\N	30.00	fixed	4	\N	1.00	f	\N	1	Service on Timesheet	\N	\N	4	service	\N	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
8	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Smoking Pipe	\N	\N	11	consu	Smoking Pipe	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
9	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	USB Plug	\N	\N	11	consu	USB Plug	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
10	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Battery	\N	\N	11	consu	Battery	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
2	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Menthol	\N	\N	10	consu	Menthol	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
13	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	Big savings leaflet	\N	\N	11	consu	Big savings leaflet	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
4	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Orange	\N	\N	10	consu	Orange Burst	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
3	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Apple	\N	\N	10	consu	Apple Splash	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
6	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Coffee	\N	\N	10	consu	Morning Coffee	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
7	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Cola	\N	\N	10	consu	Cola Sensation	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
5	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Blackcurrant	\N	\N	10	consu	Blackcurrant Crush	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
14	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	30% off voucher	\N	\N	11	consu	30% off voucher	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
20	1	2015-12-04 16:07:30.066899	2016-01-16 17:38:29.97909	1	0	buy	\N	69.99	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Nicotine Base Liquid (1 Litre - 1000ml):	\N	\N	1	consu	This 1 litre bottle of Nicotine base liquid is perfect for vapers who like to mix their own liquid, it is sourced from the largest nicotine manufacturer in Europe and produced under full pharmaceutical cGMP conditions (current pharmaceutical Good Manufacturing Practice). This product is supplied as a 72mg/ml (7.2%) base liquid (50/50 pg/vg) and is for experienced vapers only, it is not an e-liquid and must be diluted before use. 	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
30	1	2016-01-21 16:13:18.232093	\N	\N	0	buy	\N	9.95	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Propylene Glycol (1 litre): (copy)	\N	\N	1	consu	1 Litre Bottle - Propylene Glycol (or PG as its widely referred to) is a clear, odourless and virtually tasteless liquid which acts as the main carrier agent. We source our PG from qualified suppliers whose production methods are tested to meet both EP (European Pharmacopeia) and BP (British Pharmacopeia) certification.	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
17	1	2015-12-04 15:53:47.119517	2016-01-23 17:49:25.762084	1	0	buy	\N	49.99	0.00	1.00	fixed	1	\N	1.000	t	\N	1	The Challenger ... Designed For Blowing GIANT Clouds.	\N	\N	1	consu	Just like the British Army's Challenger tank this authentic iStick Original is highly reliable and designed to deliver BIG results. With the ability to fire atomizers as low as 0.15 ohm this device is perfect for anyone looking to blow giant clouds (2V-10V / 5W-100W). With its impressive sub ohm abilities its proven an ideal device for both high VG and high PG liquids without any flavour loss. This device is black in colour (as shown) has variable volt settings, a strong spring loaded 510 contact pin, and wear-resistant stainless steel thread. It also comes with its own USB charger, and a huge capacity 5.5ml melo tank. (replaceable heads available when needed).	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
31	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Vanilla	\N	\N	10	consu	Vanilla	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
32	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Aniseed	\N	\N	10	consu	Aniseed	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
\.


--
-- TOC entry 3045 (class 0 OID 0)
-- Dependencies: 368
-- Name: product_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_template_id_seq', 30, true);


-- Completed on 2016-01-26 09:30:48 CST

--
-- PostgreSQL database dump complete
--

