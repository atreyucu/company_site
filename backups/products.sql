--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-10-30 14:40:41 CDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2963 (class 0 OID 45379)
-- Dependencies: 353
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_product (id, create_uid, create_date, write_date, write_uid, ean13, is_flavour, color, price_extra, default_code, name_template, active, variants, product_strength, product_image, product_tmpl_id, price_margin) FROM stdin;
1	1	2015-02-26 19:17:45.783992	\N	\N	\N	f	0	0.00	\N	Service on Timesheet	t	\N	\N	\N	1	1.00
8	1	2015-02-26 19:20:26.058933	\N	\N	\N	f	0	0.00	C-1	Smoking Pipe	t	\N	\N	\N	8	1.00
9	1	2015-02-26 19:20:26.058933	\N	\N	\N	f	0	0.00	D-1	USB Plug	t	\N	\N	\N	9	1.00
10	1	2015-02-26 19:20:26.058933	\N	\N	\N	f	0	0.00	E-1	Battery	t	\N	\N	\N	10	1.00
11	1	2015-02-26 19:20:26.058933	\N	\N	\N	f	0	0.00	F-1	Vaporizer	t	\N	\N	\N	11	1.00
12	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-2	Menthol	t	\N	1	\N	2	1.00
13	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-3	Menthol	t	\N	5	\N	2	1.00
14	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-4	Menthol	t	\N	9	\N	2	1.00
15	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-5	Menthol	t	\N	13	\N	2	1.00
16	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-1	Menthol	t	\N	17	\N	2	1.00
18	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-7	Apple	t	\N	1	\N	3	1.00
19	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-8	Apple	t	\N	5	\N	3	1.00
20	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-9	Apple	t	\N	9	\N	3	1.00
21	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-10	Apple	t	\N	13	\N	3	1.00
22	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-2	Apple	t	\N	17	\N	3	1.00
23	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-11	Orange	t	\N	1	\N	4	1.00
24	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-12	Orange	t	\N	5	\N	4	1.00
25	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-13	Orange	t	\N	9	\N	4	1.00
26	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-14	Orange	t	\N	13	\N	4	1.00
27	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-3	Orange	t	\N	17	\N	4	1.00
28	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-15	Grapefruit	t	\N	1	\N	5	1.00
29	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-16	Grapefruit	t	\N	5	\N	5	1.00
30	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-17	Grapefruit	t	\N	9	\N	5	1.00
31	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-18	Grapefruit	t	\N	13	\N	5	1.00
32	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-4	Grapefruit	t	\N	17	\N	5	1.00
33	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-19	Coffee	t	\N	1	\N	6	1.00
34	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-20	Coffee	t	\N	5	\N	6	1.00
35	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-21	Coffee	t	\N	9	\N	6	1.00
36	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-22	Coffee	t	\N	13	\N	6	1.00
37	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-5	Coffee	t	\N	17	\N	6	1.00
38	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-23	Cola	t	\N	1	\N	7	1.00
39	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-24	Cola	t	\N	5	\N	7	1.00
40	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-25	Cola	t	\N	9	\N	7	1.00
41	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-26	Cola	t	\N	13	\N	7	1.00
42	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-6	Cola	t	\N	17	\N	7	1.00
43	1	2015-02-26 19:20:26.058933	\N	\N	\N	f	0	0.00	G-1	Smoking Device and USB charger package	t	\N	\N	\N	12	1.00
\.


--
-- TOC entry 2983 (class 0 OID 0)
-- Dependencies: 354
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_product_id_seq', 11, true);


-- Completed on 2015-10-30 14:40:41 CDT

--
-- PostgreSQL database dump complete
--

