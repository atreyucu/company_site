--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-12-04 11:29:23 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2995 (class 0 OID 93906)
-- Dependencies: 363
-- Data for Name: product_template; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_template (id, create_uid, create_date, write_date, write_uid, warranty, supply_method, uos_id, list_price, weight, standard_price, mes_type, uom_id, description_purchase, uos_coeff, purchase_ok, product_manager, company_id, name, state, loc_rack, uom_po_id, type, description, weight_net, volume, loc_row, description_sale, procure_method, cost_method, rental, sale_ok, sale_delay, loc_case, produce_delay, categ_id) FROM stdin;
8	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Smoking Pipe	\N	\N	11	consu	Smoking Pipe	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
9	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	USB Plug	\N	\N	11	consu	USB Plug	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
10	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	1.00	fixed	11	\N	1.000	t	\N	1	Battery	\N	\N	11	consu	Battery	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
2	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Menthol	\N	\N	10	consu	Menthol	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
13	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	Big savings leaflet	\N	\N	11	consu	Big savings leaflet	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
4	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Orange	\N	\N	10	consu	Orange Burst	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
3	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Apple	\N	\N	10	consu	Apple Splash	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
6	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Coffee	\N	\N	10	consu	Morning Coffee	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
7	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Cola	\N	\N	10	consu	Cola Sensation	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
5	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	2.99	\N	1.00	fixed	10	\N	1.000	t	\N	1	Blackcurrant	\N	\N	10	consu	Blackcurrant Crush	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	2
14	1	2015-02-26 19:20:26.058933	2015-02-26 19:20:26.058933	1	\N	buy	\N	0.00	\N	0.00	fixed	11	\N	1.000	t	\N	1	30% off voucher	\N	\N	11	consu	30% off voucher	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
12	1	2015-02-26 19:20:26.058933	2015-12-03 18:50:50.461021	1	\N	buy	\N	4.99	\N	1.00	fixed	11	\N	1.000	t	\N	1	Smoking Device and USB charger package	\N	\N	11	consu	A reliable device, with a simple 5 click on, 5 click off operating function, when fully charged battery can generate enough vapour for 800 puffs.	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
11	1	2015-02-26 19:20:26.058933	2015-12-03 18:53:54.638944	1	\N	buy	\N	4.99	\N	1.00	fixed	11	\N	1.000	t	\N	1	Vaporizer	\N	\N	11	consu	Each vaporizer can hold 1.6 ml of liquid and can be refilled many times before needing to be replaced.	\N	\N	\N	\N	make_to_stock	standard	f	t	7	\N	1	3
15	1	2015-12-04 15:51:49.001002	\N	\N	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Device1	\N	\N	1	consu	That is just a test	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
16	1	2015-12-04 15:51:56.496749	2015-12-04 15:53:16.958352	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Device2	\N	\N	1	consu	That is just a test	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
17	1	2015-12-04 15:53:47.119517	2015-12-04 15:56:02.310767	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Device 3	\N	\N	1	consu	That is just a test	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
18	1	2015-12-04 15:56:05.716387	2015-12-04 15:56:24.964806	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	Device 4 	\N	\N	1	consu	That is just a test	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
19	1	2015-12-04 16:07:26.049493	\N	\N	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	DIY Liquid1	\N	\N	1	consu	This is just a test with DIY Liquid	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
20	1	2015-12-04 16:07:30.066899	2015-12-04 16:07:49.027742	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	DIY Liquid2	\N	\N	1	consu	This is just a test with DIY Liquid	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
21	1	2015-12-04 16:07:53.042799	2015-12-04 16:08:07.082369	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	DIY Liquid3	\N	\N	1	consu	This is just a test with DIY Liquid	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
22	1	2015-12-04 16:08:11.105775	2015-12-04 16:08:28.700147	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	DIY Liquid4	\N	\N	1	consu	This is just a test with DIY Liquid	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
23	1	2015-12-04 16:08:30.654278	2015-12-04 16:08:49.788347	1	0	buy	\N	1.00	0.00	1.00	fixed	1	\N	1.000	t	\N	1	DIY Liquid5	\N	\N	1	consu	This is just a test with DIY Liquid	0.00	0	\N	\N	make_to_stock	standard	f	t	7	\N	1	1
\.


--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 364
-- Name: product_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_template_id_seq', 23, true);


-- Completed on 2015-12-04 11:29:23 CST

--
-- PostgreSQL database dump complete
--

