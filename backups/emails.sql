--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-11-20 10:12:02 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2991 (class 0 OID 82795)
-- Dependencies: 526
-- Data for Name: smk_lqd_express_notification_email; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY smk_lqd_express_notification_email (id, create_uid, create_date, write_date, write_uid, email_from_addr, status, email_template, email_content, email_condition_listb, email_list, addressee, require_addr, email_server, minute, name, hour, email_condition_lista1, email_condition_lista2, frequency, email_subject, email_condition_listb2, email_header, frequency2, beginning, beginning2) FROM stdin;
1	1	2015-11-17 15:25:12.532103	\N	\N	1	y	5	<p>Thanks for giving us a try!</p>\n\n<p>This is an automated email to let you know that we have created your account for you at <a href="http://lambertsmokingliquid.co.uk/usersadmin">http://lambertsmokingliquid.co.uk/usersadmin</a></p>\n\n<p>This is the password you will need to access your personalised user area...</p>\n\n<p>password: {{password}}</p>\n\n<p>&nbsp;\n<p>All the team<br />\nlambertsmokingliquid.co.uk</p>\n</p>\n	cb1	c	\N	n	2	\N	account_details	\N	ca11	ca21	1	Account Details	cb1	5	1	\N	\N
2	1	2015-11-17 15:39:04.06083	\N	\N	1	y	5	Your order was received.	cb1	c	\N	n	2	\N	new_order_received	\N	ca11	ca21	1	Order Received	cb1	5	1	\N	\N
3	1	2015-11-17 15:42:46.995543	\N	\N	1	y	5	<p>&nbsp;</p>\n\n<p>This is an automated email to let you know your username and password:</p>\n\n<p>Username:{{username}}<br />\nPassword:{{password}}</p>\n\n<p>If you did not request this email please contact us straight away.</p>\n\n<p>&nbsp;</p>\n\n<p>many thanks</p>\n\n<p>All the team<br />\nlambertsmokingliquid.co.uk</p>\n	cb1	c	\N	n	2	\N	password	\N	ca11	ca21	1	Your Password	cb1	5	1	\N	\N
4	1	2015-11-17 15:47:48.093055	\N	\N	1	y	5	You have earned a new free bottle again, we will include in your next order.	cb1	c	\N	n	2	\N	another_free_bottle	\N	ca11	ca21	1	Another FREE bottle of Liquid for you	cb1	5	1	\N	\N
5	1	2015-11-17 16:03:53.676695	\N	\N	1	y	5	<p>&nbsp;</p>\n\n<p>We received your request please confirm that follow this link {{link}}.</p>\n\n<p>many thanks</p>\n\n<p>All the team<br />\nlambertsmokingliquid.co.uk</p>\n	cb1	c	\N	n	2	\N	please_confirm	\N	ca11	ca21	1	Please Confirm	cb1	3	1	\N	\N
6	1	2015-11-17 17:26:40.687356	\N	\N	1	y	5	<p>Your Free Sample order was printed, that include:</p>\n\n<p>1 x big savings leaflet<br />\n1 x 40% off voucher</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n	cb1	c	\N	n	2	\N	free_sample_printed	\N	ca11	ca21	1	FREE Sample on its way	cb1	3	1	\N	\N
7	1	2015-11-17 17:26:47.463603	2015-11-17 17:27:22.539048	1	1	y	5	<p>Your Free Sample order was generated, that include:</p>\n\n<p>1 x big savings leaflet<br />\n1 x 40% off voucher</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n	cb1	c	\N	n	2	\N	free_sample_generated	\N	ca11	ca21	1	FREE Bottle confirmed...thanks...	cb1	3	1	\N	\N
8	1	2015-11-17 17:30:11.501761	\N	\N	1	y	5	<p>You receive a new internal message, login to view it.</p>\n	cb1	c	"c.lambert@lambertsmokingliquid.co.uk"	y	2	\N	new_message	\N	ca11	ca21	1	New internal message	cb1	5	1	\N	\N
11	1	2015-11-18 16:18:33.497802	\N	\N	1	y	5	<p>Email text</p>\n\n<p>&nbsp;</p>\n\n<p>Move this variable to the place where you want to put the 30% code {{code30}}</p>\n	cb1	d	\N	n	2	0	\N	6	ca11	ca22	1	30% OFF Voucher	cb1	5	1	\N	\N
12	1	2015-11-18 16:18:40.29202	2015-11-18 16:19:29.791025	1	1	y	5	<p>Email text</p>\n\n<p>&nbsp;</p>\n\n<p>Move this variable to the place where you want to put the 30% code {{code30}}</p>\n	cb2	b	\N	n	2	0	\N	6	ca11	ca22	1	30% OFF Voucher	cb2	5	1	\N	\N
10	1	2015-11-18 15:24:17.048728	2015-11-18 16:20:07.945847	1	1	y	5	<p>Email text here</p>\n	cb2	b	\N	n	2	0	\N	6	ca11	ca22	2	VARIOUS SUBJECTS WILL BE USED	cb2	5	2	\N	\N
9	1	2015-11-18 15:24:10.510257	2015-11-18 16:55:29.627391	1	1	y	5	<p>Email text here</p>\n	cb1	d	\N	n	2	0	\N	6	ca11	ca22	2	VARIOUS SUBJECTS WILL BE USED	cb1	5	1	\N	\N
13	1	2015-11-18 16:58:33.596568	\N	\N	1	y	5	<p>Email text here</p>\n	cb3	b	\N	n	2	0	\N	6	ca11	ca21	1	FREE Liquid for Life	cb2	5	5	\N	5
14	1	2015-11-18 17:46:52.931927	2015-11-18 18:01:47.377442	1	1	y	5	<p>Email text here</p>\n\n<p>&nbsp;</p>\n\n<p>Select into this variables list :</p>\n\n<p>{{current_free_bottles_earned}}</p>\n\n<p>{{last_free_bottles_earned}}</p>\n\n<p>{{introduced_peoples}}</p>\n\n<p>and put in the text place where you want to put the stats.</p>\n	cb3	b	\N	n	1	0	\N	6	ca11	ca21	1	your referrals	cb1	5	2	\N	7
15	1	2015-11-19 18:21:08.499729	2015-11-19 18:23:37.940196	1	1	y	5	<p>Email text</p>\n\n<p>&nbsp;</p>\n\n<p>Move this variable to the place where you want to put the 30% code {{code30}}</p>\n	cb3	b	\N	n	2	0	\N	6	ca11	ca22	1	30% OFF Voucher	cb2	4	6	\N	6
16	1	2015-11-19 18:23:44.213188	2015-11-19 18:24:20.935825	1	1	y	5	<p>Email text</p>\n\n<p>&nbsp;</p>\n\n<p>Move this variable to the place where you want to put the 30% code {{code30}}</p>\n	cb3	d	\N	n	2	0	\N	6	ca11	ca23	6	30% OFF Voucher	cb2	4	6	6	6
\.


--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 525
-- Name: smk_lqd_express_notification_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('smk_lqd_express_notification_email_id_seq', 16, true);


-- Completed on 2015-11-20 10:12:02 CST

--
-- PostgreSQL database dump complete
--

