--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-11-13 10:47:06 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 3011 (class 0 OID 77397)
-- Dependencies: 176
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
32	pbkdf2_sha256$10000$nSMd07QkGb4z$mr3sjvTf5UdZC8UE7cmFYnqWjJLJgL3cfUbZpyjVb0U=	2015-11-11 14:01:14.401643-05	f	clive.lambert@outlook.com	test - name	test	clive.lambert@outlook.com	f	t	2015-07-07 15:01:26.327318-04
\.


--
-- TOC entry 3064 (class 0 OID 0)
-- Dependencies: 179
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('auth_user_id_seq', 36, true);


--
-- TOC entry 3013 (class 0 OID 77515)
-- Dependencies: 212
-- Data for Name: common_profile; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY common_profile (id, user_id, customer_id) FROM stdin;
25	32	29
\.


--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 213
-- Name: common_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('common_profile_id_seq', 29, true);


--
-- TOC entry 3015 (class 0 OID 78244)
-- Dependencies: 408
-- Data for Name: res_partner; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY res_partner (id, create_uid, create_date, write_date, write_uid, website, ean13, color, active, owner, supplier, user_id, title, company_id, parent_id, password_blank, employee, ref, vat, username, lang, date, customer, credit_limit, name, comment) FROM stdin;
29	\N	\N	\N	\N			\N	t	\N	f	\N	7	\N	\N	c1234c	f			clive.lambert@outlook.com		\N	t	\N	test - name test	
\.


--
-- TOC entry 3066 (class 0 OID 0)
-- Dependencies: 422
-- Name: res_partner_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('res_partner_id_seq', 33, true);


--
-- TOC entry 3018 (class 0 OID 82328)
-- Dependencies: 524
-- Data for Name: smk_lqd_subs_customer; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY smk_lqd_subs_customer (id, create_uid, create_date, write_date, write_uid, username, town, last_order_date, name, city, street, postcode, hbn, created_date, email, first_order_date, customer, free_bottle_count, introduced_peoples) FROM stdin;
1	\N	\N	\N	\N		my town	2015-11-13	test - name test	my city	street	12345	1234	\N	clive.lambert@outlook.com	2015-11-13	29	0	0
\.


--
-- TOC entry 3067 (class 0 OID 0)
-- Dependencies: 523
-- Name: smk_lqd_subs_customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('smk_lqd_subs_customer_id_seq', 2, true);


-- Completed on 2015-11-13 10:47:06 CST

--
-- PostgreSQL database dump complete
--

