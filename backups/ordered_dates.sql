--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-12-24 14:14:51 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2994 (class 0 OID 100556)
-- Dependencies: 532
-- Data for Name: common_monthodered; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY common_monthodered (id, profile_id, month_index, ordered_date) FROM stdin;
1	25	1	2015-05-20
2	25	3	2015-06-21
3	25	10	2015-07-22
\.


--
-- TOC entry 3000 (class 0 OID 0)
-- Dependencies: 531
-- Name: common_monthodered_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('common_monthodered_id_seq', 1, false);


-- Completed on 2015-12-24 14:14:52 CST

--
-- PostgreSQL database dump complete
--

