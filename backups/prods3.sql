--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-11-26 11:57:39 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2991 (class 0 OID 78049)
-- Dependencies: 353
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY product_product (id, create_uid, create_date, write_date, write_uid, ean13, is_flavour, color, price_extra, default_code, name_template, active, variants, product_strength, product_image, product_tmpl_id, price_margin, stock_qty, cloud_category) FROM stdin;
47	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-27	Menthol	t	\N	1	\N	2	1.00	10	1
48	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-28	Menthol	t	\N	7	\N	2	1.00	10	1
49	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-29	Apple	t	\N	1	\N	3	1.00	10	1
50	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-30	Apple	t	\N	7	\N	3	1.00	10	1
51	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-31	Orange	t	\N	1	\N	4	1.00	10	1
52	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-32	Orange	t	\N	7	\N	4	1.00	10	1
53	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-33	Blackcurrant	t	\N	1	\N	5	1.00	10	1
55	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-35	Coffee	t	\N	1	\N	6	1.00	10	1
56	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-36	Coffee	t	\N	7	\N	6	1.00	10	1
57	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-37	Cola	t	\N	1	\N	7	1.00	10	1
58	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-38	Cola	t	\N	7	\N	7	1.00	10	1
54	1	2015-02-26 19:20:26.058933	\N	\N	\N	t	0	0.00	A-34	Blackcurrant	t	\N	7	\N	5	1.00	0	1
\.


--
-- TOC entry 3012 (class 0 OID 0)
-- Dependencies: 354
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('product_product_id_seq', 11, true);


-- Completed on 2015-11-26 11:57:39 CST

--
-- PostgreSQL database dump complete
--

