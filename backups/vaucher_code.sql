--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.6
-- Started on 2015-11-05 08:11:47 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 523 (class 1259 OID 76896)
-- Name: smk_lqd_vaucher_code; Type: TABLE; Schema: public; Owner: smokingadmin; Tablespace: 
--

CREATE TABLE smk_lqd_vaucher_code (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    code character varying(10) NOT NULL,
    expired_on date,
    expired character varying,
    created date,
    percent integer
);


ALTER TABLE public.smk_lqd_vaucher_code OWNER TO smokingadmin;

--
-- TOC entry 2983 (class 0 OID 0)
-- Dependencies: 523
-- Name: TABLE smk_lqd_vaucher_code; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON TABLE smk_lqd_vaucher_code IS 'Vaucher code';


--
-- TOC entry 2984 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN smk_lqd_vaucher_code.code; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON COLUMN smk_lqd_vaucher_code.code IS 'code';


--
-- TOC entry 2985 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN smk_lqd_vaucher_code.expired_on; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON COLUMN smk_lqd_vaucher_code.expired_on IS 'Expired on';


--
-- TOC entry 2986 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN smk_lqd_vaucher_code.expired; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON COLUMN smk_lqd_vaucher_code.expired IS 'Expired';


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN smk_lqd_vaucher_code.created; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON COLUMN smk_lqd_vaucher_code.created IS 'Created';


--
-- TOC entry 2988 (class 0 OID 0)
-- Dependencies: 523
-- Name: COLUMN smk_lqd_vaucher_code.percent; Type: COMMENT; Schema: public; Owner: smokingadmin
--

COMMENT ON COLUMN smk_lqd_vaucher_code.percent IS 'Discount percent';


--
-- TOC entry 524 (class 1259 OID 76902)
-- Name: smk_lqd_vaucher_code_id_seq; Type: SEQUENCE; Schema: public; Owner: smokingadmin
--

CREATE SEQUENCE smk_lqd_vaucher_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.smk_lqd_vaucher_code_id_seq OWNER TO smokingadmin;

--
-- TOC entry 2989 (class 0 OID 0)
-- Dependencies: 524
-- Name: smk_lqd_vaucher_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: smokingadmin
--

ALTER SEQUENCE smk_lqd_vaucher_code_id_seq OWNED BY smk_lqd_vaucher_code.id;


--
-- TOC entry 2864 (class 2604 OID 76904)
-- Name: id; Type: DEFAULT; Schema: public; Owner: smokingadmin
--

ALTER TABLE ONLY smk_lqd_vaucher_code ALTER COLUMN id SET DEFAULT nextval('smk_lqd_vaucher_code_id_seq'::regclass);


--
-- TOC entry 2977 (class 0 OID 76896)
-- Dependencies: 523
-- Data for Name: smk_lqd_vaucher_code; Type: TABLE DATA; Schema: public; Owner: smokingadmin
--

COPY smk_lqd_vaucher_code (id, create_uid, create_date, write_date, write_uid, code, expired_on, expired, created, percent) FROM stdin;
1	\N	\N	\N	\N	5819d8	\N	n	2015-10-20	40
\.


--
-- TOC entry 2990 (class 0 OID 0)
-- Dependencies: 524
-- Name: smk_lqd_vaucher_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: smokingadmin
--

SELECT pg_catalog.setval('smk_lqd_vaucher_code_id_seq', 1, true);


--
-- TOC entry 2866 (class 2606 OID 76906)
-- Name: smk_lqd_vaucher_code_pkey; Type: CONSTRAINT; Schema: public; Owner: smokingadmin; Tablespace: 
--

ALTER TABLE ONLY smk_lqd_vaucher_code
    ADD CONSTRAINT smk_lqd_vaucher_code_pkey PRIMARY KEY (id);


--
-- TOC entry 2868 (class 2606 OID 76907)
-- Name: smk_lqd_vaucher_code_create_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: smokingadmin
--

ALTER TABLE ONLY smk_lqd_vaucher_code
    ADD CONSTRAINT smk_lqd_vaucher_code_create_uid_fkey FOREIGN KEY (create_uid) REFERENCES res_users(id) ON DELETE SET NULL;


--
-- TOC entry 2867 (class 2606 OID 76912)
-- Name: smk_lqd_vaucher_code_write_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: smokingadmin
--

ALTER TABLE ONLY smk_lqd_vaucher_code
    ADD CONSTRAINT smk_lqd_vaucher_code_write_uid_fkey FOREIGN KEY (write_uid) REFERENCES res_users(id) ON DELETE SET NULL;


-- Completed on 2015-11-05 08:11:48 CST

--
-- PostgreSQL database dump complete
--

